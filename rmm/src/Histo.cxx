// Developed at ASC ANL
// S.Chekanov (ANL). chakanau@hep.anl.gov
// Feb. 2010

#include <iostream>
using namespace std;

#include<iostream>

#include <TROOT.h>
#include"TFile.h"
#include"TH1D.h"
#include"TH2F.h"
#include"Histo.h"
#include<stdlib.h>

#include<TClonesArray.h>
#include <TTree.h>
#include <TFile.h>
#include "Global.h"
#include "SystemOfUnits.h"
#include "TMultiGraph.h"
#include "TCanvas.h"

extern string floatToString(float);
extern Global glob;


// constructor
Histo::Histo() {}


// constructor
void Histo::setOutput(string output) {


    
    string ffile="output/"+output+".root";
    cout << "\n -> Output file is =" << ffile << endl;
    RootFile = new TFile(ffile.c_str(), "RECREATE", "Histogram file");


}



// constructor
void Histo::setHistograms() {


    //Define the limit quantities for the histograms here
    //const float MIN_PT = 0;   const float MAX_PT = 3000;
    const float MIN_ETA = -3;   const float MAX_ETA = 3;
    //const float MIN_PHI = -4;   const float MAX_PHI = 4;
    const float MIN_MASS = 0;   const float MAX_MASS = 400;
    const float MIN_MASS_DIJET = 100;   const float MAX_MASS_DIJET = 10000; const int NBIN = 1800;


//     const int nBins=106;
//     double xbins[nBins] =  { 1006, 1037, 1068, 1100, 1133, 1166, 1200, 1234, 1269, 1305, 1341, 1378, 1416, 1454, 1493, 1533, 1573, 1614, 1656, 1698, 1741, 1785, 1830, 1875, 1921, 1968, 2016, 2065, 2114, 2164, 2215, 2267, 2320, 2374, 2429, 2485, 2542, 2600, 2659, 2719, 2780, 2842, 2905, 2969, 3034, 3100, 3167, 3235, 3305, 3376, 3448, 3521, 3596, 3672, 3749, 3827, 3907, 3988, 4070, 4154, 4239, 4326, 4414, 4504, 4595, 4688, 4782, 4878, 4975, 5074, 5175, 5277, 5381, 5487, 5595, 5705, 5817, 5931, 6047, 6165, 6285, 6407, 6531, 6658, 6787, 6918, 7052, 7188, 7326, 7467, 7610, 7756, 7904, 8055, 8208, 8364, 8523, 8685, 8850, 9019, 9191, 9366, 9544, 9726, 9911, 10100};

      double mjjBins[] = {99,112,125,138,151,164,177,190, 203, 216, 229, 243, 257, 272, 287, 303, 319, 335, 352, 369, 387, 405, 424, 443, 462, 482, 502, 523, 544, 566, 588, 611, 634, 657, 681, 705, 730, 755, 781, 807, 834, 861, 889, 917, 946, 976, 1006, 1037, 1068, 1100, 1133, 1166, 1200, 1234, 1269, 1305, 1341, 1378, 1416, 1454, 1493, 1533, 1573, 1614, 1656, 1698, 1741, 1785, 1830, 1875, 1921, 1968, 2016, 2065, 2114, 2164, 2215, 2267, 2320, 2374, 2429, 2485, 2542, 2600, 2659, 2719, 2780, 2842, 2905, 2969, 3034, 3100, 3167, 3235, 3305, 3376, 3448, 3521, 3596, 3672, 3749, 3827, 3907, 3988, 4070, 4154, 4239, 4326, 4414, 4504, 4595, 4688, 4782, 4878, 4975, 5074, 5175, 5277, 5381, 5487, 5595, 5705, 5817, 5931, 6047, 6165, 6285, 6407, 6531, 6658, 6787, 6918, 7052, 7188, 7326, 7467, 7610, 7756, 7904, 8055, 8208, 8364, 8523, 8685, 8850, 9019, 9191, 9366, 9544, 9726, 9911, 10100, 10292, 10488, 10688, 10892, 11100, 11312, 11528, 11748, 11972, 12200, 12432, 12669, 12910, 13156};

    const int nBins=sizeof(mjjBins)/sizeof(double);
    double xbins[nBins];
    double xbins_tev[nBins];
    for (int j=0; j<nBins; j++){xbins[j]=mjjBins[j]; xbins_tev[j]=0.001*mjjBins[j]; };

    tilePhiEta = new TH2D("tilePhiEta", "tilePhiEta", 64*3, -3.2, 3.2, 90, -1.5, 1.5); //dead modules, overcorrections, non-uniformity


    for (int i=0; i<nBins; i++) cout << i << " " << xbins[i] << endl;


    cout << "Histo::setHistograms() is called" << endl;
    // initialisations of histograms
    debug = new TH1D( "debug", "debug numbers", 15, 0, 15);
    hweights = new TH1D( "analysis_weights", "analysis_weights", 10000, 0, 10000);
    hweights_nominal = new TH1D( "nominal_weights", "nominal_weights", 10000, 0, 10000);

    triggers = new TH1D( "triggers", "trigger LR=0, met=1, 2g=2, 3j=3, 1j=4, 1g=5", 10, 0, 10);
    triggers_ex = new TH1D( "triggers_ex", "exclusive trigger LR=0, met=1, 2g=2,3j=3, 1j=4, 1g=5", 10, 0, 10);
    triggers_selected = new TH1D( "triggers_selected", "triggers for selected events LR=0, met=1, 2g=2, 3j=3, 1j=4, 1g=5", 10, 0, 10);

    cpucores = new TH1D( "cpucores", "CPU cores", 5, 0, 5);
    cpucores->Fill(1,1);


    float MinRun=325000; 
    float MaxRun=340100; 
    runnumber2017=new TH1D( "events_per_run2017", "event_per_run in 2017", MaxRun-MinRun, MinRun, MaxRun);
    MinRun=297500;
    MaxRun=311500; 
    runnumber2016=new TH1D( "events_per_run2016", "event_per_run in 2016", MaxRun-MinRun, MinRun, MaxRun);

    cutflow = new TH1D( "cutflow", "cutflow in HNominal", 100, 0, 100);
    cutflow_weighted = new TH1D( "cutflow_weighted", "cutflow_weighted in HNominal with SumOfWeights", 100, 0, 100);
    cutflow_weighted_pu = new TH1D( "cutflow_weighted_pu", "cutflow_weighted in HNominal_PUWg", 100, 0, 100);

    cutflow_electrons = new TH1D( "cutflow_electrons", "cutflow for electron selection ", 12, 0, 12);
    cutflow_muons = new TH1D( "cutflow_muons", "cutflow for muon selection", 12, 0, 12);
    cutflow_jets = new TH1D( "cutflow_jets", "cutflow for jet selection", 12, 0, 12);
    cutflow_photons = new TH1D( "cutflow_photons", "cutflow for photon selection", 12, 0, 12);
    cross_section = new TH1D( "CrossSection", "Cross section*filter_eff for MC [pb]", 10, 0, 10);

    monrandom = new TH1D( "monrandom", "Random monitor", 100, 0,1.1); 

   // Large-R jet kinematics
   
    LR_jet_pt = new TH1D( "recojet_antikt10UFO_NOSYS_pt", "pt of all LR jets", 400, 0, 4000); LR_jet_pt->Sumw2();
    LR_jet_eta = new TH1D( "recojet_antikt10UFO_NOSYS_eta", "eta of all LR jets", 40,-3,3); LR_jet_eta->Sumw2();
    LR_jet_phi = new TH1D( "recojet_antikt10UFO_NOSYS_phi", "phi of all LR jets", 40, -4,4); LR_jet_phi->Sumw2();
    LR_jet_m = new TH1D( "recojet_antikt10UFO_NOSYS_m", "mass of all LR jets", 400,0,1000); LR_jet_m->Sumw2();
 
    LR_leading_jet_pt = new TH1D( "recojet_antikt10UFO_NOSYS_leadingVRTrackJetsPt", "pt of all LR jets", 400, 0, 4000); LR_leading_jet_pt->Sumw2();
    LR_leading_jet_eta = new TH1D( "recojet_antikt10UFO_NOSYS_leadingVRTrackJetsEta", "eta of all LR jets", 40,-3,3); LR_leading_jet_eta->Sumw2();
    LR_leading_jet_phi = new TH1D( "recojet_antikt10UFO_NOSYS_leadingVRTrackJetsPhi", "phi of all LR jets", 40, -4,4); LR_leading_jet_phi->Sumw2();
    LR_leading_jet_m = new TH1D( "recojet_antikt10UFO_NOSYS_leadingVRTrackJetsM", "mass of all LR jets", 400,0,1000); LR_leading_jet_m->Sumw2();

    xbb_higgs = new TH1D( "recojet_antikt10UFO_NOSYS_Xbb2020v3_Higgs", "xbb Higgs score",100,-1,1);
    leading_jet_DL1r = new TH1D( "recojet_antikt10UFO_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_77", "Leading LR jet DL1r score", 100, -1,1);
 
    jet_pt = new TH1D( "jet_pt", "pt of all jets", 400, 0,4000);  jet_pt->Sumw2();
    jet_eta = new TH1D( "jet_eta", "eta of all jets", 40, -3.,3.);  jet_eta->Sumw2();

    //Jet Kinematic quantities
    leadingjetpt = new TH1D( "LeadingJetPt", "Leading Jet Pt", 400, 0,4000);  leadingjetpt->Sumw2(); 
    leadingjetpt_el = new TH1D( "LeadingJetPt_el", "Leading Jet Pt el", 200, 0,2000);  leadingjetpt_el->Sumw2();
    leadingjetpt_mu = new TH1D( "LeadingJetPt_mu", "Leading Jet Pt mu", 200, 0,2000);  leadingjetpt_mu->Sumw2();

    leadingjetm = new TH1D( "LeadingJetMass", "Leading Jet Mass", 200, MIN_MASS, MAX_MASS); leadingjetm->Sumw2(); 
    leadingjetm800 = new TH1D( "LeadingJetMass800", "Leading Jet Mass pT=800 GeV", 200, MIN_MASS, MAX_MASS); leadingjetm800->Sumw2(); 
    leadingjetm800b1 = new TH1D( "LeadingJetMass800b1", "Leading Jet Mass pT=800 GeV B1 tag", 200, MIN_MASS, MAX_MASS); leadingjetm800b1->Sumw2();
    leadingjetm800b2 = new TH1D( "LeadingJetMass800b2", "Leading Jet Mass pT=800 GeV B2 tag", 200, MIN_MASS, MAX_MASS); leadingjetm800b2->Sumw2();
    leadingjetm800b12 = new TH1D( "LeadingJetMass800b12", "Leading Jet Mass pT=800 GeV B12 tag", 200, MIN_MASS, MAX_MASS); leadingjetm800b12->Sumw2();
    leadingjetm700 = new TH1D( "LeadingJetMass700", "Leading Jet Mass pT=700 GeV", 200, MIN_MASS, MAX_MASS); leadingjetm700->Sumw2();
    leadingjetm700b1 = new TH1D( "LeadingJetMass700b1", "Leading Jet Mass pT=700 GeV B1 tag", 200, MIN_MASS, MAX_MASS); leadingjetm700b1->Sumw2();
    leadingjetm700b2 = new TH1D( "LeadingJetMass700b2", "Leading Jet Mass pT=700 GeV B2 tag", 200, MIN_MASS, MAX_MASS); leadingjetm700b2->Sumw2();
    leadingjetm700b12 = new TH1D( "LeadingJetMass700b12", "Leading Jet Mass pT=700 GeV B12 tag", 200, MIN_MASS, MAX_MASS); leadingjetm700b12->Sumw2();
    leadingjetm600 = new TH1D( "LeadingJetMass600", "Leading Jet Mass pT=600 GeV", 200, MIN_MASS, MAX_MASS); leadingjetm600->Sumw2();
    leadingjetm600b1 = new TH1D( "LeadingJetMass600b1", "Leading Jet Mass pT=600 GeV B1 tag", 200, MIN_MASS, MAX_MASS); leadingjetm600b1->Sumw2();
    leadingjetm600b2 = new TH1D( "LeadingJetMass600b2", "Leading Jet Mass pT=600 GeV B2 tag", 200, MIN_MASS, MAX_MASS); leadingjetm600b2->Sumw2();
    leadingjetm600b12 = new TH1D( "LeadingJetMass600b12", "Leading Jet Mass pT=600 GeV B12 tag", 200, MIN_MASS, MAX_MASS); leadingjetm600b12->Sumw2();

    leadingjeteta = new TH1D( "LeadingJetEta", "Leading Jet Eta", 40, MIN_ETA, MAX_ETA);leadingjeteta->Sumw2(); 
    leadingjetphi = new TH1D( "LeadingJetPhi", "Leading Jet Phi", 40, -4, 4);leadingjetphi->Sumw2();  
    leadingjetetavsphi = new TH2D( "LeadingJetEtavsPhi", "Leading Jet Eta vs Phi", 20, MIN_ETA, MAX_ETA, 20, -4, 4);leadingjetetavsphi->Sumw2(); 

    secondjetpt = new TH1D( "SecondLeadingJetPt", "Second Leading Jet Pt", 400, 0, 4000); secondjetpt->Sumw2(); 
    secondjetm = new TH1D( "SecondLeadingJetMass", "Second Leading Jet Mass", 200, MIN_MASS, MAX_MASS); secondjetm->Sumw2(); 
    secondjeteta = new TH1D( "SecondLeadingJetEta", "Second Leading Jet Eta", 40, MIN_ETA, MAX_ETA); secondjeteta->Sumw2();  
    secondjetphi = new TH1D( "SecondLeadingJetPhi", "Second Leading Jet Phi", 40, -4, 4); secondjetphi->Sumw2();    
    secondjetetavsphi = new TH2D( "SecondJetEtavsPhi", "Second Jet Eta vs Phi", 40, MIN_ETA, MAX_ETA, 20, -4, 4);secondjetetavsphi->Sumw2(); 

    ystar = new TH1D( "yStar", "yStar", 30, -1.5, 1.5);ystar->Sumw2();
    dphi = new TH1D( "dphi", "delta phi", 200, 0, 4);dphi->Sumw2();
    njet = new TH1D( "njet", "number of jets", 15, 0, 15);njet->Sumw2();
    et = new TH1D( "et", "missing ET", 30, -1.5, 1.5);et->Sumw2();
    ht = new TH1D( "ht", "ht", 30, -1.5, 1.5);ht->Sumw2();

    m_ele_pt = new TH1D( "m_ele_pt", "pT ele", 400, 0, 4000);m_ele_pt->Sumw2();
    m_mu_pt = new TH1D( "m_mu_pt", "pT muon", 400, 0, 4000);m_mu_pt->Sumw2();
    overlap_ele = new TH1D( "overlap_ele", "overlap_ele (electrons) dR=0.1", 500, 0, 2.5);
    overlap_ntrk_ele = new TH1D( "overlap_ntrk_ele", "overlap_ntrk (electrons) dR=0.1", 100, 0, 100.0); 
    overlap_mu = new TH1D( "overlap_mu", "overlap_ele (muons) dR=0.1", 500, 0, 2.0);
    overlap_ntrk_mu = new TH1D( "overlap_ntrk_mu", "overlap_ntrk (muons) dR=0.1", 100, 0, 100.0); 


    muon_eta_phi = new TH2D( "muon_eta_phi", "muon eta - phi", 100, -3.0, 3.0, 100, -4.0, 4.0);
    elec_eta_phi = new TH2D( "electron_eta_phi", "electron eta - phi", 100, -3.0, 3.0, 100, -4.0, 4.0);

    muon_pt3500 = new TH1D( "muon_pt_mjj3500", "muon pt for Mjj above 3500", 400, 0, 4000);
    elec_pt3500 = new TH1D( "elec_pt_mjj3500", "elec pt for Mjj above 3500", 400, 0, 4000);

    pt_ele_tight = new TH1D( "pt_ele_tight", "pt_ele_tight", 400, 0, 2000);
    pt_ele_medium = new TH1D( "pt_ele_medium", "pt_ele_medium", 400, 0, 2000);
    pt_ele_loose = new TH1D( "pt_ele_loose", "pt_ele_loose", 400, 0, 2000);
    pt_mu_tight = new TH1D( "pt_mu_tight", "pt_mu_tight", 400, 0, 2000);
    pt_mu_medium = new TH1D( "pt_mu_medium", "pt_mu_medium", 400, 0, 2000);
    pt_mu_loose = new TH1D( "pt_mu_loose", "pt_mu_loose", 400, 0, 2000);


   binsM = new TH1D("bins_m", "bins_m", nBins-1, xbins);
   binsM->Sumw2();
   binsM_tev = new TH1D("bins_m_tev", "bins_m_tev", nBins-1, xbins_tev);
   binsM_tev->Sumw2();

   for (Int_t j=0; j<nBins-1; j++) {
             float x=xbins[j+1]-xbins[j];
             binsM->Fill(xbins[j]+0.5*x,x);
             x=xbins_tev[j+1]-xbins_tev[j];
             binsM_tev->Fill(xbins_tev[j]+0.5*x,x);
             }
    // set bin errors to 0 (we use it to divide the bin width!)  
    for (int i=0 ; i<(binsM->GetNbinsX()); i++) {
                                binsM->SetBinError(  i+1, 0.0);
                                binsM_tev->SetBinError(  i+1, 0.0);
    } 

    leadingjetpt_b = new TH1D( "LeadingJetPt_b", "Leading Jet Pt B-tag", 300, 0,3000);  leadingjetpt_b->Sumw2();
    leadingjetm_b = new TH1D( "LeadingJetMass_b", "Leading Jet Mass B-tag", 200, MIN_MASS, MAX_MASS); leadingjetm_b->Sumw2();
    secondjetpt_b = new TH1D( "secondJetPt_b", "second Jet Pt B-tag", 300, 0,3000);  secondjetpt_b->Sumw2();
    secondjetm_b = new TH1D( "secondJetMass_b", "second Jet Mass B-tag", 200, MIN_MASS, MAX_MASS); secondjetm_b->Sumw2();
    leadingjeteta_b = new TH1D( "LeadingJetEta_b", "Leading Jet Eta B-tag", 40, MIN_ETA, MAX_ETA);leadingjeteta_b->Sumw2(); 
    leadingjetphi_b = new TH1D( "LeadingJetPhi_b", "Leading Jet Phi B-tag", 40, -4, 4);leadingjetphi_b->Sumw2();  
    leadingjetetavsphi_b = new TH2D( "LeadingJetEtavsPhi_b", "Leading Jet Eta vs Phi B-tag", 20, MIN_ETA, MAX_ETA, 20, -4, 4);leadingjetetavsphi_b->Sumw2(); 
    leadingjetmv2c20_b = new TH1D( "LeadingJetmv2c20_b", "Leading Jet mv2c20 B-tag", 100, -1, 1);leadingjetmv2c20_b->Sumw2(); 
    secondjeteta_b = new TH1D( "SecondLeadingJetEta_b", "Second Leading Jet Eta B-tag", 40, MIN_ETA, MAX_ETA); secondjeteta_b->Sumw2();  
    secondjetphi_b = new TH1D( "SecondLeadingJetPhi_b", "Second Leading Jet Phi B-tag", 40, -4, 4); secondjetphi_b->Sumw2();    
    secondjetetavsphi_b = new TH2D( "SecondJetEtavsPhi_b", "Second Jet Eta vs Phi B-tag", 40, MIN_ETA, MAX_ETA, 20, -4, 4);secondjetetavsphi_b->Sumw2(); 
    ystar_b = new TH1D( "yStar_b", "yStar B-tag", 30, -1.5, 1.5);ystar_b->Sumw2();
    dphi_b = new TH1D( "dphi_b", "delta phi B-tag", 100, 0, 4);dphi_b->Sumw2();
    njet_b = new TH1D( "njet_b", "number of jets B-tag", 15, 0, 15);njet_b->Sumw2();
    et_b = new TH1D( "et_b", "missing ET B-tag", 30, -1.5, 1.5);et_b->Sumw2();
    ht_b = new TH1D( "ht_b", "ht B-tag", 30, -1.5, 1.5);ht_b->Sumw2();

   n_muon=new TH1D( "Nmuons", "Nr of muons", 50,0,50);
   n_muonCB=new TH1D( "sigmaQPBCabs", "sigmaQPBCabs", 100,0,1.0);
   n_electron=new TH1D( "Nelectrons", "Nr of electrons",50,0,50);
   n_muon_electron=new TH2D( "NmuonsNelecrons", "Nr of muons vs Nr of electrons", 10,0,10,10,0,10);
   n_ptmuon_ptelec=new TH2D( "PTmuonsPTelectrons", "pT electrons vs pT muons", 250,0,500,250,0,500);
   n_jet=new TH1D( "Njets", "Nr of jets",50,0,50);
   n_trigger=new TH1D( "Trigger", "Fired trigger",15,0,15);
   n_trigger_after_cuts=new TH1D( "TriggerAfterCuts", "Fired trigger after cuts",15,0,15);

   mass_ee=new TH1D( "mass_ee", "E+E- mass", 1000,0,2000); mass_ee->Sumw2();
   mass_mumu=new TH1D( "mass_mumu", "MU+MU- mass", 1000,0,2000); mass_mumu->Sumw2();
   mass_emu=new TH1D( "mass_emu", "EMU mass", 1000,0,2000); mass_emu->Sumw2();


   jetjetmass=new TH1D( "JetJetMass", "Jet Jet Mass", nBins-1, xbins);jetjetmass->Sumw2();
   jetjetmass_tev=new TH1D( "JetJetMass_tev", "Jet Jet Mass in TeV", nBins-1, xbins_tev);jetjetmass_tev->Sumw2();

   jetjetmass_lowtrigger=new TH1D( "JetJetMass_lowtrigger", "Jet Jet Mass Low and Higg pT trigger", nBins-1, xbins);jetjetmass_lowtrigger->Sumw2();
   jetjetmass_hightrigger=new TH1D( "JetJetMass_hightrigger", "Jet Jet Mass after low pT trigger", nBins-1, xbins);jetjetmass_hightrigger->Sumw2();



    Mjj=new TH1D( "Mjj", "Jet Jet Mass", nBins-1, xbins);Mjj->Sumw2();
    Mjb=new TH1D( "Mjb", "Jet Bjet Mass", nBins-1, xbins);Mjb->Sumw2();
    Mbb=new TH1D( "Mbb", "BB jet  Mass", nBins-1, xbins);Mbb->Sumw2();
    Mje=new TH1D( "Mje", "jet+e", nBins-1, xbins);Mje->Sumw2();
    Mjm=new TH1D( "Mjm", "jet+m", nBins-1, xbins);Mjm->Sumw2();
    Mjg=new TH1D( "Mjg", "jet+g", nBins-1, xbins);Mjg->Sumw2();
    Mbe=new TH1D( "Mbe", "b+e mass", nBins-1, xbins);Mbe->Sumw2();
    Mbm=new TH1D( "Mbm", "b+m mass", nBins-1, xbins);Mbm->Sumw2();
    Mbg=new TH1D( "Mbg", "b+g mass", nBins-1, xbins);Mbg->Sumw2();




   // no OR
   jetjetmass_noor=new TH1D( "JetJetMass_noor", "Jet Jet Mass no OR", nBins-1, xbins);jetjetmass_noor->Sumw2();
   jetjetmass_noor_tev=new TH1D( "JetJetMass_noor_tev", "Jet Jet Mass in TeV no OR", nBins-1, xbins_tev);jetjetmass_noor_tev->Sumw2();

   jetjetmassnb=new TH1D( "JetJetMassnb", "Jet Jet Mass normal binning", NBIN,MIN_MASS_DIJET,MAX_MASS_DIJET);jetjetmassnb->Sumw2();

   jetjetmass_mu=new TH1D( "JetJetMass_mu", "Jet Jet Mass + mu", nBins-1, xbins);jetjetmass_mu->Sumw2();
   jetjetmass_mu_exc=new TH1D( "JetJetMass_mu_exc", "Jet Jet Mass + mu (exclusive)", nBins-1, xbins);jetjetmass_mu_exc->Sumw2();

   jetjetmass_mu_tev=new TH1D( "JetJetMass_mu_tev", "Jet Jet Mass + mu", nBins-1, xbins_tev);jetjetmass_mu_tev->Sumw2();
   jetjetmass_el_tight=new TH1D( "JetJetMass_el_tight", "Jet Jet Mass + tight ele", nBins-1, xbins);jetjetmass_el_tight->Sumw2();
   jetjetmass_el_loose=new TH1D( "JetJetMass_el_loose", "Jet Jet Mass + loose ele", nBins-1, xbins);jetjetmass_el_loose->Sumw2();
   jetjetmass_el_medium=new TH1D( "JetJetMass_el_medium", "Jet Jet Mass + medium ele", nBins-1, xbins);jetjetmass_el_medium->Sumw2();

   // muons and electrons
   jetjetmass_muel=new TH1D( "JetJetMass_muel", "Jet Jet Mass + mu and e", nBins-1, xbins);jetjetmass_muel->Sumw2();
   jetjetmass_muel_tev=new TH1D( "JetJetMass_muel_tev", "Jet Jet Mass + mu and e", nBins-1, xbins_tev);jetjetmass_muel_tev->Sumw2();

   
    // 3 and 4 body
    jjlmass=new TH1D( "jjlmass", "Jet Jet Lep mass", nBins-1, xbins); jjlmass->Sumw2();
    jjlmass_mu=new TH1D( "jjlmass_mu", "Jet Jet MU mass", nBins-1, xbins); jjlmass_mu->Sumw2();
    jjlmass_el=new TH1D( "jjlmass_el", "Jet Jet EL mass", nBins-1, xbins); jjlmass_el->Sumw2();
    jjlmass_tev=new TH1D( "jjlmass_tev", "Jet Jet Lep mass", nBins-1, xbins_tev); jjlmass_tev->Sumw2();
    jjlmass_mu_tev=new TH1D( "jjlmass_mu_tev", "Jet Jet MU TeV mass", nBins-1, xbins_tev); jjlmass_mu_tev->Sumw2();
    jjlmass_el_tev=new TH1D( "jjlmass_el_tev", "Jet Jet EL TeV mass", nBins-1, xbins_tev); jjlmass_el_tev->Sumw2();

    jllmass=new TH1D( "jllmass", "Jet Lep Lep mass", nBins-1, xbins); jllmass->Sumw2();
    jllmass_mu=new TH1D( "jllmass_mu", "Jet MU MU mass", nBins-1, xbins); jllmass_mu->Sumw2();
    jllmass_el=new TH1D( "jllmass_el", "Jet EL EL mass", nBins-1, xbins); jllmass_el->Sumw2();
    jllmass_tev=new TH1D( "jllmass_tev", "Jet Lep Lep mass", nBins-1, xbins_tev); jllmass_tev->Sumw2();
    jllmass_mu_tev=new TH1D( "jllmass_mu_tev", "Jet MU MU TeV mass", nBins-1, xbins_tev); jllmass_mu_tev->Sumw2();
    jllmass_el_tev=new TH1D( "jllmass_el_tev", "Jet EL EL TeV mass", nBins-1, xbins_tev); jllmass_el_tev->Sumw2();

    jllmass_muel=new TH1D( "jllmass_muel", "Jet MU EL mass", nBins-1, xbins); jllmass_muel->Sumw2();
    jllmass_muel_tev=new TH1D( "jllmass_muel_tev", "Jet MU MU TeV mass", nBins-1, xbins_tev); jllmass_muel_tev->Sumw2();
 

    jjjlmass=new TH1D( "jjjlmass", "Jet Jet Jet Lep mass", nBins-1, xbins); jjjlmass->Sumw2();
    jjjlmass_mu=new TH1D( "jjjlmass_mu", "Jet Jet Jet MU mass", nBins-1, xbins); jjjlmass_mu->Sumw2();
    jjjlmass_el=new TH1D( "jjjlmass_el", "Jet Jet Jet EL mass", nBins-1, xbins); jjjlmass_el->Sumw2();
    jjjlmass_tev=new TH1D( "jjjlmass_tev", "Jet Jet Jet Lep mass", nBins-1, xbins_tev); jjjlmass_tev->Sumw2();
    jjjlmass_mu_tev=new TH1D( "jjjlmass_mu_tev", "Jet Jet Jet MU TeV mass", nBins-1, xbins_tev); jjjlmass_mu_tev->Sumw2();
    jjjlmass_el_tev=new TH1D( "jjjlmass_el_tev", "Jet Jet Jet EL TeV mass", nBins-1, xbins_tev); jjjlmass_el_tev->Sumw2();

    jjllmass=new TH1D( "jjllmass", "Jet Jet Lep Lep mass", nBins-1, xbins); jjllmass->Sumw2();
    jjllmass_mu=new TH1D( "jjllmass_mu", "Jet Jet Lep MU mass", nBins-1, xbins); jjllmass_mu->Sumw2();
    jjllmass_el=new TH1D( "jjllmass_el", "Jet Jet Lep EL mass", nBins-1, xbins); jjllmass_el->Sumw2();
    jjllmass_tev=new TH1D( "jjllmass_tev", "Jet Jet Lep Lep mass", nBins-1, xbins_tev); jjllmass_tev->Sumw2();
    jjllmass_mu_tev=new TH1D( "jjllmass_mu_tev", "Jet Jet Lep MU TeV mass", nBins-1, xbins_tev); jjllmass_mu_tev->Sumw2();
    jjllmass_el_tev=new TH1D( "jjllmass_el_tev", "Jet Jet Lep EL TeV mass", nBins-1, xbins_tev); jjllmass_el_tev->Sumw2();

    jjlmass_pt = new TH1D( "jjlmass_pt", "pt of all jets above mjjl 400 GeV", 400, 0,4000);  jjlmass_pt->Sumw2();
    jjllmass_pt = new TH1D( "jjllmass_pt", "pt of all jets above mjjll 500 GeV", 400, 0,4000);  jjllmass_pt->Sumw2();
 
    // run dependence
    jetjetmass1=new TH1D( "JetJetMass1", "Jet Jet Mass period 1", nBins-1, xbins);jetjetmass1->Sumw2();
    jetjetmass1_mu=new TH1D( "JetJetMass1_mu", "Jet Jet Mass mu period 1", nBins-1, xbins);jetjetmass1_mu->Sumw2();
    jetjetmass1_el=new TH1D( "JetJetMass1_el", "Jet Jet Mass el period 1", nBins-1, xbins);jetjetmass1_el->Sumw2();

    jetjetmass2=new TH1D( "JetJetMass2", "Jet Jet Mass perioud 2", nBins-1, xbins);jetjetmass2->Sumw2();
    jetjetmass2_mu=new TH1D( "JetJetMass2_mu", "Jet Jet Mass mu period 2", nBins-1, xbins);jetjetmass2_mu->Sumw2();
    jetjetmass2_el=new TH1D( "JetJetMass2_el", "Jet Jet Mass el period 2", nBins-1, xbins);jetjetmass2_el->Sumw2();

    jetjetmass3=new TH1D( "JetJetMass3", "Jet Jet Mass period 3", nBins-1, xbins);jetjetmass3->Sumw2();
    jetjetmass3_mu=new TH1D( "JetJetMass3_mu", "Jet Jet Mass mu period 3", nBins-1, xbins);jetjetmass3_mu->Sumw2();
    jetjetmass3_el=new TH1D( "JetJetMass3_el", "Jet Jet Mass el period 3", nBins-1, xbins);jetjetmass3_el->Sumw2();

    jetjetmass4=new TH1D( "JetJetMass4", "Jet Jet Mass period 4", nBins-1, xbins);jetjetmass4->Sumw2();
    jetjetmass4_mu=new TH1D( "JetJetMass4_mu", "Jet Jet Mass mu period 4", nBins-1, xbins);jetjetmass4_mu->Sumw2();
    jetjetmass4_el=new TH1D( "JetJetMass4_el", "Jet Jet Mass el period 4", nBins-1, xbins);jetjetmass4_el->Sumw2();


   // tight+medium
   jetjetmass_el=new TH1D( "JetJetMass_el", "Jet Jet Mass + ele", nBins-1, xbins);jetjetmass_el->Sumw2();
   jetjetmass_el_exc=new TH1D( "JetJetMass_el_exc", "Jet Jet Mass + ele (exclusive)", nBins-1, xbins);jetjetmass_el_exc->Sumw2();
   jetjetmass_el_tev=new TH1D( "JetJetMass_el_tev", "Jet Jet Mass + ele in TeV", nBins-1, xbins_tev);jetjetmass_el_tev->Sumw2();
   jetjetmass_el_isolated_Gradient=new TH1D( "JetJetMass_el_isolated_Gradient", "Jet Jet Mass + ele + isolation", nBins-1, xbins);jetjetmass_el_isolated_Gradient->Sumw2();
   jetjetmass_el_nonIsolated_Gradient=new TH1D( "JetJetMass_el_nonIsolated_Gradient", "Jet Jet Mass + ele + non-isolation", nBins-1, xbins);jetjetmass_el_nonIsolated_Gradient->Sumw2();

   // after isolation
   jetjetmass_el_medium_isolated_Gradient=new TH1D( "JetJetMass_el_medium_isolated_Gradient", "Jet Jet Mass + medium ele + gradient isolation", nBins-1, xbins);jetjetmass_el_medium_isolated_Gradient->Sumw2();
   jetjetmass_el_medium_nonIsolated_Gradient=new TH1D( "JetJetMass_el_medium_nonIsolated_Gradient", "Jet Jet Mass + medium ele + non gradient isolation", nBins-1, xbins);jetjetmass_el_medium_nonIsolated_Gradient->Sumw2();
   jetjetmass_el_tight_isolated_Gradient=new TH1D( "JetJetMass_el_tight_isolated_Gradient", "Jet Jet Mass + tight ele + gradient isolation", nBins-1, xbins);jetjetmass_el_tight_isolated_Gradient->Sumw2();
   jetjetmass_el_tight_nonIsolated_Gradient=new TH1D( "JetJetMass_el_tight_nonIsolated_Gradient", "Jet Jet Mass + tight ele + non gradient isolation", nBins-1, xbins);jetjetmass_el_tight_nonIsolated_Gradient->Sumw2();


   // 77% b-tag
   jetjetmass_b=new TH1D( "JetJetMass_b", "Jet Jet Mass B-tag 77%", nBins-1, xbins);jetjetmass_b->Sumw2();
   jetjetmass_b_exc=new TH1D( "JetJetMass_b_exc", "Jet Jet Mass B-tag exclusive 77%", nBins-1, xbins);jetjetmass_b_exc->Sumw2();

   jetjetmass_bb=new TH1D( "JetJetMass_bb", "Jet Jet Mass Double B-tag", nBins-1, xbins);jetjetmass_bb->Sumw2();
   jetjetmassnb_b=new TH1D( "JetJetMassnb_b", "Jet Jet Mass normal binning B-tag", NBIN,MIN_MASS_DIJET,MAX_MASS_DIJET);jetjetmassnb_b->Sumw2();
   jetjetmassnb_bb=new TH1D( "JetJetMassnb_bb", "Jet Jet Mass normal binning Double B-tag", NBIN,MIN_MASS_DIJET,MAX_MASS_DIJET);jetjetmassnb_bb->Sumw2();

   jetjetmass_mu_b=new TH1D( "JetJetMass_mu_b", "Jet Jet Mass B-tag + mu", nBins-1, xbins);jetjetmass_mu_b->Sumw2();
   jetjetmass_mu_bb=new TH1D( "JetJetMass_mu_bb", "Jet Jet Mass Double B-tag +mu", nBins-1, xbins);jetjetmass_mu_bb->Sumw2();
   jetjetmass_el_b=new TH1D( "JetJetMass_el_b", "Jet Jet Mass B-tag + ele", nBins-1, xbins);jetjetmass_el_b->Sumw2();
   jetjetmass_el_bb=new TH1D( "JetJetMass_el_bb", "Jet Jet Mass Double B-tag + ele", nBins-1, xbins);jetjetmass_el_bb->Sumw2();

   // anti-EWK region 
   jetjetmass_rest=new TH1D( "JetJetMass_rest", "Jet Jet Mass + rest of events", nBins-1, xbins);jetjetmass_rest->Sumw2();

   // with muon
   jetwithwithout = new TH2D( "Nwithwithout", "Nr with vs without", 10, 0, 10, 10, 0, 10); jetwithwithout->Sumw2();

    leadingjetpt_bb = new TH1D( "LeadingJetPt_bb", "Leading Jet Pt Double B-tag", 300, 0,3000);  leadingjetpt_bb->Sumw2();
    leadingjetm_bb = new TH1D( "LeadingJetMass_bb", "Leading Jet Mass Double B-tag", 100, MIN_MASS, MAX_MASS); leadingjetm_bb->Sumw2();
    secondjetpt_bb = new TH1D( "secondJetPt_bb", "second Jet Pt Double B-tag", 300, 0,3000);  secondjetpt_bb->Sumw2();
    secondjetm_bb = new TH1D( "secondJetMass_bb", "second Jet Mass Double B-tag", 100, MIN_MASS, MAX_MASS); secondjetm_bb->Sumw2();
    leadingjeteta_bb = new TH1D( "LeadingJetEta_bb", "Leading Jet Eta Double B-tag", 40, MIN_ETA, MAX_ETA);leadingjeteta_bb->Sumw2(); 
    leadingjetphi_bb = new TH1D( "LeadingJetPhi_bb", "Leading Jet Phi Double B-tag", 40, -4, 4);leadingjetphi_bb->Sumw2();  
    leadingjetetavsphi_bb = new TH2D( "LeadingJetEtavsPhi_bb", "Leading Jet Eta vs Phi Double B-tag", 20, MIN_ETA, MAX_ETA, 20, -4, 4);leadingjetetavsphi_bb->Sumw2(); 
    leadingjetmv2c20_bb = new TH1D( "LeadingJetmv2c20_bb", "Leading Jet mv2c20 Double B-tag", 100, -1, 1);leadingjetmv2c20_bb->Sumw2(); 
    secondjeteta_bb = new TH1D( "SecondLeadingJetEta_bb", "Second Leading Jet Eta Double B-tag", 40, MIN_ETA, MAX_ETA); secondjeteta_bb->Sumw2();  
    secondjetphi_bb = new TH1D( "SecondLeadingJetPhi_bb", "Second Leading Jet Phi Double B-tag", 40, -4, 4); secondjetphi_bb->Sumw2();    
    secondjetetavsphi_bb = new TH2D( "SecondJetEtavsPhi_bb", "Second Jet Eta vs Phi Double B-tag", 20, MIN_ETA, MAX_ETA, 20, -4, 4);secondjetetavsphi_bb->Sumw2(); 
    secondjetmv2c20_bb = new TH1D( "SecondLeadingJetmv2c20_bb", "Second Leading Jet mv2c20 Double B-tag", 100, -1, 1); secondjetmv2c20_bb->Sumw2();
    ystar_bb = new TH1D( "yStar_bb", "yStar Double B-tag", 30, -1.5, 1.5);ystar_bb->Sumw2();
    dphi_bb = new TH1D( "dphi_bb", "delta phi Double B-tag", 100, 0, 4);dphi_bb->Sumw2();
    njet_bb = new TH1D( "njet_bb", "number of jets Double B-tag", 15, 0, 15);njet_bb->Sumw2();
    et_bb = new TH1D( "et_bb", "missing ET Double B-tag", 30, -1.5, 1.5);et_bb->Sumw2();
    ht_bb = new TH1D( "ht_bb", "ht Double B-tag", 30, -1.5, 1.5);ht_bb->Sumw2();

    jetjetmass_badgood=new TH1D( "JetJetMassBadGood", "Jet Jet Mass BadGood", nBins-1, xbins);jetjetmass->Sumw2();


    jet_muon_angle = new TH1D( "jet_muon_angle", "Angle between dijet and muon", 200, 0, 4.0);  jet_muon_angle->Sumw2();
    jet_elec_angle = new TH1D( "jet_elec_angle", "Angle between dijet and electron", 200, 0, 4.0); jet_elec_angle->Sumw2();
    jet_muon_dR = new TH1D( "jet_muon_dR", "dR between jet and lead muon after final cuts", 200, 0, 7.0);  jet_muon_dR->Sumw2();
    jet_elec_dR = new TH1D( "jet_elec_dR", "dR between jet and lead electron after final cuts", 200, 0, 7.0); jet_elec_dR->Sumw2();
    jet_elec_overlap_dR = new TH1D( "jet_elec_overlap_dR", "dR between jet and lead electron before OR", 200, 0, 7.0);
  
    jet_muon_dR_after = new TH1D( "jet_muon_dR_after", "dR between jet and all muons after final cuts", 200, 0, 7.0);  jet_muon_dR_after->Sumw2();
    jet_elec_dR_after = new TH1D( "jet_elec_dR_after", "dR between jet and all electrons after final cuts", 200, 0, 7.0); jet_elec_dR_after->Sumw2();


    jet_muon3000_dR = new TH1D( "jet_muon3000_dR", "dR between jet and muon for Mjj above 3000", 200, 0, 7.0);  jet_muon3000_dR->Sumw2();
    jet_elec3000_dR = new TH1D( "jet_elec3000_dR", "dR between jet and electron for Mjj above 3000", 200, 0, 7.0); jet_elec3000_dR->Sumw2();

    ptmissTrk = new TH1D( "ptmissTrk", "MET from metFinalTrk", 300, 0, 3000.0); ptmissTrk->Sumw2();
    ptmissClus = new TH1D( "ptmissClus", "metFinalClus", 300, 0, 3000.0); ptmissClus->Sumw2();
    ptmissSum = new TH1D( "ptmissSum", "metMuons**2+metJet**2++metEle**2", 300, 0, 3000.0); ptmissSum->Sumw2();

    ptmissTrk_mu = new TH1D( "ptmissTrk_mu", "MET from metFinalTrk for muons", 300, 0, 3000.0); ptmissTrk_mu->Sumw2();
    ptmissClus_mu = new TH1D( "ptmissClus_mu", "metFinalClus for muons", 300, 0, 3000.0); ptmissClus_mu->Sumw2();
    ptmissSum_mu = new TH1D( "ptmissSum_mu", "metMuons**2+metJet**2++metEle**2 muons", 300, 0, 3000.0); ptmissSum_mu->Sumw2();

    ptmissTrk_el = new TH1D( "ptmissTrk_el", "MET from metFinalTrk for electrons", 300, 0, 3000.0); ptmissTrk_el->Sumw2();
    ptmissClus_el = new TH1D( "ptmissClus_el", "metFinalClus for electrons", 300, 0, 3000.0); ptmissClus_el->Sumw2();
    ptmissSum_el = new TH1D( "ptmissSum_el", "metMuons**2+metJet**2++metEle**2 electrons", 300, 0, 3000.0); ptmissSum_el->Sumw2();

    // met vs ptlepton
    ptmiss_ptlepton = new TH2D( "ptmiss_ptlepton", "MET vs max PT(lept)", 200, 0, 1000, 200, 0, 1000 );
    ptmissDIVptlepton = new TH1D( "ptmiss_div_ptlepton", "MET divided by ptlepton", 500, 0, 20.0);

    jetjetmass_ptmiss=new TH1D( "JetJetMass_ptmiss", "Jet Jet Mass with ptmiss", nBins-1, xbins);jetjetmass_ptmiss->Sumw2();
    jetjetmass_ptmiss_el=new TH1D( "JetJetMass_ptmiss_el", "Jet Jet Mass with ptmiss+ele", nBins-1, xbins);jetjetmass_ptmiss_el->Sumw2();
    jetjetmass_ptmiss_mu=new TH1D( "JetJetMass_ptmiss_mu", "Jet Jet Mass with ptmiss+mu", nBins-1, xbins);jetjetmass_ptmiss_mu->Sumw2();

    jetjetmass_ptmiss2=new TH1D( "JetJetMass_ptmiss2", "Jet Jet Mass with ptmiss x2 ", nBins-1, xbins);jetjetmass_ptmiss2->Sumw2();
    jetjetmass_ptmiss2_el=new TH1D( "JetJetMass_ptmiss2_el", "Jet Jet Mass with ptmiss+ele x2", nBins-1, xbins);jetjetmass_ptmiss2_el->Sumw2();
    jetjetmass_ptmiss2_mu=new TH1D( "JetJetMass_ptmiss2_mu", "Jet Jet Mass with ptmiss+mu x2", nBins-1, xbins);jetjetmass_ptmiss2_mu->Sumw2();

    jetjetmass_ptmiss100=new TH1D( "JetJetMass_ptmiss100", "Jet Jet Mass with ptmiss MET 100 GeV", nBins-1, xbins);jetjetmass_ptmiss100->Sumw2();
    jetjetmass_ptmiss100_el=new TH1D( "JetJetMass_ptmiss100_el", "Jet Jet Mass with ptmiss+ele MET 100 GeV", nBins-1, xbins);jetjetmass_ptmiss100_el->Sumw2();
    jetjetmass_ptmiss100_mu=new TH1D( "JetJetMass_ptmiss100_mu", "Jet Jet Mass with ptmiss+mu MET 100 GeV", nBins-1, xbins);jetjetmass_ptmiss100_mu->Sumw2();

    jetjetmass_ptmiss200=new TH1D( "JetJetMass_ptmiss200", "Jet Jet Mass with ptmiss MET 200 GeV", nBins-1, xbins);jetjetmass_ptmiss200->Sumw2();
    jetjetmass_ptmiss200_el=new TH1D( "JetJetMass_ptmiss200_el", "Jet Jet Mass with ptmiss+ele MET 200 GeV", nBins-1, xbins);jetjetmass_ptmiss200_el->Sumw2();
    jetjetmass_ptmiss200_mu=new TH1D( "JetJetMass_ptmiss200_mu", "Jet Jet Mass with ptmiss+mu MET 200 GeV", nBins-1, xbins);jetjetmass_ptmiss200_mu->Sumw2();

    jetjetmass_ptmiss400=new TH1D( "JetJetMass_ptmiss400", "Jet Jet Mass with ptmiss MET 400 GeV", nBins-1, xbins);jetjetmass_ptmiss400->Sumw2();
    jetjetmass_ptmiss600=new TH1D( "JetJetMass_ptmiss600", "Jet Jet Mass with ptmiss MET 600 GeV", nBins-1, xbins);jetjetmass_ptmiss600->Sumw2();
    jetjetmass_ptmiss800=new TH1D( "JetJetMass_ptmiss800", "Jet Jet Mass with ptmiss MET 800 GeV", nBins-1, xbins);jetjetmass_ptmiss800->Sumw2();
    jetjetmass_ptmiss1000=new TH1D( "JetJetMass_ptmiss1000", "Jet Jet Mass with ptmiss MET 1000 GeV", nBins-1, xbins);jetjetmass_ptmiss1000->Sumw2();

 
    // with some cuts
    jetjetmass_ystar=new TH1D( "JetJetMass_ystar", "JetJet Mass with ystar", nBins-1, xbins);jetjetmass_ystar->Sumw2();
    jetjetmass_ptl100=new TH1D( "JetJetMass_ptl100", "JetJet Mass with pt lep 100 GeV", nBins-1, xbins);jetjetmass_ptl100->Sumw2();
    jetjetmass_ptl200=new TH1D( "JetJetMass_ptl200", "JetJet Mass with pt lep 200 GeV", nBins-1, xbins);jetjetmass_ptl200->Sumw2();
    jetjetmass_ptl400=new TH1D( "JetJetMass_ptl400", "Jet Jet Mass with pt lep 400 GeV", nBins-1, xbins);jetjetmass_ptl400->Sumw2();

     // inclusive dijets
    jetjetmass_inc=new TH1D( "JetJetMass_inc", "JetJet Mass with loose lepton", nBins-1, xbins);jetjetmass_inc->Sumw2();
    jetjetmass_inc_pt60=new TH1D( "JetJetMass_inc_pt60", "JetJet Mass with loose lepton at pt 60", nBins-1, xbins);jetjetmass_inc_pt60->Sumw2();
    jetjetmass_inc_ystar=new TH1D( "JetJetMass_inc_ystar", "JetJet Mass without lepton |ystar|<0.6", nBins-1, xbins);jetjetmass_inc_ystar->Sumw2();
    jetjetmass_inc_tev=new TH1D( "JetJetMass_inc_tev", "Jet Jet Mass with no lepton  in TeV", nBins-1, xbins_tev);jetjetmass_inc_tev->Sumw2();

    // with jet masses
    jetjetmass_jhigh80=new TH1D( "JetJetMass_jhigh80", "JetJet Mass with both jet mass above 80 GeV", nBins-1, xbins);jetjetmass_jhigh80->Sumw2();
    jetjetmass_jlow80=new TH1D( "JetJetMass_jlow80", "JetJet Mass with both jet mass lower 80 GeV", nBins-1, xbins);jetjetmass_jlow80->Sumw2();
    jetjetmass_jhigh80_mu=new TH1D( "JetJetMass_jhigh80_mu", "JetJet Mass with both jet mass above 80 GeV (muons)", nBins-1, xbins);jetjetmass_jhigh80_mu->Sumw2();
    jetjetmass_jlow80_mu=new TH1D( "JetJetMass_jlow80_mu", "JetJet Mass with both jet mass lower 80 GeV (muons)", nBins-1, xbins);jetjetmass_jlow80_mu->Sumw2();
    jetjetmass_jhigh80_el=new TH1D( "JetJetMass_jhigh80_el", "JetJet Mass with both jet mass above 80 GeV (el)", nBins-1, xbins);jetjetmass_jhigh80_el->Sumw2();
    jetjetmass_jlow80_el=new TH1D( "JetJetMass_jlow80_el", "JetJet Mass with both jet mass lower 80 GeV (el)", nBins-1, xbins);jetjetmass_jlow80_el->Sumw2();
    // either jet
    jetjetmass_jhigh80e=new TH1D( "JetJetMass_jhigh80e", "JetJet Mass with with jet mass above 80 GeV", nBins-1, xbins);jetjetmass_jhigh80e->Sumw2();
    jetjetmass_jhigh80e_b=new TH1D( "JetJetMass_jhigh80e_b", "JetJet Mass with with jet mass above 80 GeV and 1 btag", nBins-1, xbins);jetjetmass_jhigh80e_b->Sumw2();

    jetjetmass_jhigh80e_mu=new TH1D( "JetJetMass_jhigh80e_mu", "JetJet Mass with with jet mass above 80 GeV (muons)", nBins-1, xbins);jetjetmass_jhigh80e_mu->Sumw2();
    jetjetmass_jhigh80e_el=new TH1D( "JetJetMass_jhigh80e_el", "JetJet Mass with with jet mass above 80 GeV (el)", nBins-1, xbins);jetjetmass_jhigh80e_el->Sumw2();
    // incl
    jetjetmass_jhigh80_inc=new TH1D( "JetJetMass_jhigh80_inc", "JetJet Mass with both jet mass above 80 GeV (inv leptons)", nBins-1, xbins);jetjetmass_jhigh80_inc->Sumw2();
    jetjetmass_jlow80_inc=new TH1D( "JetJetMass_jlow80_inc", "JetJet Mass with both jet mass lower 80 GeV (inv leptons)", nBins-1, xbins);jetjetmass_jlow80_inc->Sumw2();
    jetjetmass_jhigh80e_inc=new TH1D( "JetJetMass_jhigh80e_inc", "JetJet Mass with either jet mass above 80 GeV (inv leptons)", nBins-1, xbins);jetjetmass_jhigh80e_inc->Sumw2();
    jetjetmass_jhigh80e_b_inc=new TH1D( "JetJetMass_jhigh80e_b_inc", "JetJet Mass with either jet mass above 80 GeV (inv leptons) plus b-tag", nBins-1, xbins);jetjetmass_jhigh80e_b_inc->Sumw2();

    // with pt60
    jetjetmass_pt60_jhigh80_inc=new TH1D( "JetJetMass_pt60_jhigh80_inc", "JetJet Mass with both jet mass above 80 GeV (inv leptons pT60)", nBins-1, xbins);jetjetmass_pt60_jhigh80_inc->Sumw2();
    jetjetmass_pt60_jlow80_inc=new TH1D( "JetJetMass_pt60_jlow80_inc", "JetJet Mass with both jet mass lower 80 GeV (inv leptons pT60)", nBins-1, xbins);jetjetmass_pt60_jlow80_inc->Sumw2();
    jetjetmass_pt60_jhigh80e_inc=new TH1D( "JetJetMass_pt60_jhigh80e_inc", "JetJet Mass with either jet mass above 80 GeV (inv leptons pt60)", nBins-1, xbins);jetjetmass_pt60_jhigh80e_inc->Sumw2();


    // jet masses above 3 TeV 
    leadingjetmjj3000 = new TH1D( "LeadingJetMass_mjj3000", "Leading Jet Mass for Mjj above 3 TeV", 200, MIN_MASS, MAX_MASS); leadingjetmjj3000->Sumw2();
    subleadingjetmjj3000 = new TH1D( "SubLeadingJetMass_mjj3000", "SubLeading Jet Mass for Mjj above 3 TeV", 200, MIN_MASS, MAX_MASS); subleadingjetmjj3000->Sumw2();
    leadingjetmjj3000_inc = new TH1D( "LeadingJetMass_mjj3000_inc", "Leading Jet Mass for Mjj above 3 TeV for inverted cuts", 200, MIN_MASS, MAX_MASS); leadingjetmjj3000_inc->Sumw2();
    subleadingjetmjj3000_inc = new TH1D( "SubLeadingJetMass_mjj3000_inc", "SubLeading Jet Mass for Mjj above 3 TeV for inverted cuts", 200, MIN_MASS, MAX_MASS); subleadingjetmjj3000_inc->Sumw2();

    // after 1 TeV cut
    leadingjetmjj3000pt1 = new TH1D( "LeadingJetMass_mjj3000pt1", "Leading Jet Mass for Mjj above 3 TeV and pT above 1 TeV", 200, MIN_MASS, MAX_MASS); leadingjetmjj3000pt1->Sumw2();
    leadingjetmjj3000pt1_inc = new TH1D( "LeadingJetMass_mjj3000pt1_inc", "Leading Jet Mass for Mjj above 3 TeV for inverted cuts and pT above 1 TeV", 200, MIN_MASS, MAX_MASS); leadingjetmjj3000pt1_inc->Sumw2();


   // check lepton variables in the 3 TeV region
   muon3000_pt = new TH1D( "muon3000_pt", "PT of muons for MJJ above 3 TeV", 400, 0,4000);
   muon3000_eta = new TH1D( "muon3000_eta", "Eta of muons fpr MJJ above 3 TeV", 200, -4.0, 4.0);
   muon3000_phi = new TH1D( "muon3000_phi", "Phi of muons fpr MJJ above 3 TeV", 200, -7.0, 7.0);
   elec3000_pt = new TH1D( "elec3000_pt", "PT of elec for MJJ above 3 TeV", 400, 0,4000);
   elec3000_eta = new TH1D( "elec3000_eta", "Eta of elec fpr MJJ above 3 TeV", 200, -4.0, 4.0);
   elec3000_phi = new TH1D( "elec3000_phi", "Phi of elec fpr MJJ above 3 TeV", 200, -7.0, 7.0);

 // check lepton variables in the 2 TeV region
   muon2000_pt = new TH1D( "muon2000_pt", "PT of muons for MJJ above 2 TeV", 400, 0,4000);
   muon2000_eta = new TH1D( "muon2000_eta", "Eta of muons fpr MJJ above 2 TeV", 200, -4.0, 4.0);
   muon2000_phi = new TH1D( "muon2000_phi", "Phi of muons fpr MJJ above 2 TeV", 200, -7.0, 7.0);
   elec2000_pt = new TH1D( "elec2000_pt", "PT of elec for MJJ above 2 TeV", 400, 0,4000);
   elec2000_eta = new TH1D( "elec2000_eta", "Eta of elec fpr MJJ above 2 TeV", 200, -4.0, 4.0);
   elec2000_phi = new TH1D( "elec2000_phi", "Phi of elec fpr MJJ above 2 TeV", 200, -7.0, 7.0);


    // jet masses with single b-tag 
    leadingjetmjj3000_b = new TH1D( "LeadingJetMass_mjj3000_b", "Leading Jet Mass for Mjj above 3 TeV with b", 200, MIN_MASS, MAX_MASS); leadingjetmjj3000_b->Sumw2();
    subleadingjetmjj3000_b = new TH1D( "SubLeadingJetMass_mjj3000_b", "SubLeading Jet Mass for Mjj above 3 TeV with b", 200, MIN_MASS, MAX_MASS); subleadingjetmjj3000_b->Sumw2();
    leadingjetmjj3000_inc_b = new TH1D( "LeadingJetMass_mjj3000_inc_b", "Leading Jet Mass for Mjj above 3 TeV for inverted cuts with b", 200, MIN_MASS, MAX_MASS); leadingjetmjj3000_inc_b->Sumw2();
    subleadingjetmjj3000_inc_b = new TH1D( "SubLeadingJetMass_mjj3000_inc_b", "SubLeading Jet Mass for Mjj above 3 TeV for inverted cuts with b", 200, MIN_MASS, MAX_MASS); subleadingjetmjj3000_inc_b->Sumw2();


    // jet masses with double b-tag 
    leadingjetmjj3000_bb = new TH1D( "LeadingJetMass_mjj3000_bb", "Leading Jet Mass for Mjj above 3 TeV with bb", 200, MIN_MASS, MAX_MASS); leadingjetmjj3000_bb->Sumw2();
    subleadingjetmjj3000_bb = new TH1D( "SubLeadingJetMass_mjj3000_bb", "SubLeading Jet Mass for Mjj above 3 TeV with bb", 200, MIN_MASS, MAX_MASS); subleadingjetmjj3000_bb->Sumw2();
    leadingjetmjj3000_inc_bb = new TH1D( "LeadingJetMass_mjj3000_inc_bb", "Leading Jet Mass for Mjj above 3 TeV for inverted cuts with bb", 200, MIN_MASS, MAX_MASS); leadingjetmjj3000_inc_bb->Sumw2();
    subleadingjetmjj3000_inc_bb = new TH1D( "SubLeadingJetMass_mjj3000_inc_bb", "SubLeading Jet Mass for Mjj above 3 TeV for inverted cuts with bb", 200, MIN_MASS, MAX_MASS); subleadingjetmjj3000_inc_bb->Sumw2();

    mjj3000_leadmass = new TH2D("mjj3000_leadmass", "Mjj above 3 TeV vs jet mass", 100, 3000, 6000, 100, 100, 300); //dead modules, overcorrections, non-uniformity

    transverseMass=new TH1D( "TransverseMassMETLepton", "Transverse mass lepton+MET", 600,0,1200 );transverseMass->Sumw2();
    transverseMassJJL=new TH1D( "TransverseMassMETLeptonJJ", "Transverse mass lepton+MET+JJ", nBins-1, xbins ); transverseMassJJL->Sumw2();

    // lepton-jet
   jetlepmass_el=new TH1D( "JetLepMass_el", "Lead jet- electron Mass (standard bins)", nBins-1, xbins); jetlepmass_el->Sumw2();
   jetlepmass_mu=new TH1D( "JetLepMass_mu", "Lead jet - muon Mass (standard bins)", nBins-1, xbins); jetlepmass_mu->Sumw2();
   jetlepmass_lead_el=new TH1D( "JetLepMass_el_fine", "Leadjet-lepton mass to lead jet el (fine bins)", 500,0,1000); jetlepmass_lead_el->Sumw2();
   jetlepmass_lead_mu=new TH1D( "JetLepMass_mu_fine", "Leadjet-lepton mass to lead jet mu (fine bins)", 500,0,1000); jetlepmass_lead_mu->Sumw2();
   jetlepmass_any_el=new TH1D( "JetLepMass_any_el", "Any Jet-elec mass (fine bins)", 500,0,1000); jetlepmass_any_el->Sumw2();
   jetlepmass_any_mu=new TH1D( "JetLepMass_any_mu", "Any Jet-muon mass (fine bins)", 500,0,1000); jetlepmass_any_mu->Sumw2();
 



  // ANN studies
    profrmm  = new TProfile2D("RMMprofile", "RMM profile", glob.mSize, 0, (double)(glob.mSize), glob.mSize, 0, (double)glob.mSize, 0, 1000);
    projrmm = new TH2D("RMMprojection", "RMM projection", glob.mSize, 0, (double)(glob.mSize), glob.mSize, 0, (double)glob.mSize);

    cout << "Initialize RMM profiles" << " mSize=" << glob.mSize << " Names2 size=" << glob.Names2.size() << endl;
    // initialize with 0
    for (int h = 0; h < glob.mSize; h++) {
                for (int w = 0; w < glob.mSize; w++) {
                        int i1=h;
                        int i2=w;
                        profrmm->Fill((glob.Names2.at(i1)).c_str(),  (glob.Names1.at(i2)).c_str(),0);
                        projrmm->Fill((glob.Names2.at(i1)).c_str(),  (glob.Names1.at(i2)).c_str(),0);
                }}


    // MC-based NN
    // all BSM (non-SM)
    jetjetmassNN=new TH1D( "JetJetMassNN", "JetJet Mass for non SM", nBins-1, xbins);jetjetmassNN->Sumw2();
    jetjetmassNN_tev=new TH1D( "JetJetMassNN_tev", "JetJet Mass for non SM in TeV", nBins-1, xbins_tev);jetjetmassNN_tev->Sumw2();
    // for each channel
    jetjetmassNN0=new TH1D( "JetJetMassNN0", "JetJet Mass after NN index 0 Dijets", nBins-1, xbins);jetjetmassNN0->Sumw2();
    jetjetmassNN0_tev=new TH1D( "JetJetMassNN0_tev", "JetJet Mass after NN index 0 in TeV Dijets", nBins-1, xbins_tev);jetjetmassNN0_tev->Sumw2();
    jetjetmassNN1=new TH1D( "JetJetMassNN1", "JetJet Mass after NN index 1 t/W+jets/s-top", nBins-1, xbins);jetjetmassNN1->Sumw2();
    jetjetmassNN1_tev=new TH1D( "JetJetMassNN1_tev", "JetJet Mass after NN index 1 in TeV t/W+jets/s-top", nBins-1, xbins_tev);jetjetmassNN1_tev->Sumw2();
    jetjetmassNN2=new TH1D( "JetJetMassNN2", "JetJet Mass after NN index 2 Charged Higgs", nBins-1, xbins);jetjetmassNN2->Sumw2();
    jetjetmassNN2_tev=new TH1D( "JetJetMassNN2_tev", "JetJet Mass after NN index 2 in TeV Charged Higgs", nBins-1, xbins_tev);jetjetmassNN2_tev->Sumw2();
    jetjetmassNN3=new TH1D( "JetJetMassNN3", "JetJet Mass after NN index 3 Wprime SSM", nBins-1, xbins);jetjetmassNN3->Sumw2();
    jetjetmassNN3_tev=new TH1D( "JetJetMassNN3_tev", "JetJet Mass after NN index 3 in TeV Wprime SSM", nBins-1, xbins_tev);jetjetmassNN3_tev->Sumw2();
    jetjetmassNN4=new TH1D( "JetJetMassNN4", "JetJet Mass after NN index 4  rhoT", nBins-1, xbins);jetjetmassNN4->Sumw2();
    jetjetmassNN4_tev=new TH1D( "JetJetMassNN4_tev", "JetJet Mass after NN index 4 in TeV  rhoT", nBins-1, xbins_tev);jetjetmassNN4_tev->Sumw2();
    jetjetmassNN5=new TH1D( "JetJetMassNN5", "JetJet Mass after NN index 5 DM WZ", nBins-1, xbins);jetjetmassNN5->Sumw2();
    jetjetmassNN5_tev=new TH1D( "JetJetMassNN5_tev", "JetJet Mass after NN index 5 in TeV DM WZ", nBins-1, xbins_tev);jetjetmassNN5_tev->Sumw2();
    jetjetmassNN6=new TH1D( "JetJetMassNN6", "JetJet Mass after NN index 6  DM ZZ", nBins-1, xbins);jetjetmassNN6->Sumw2();
    jetjetmassNN6_tev=new TH1D( "JetJetMassNN6_tev", "JetJet Mass after NN index 6 in TeV DM ZZ", nBins-1, xbins_tev);jetjetmassNN6_tev->Sumw2();

    // MC for dijets replaced by data2015
    jetjetmassNND=new TH1D( "JetJetMassNND", "JetJet Mass for non SM", nBins-1, xbins);jetjetmassNND->Sumw2();
    jetjetmassNND_tev=new TH1D( "JetJetMassNND_tev", "JetJet Mass for non SM in TeV", nBins-1, xbins_tev);jetjetmassNND_tev->Sumw2();
    // fo each channel
    jetjetmassNND0=new TH1D( "JetJetMassNND0", "JetJet Mass after NN index 0 Dijets", nBins-1, xbins);jetjetmassNND0->Sumw2();
    jetjetmassNND0_tev=new TH1D( "JetJetMassNND0_tev", "JetJet Mass after NN index 0 in TeV Dijets", nBins-1, xbins_tev);jetjetmassNND0_tev->Sumw2();
    jetjetmassNND1=new TH1D( "JetJetMassNND1", "JetJet Mass after NN index 1 t/W+jets/s-top", nBins-1, xbins);jetjetmassNND1->Sumw2();
    jetjetmassNND1_tev=new TH1D( "JetJetMassNND1_tev", "JetJet Mass after NN index 1 in TeV t/W+jets/s-top", nBins-1, xbins_tev);jetjetmassNND1_tev->Sumw2();
    jetjetmassNND2=new TH1D( "JetJetMassNND2", "JetJet Mass after NN index 2 Charged Higgs", nBins-1, xbins);jetjetmassNND2->Sumw2();
    jetjetmassNND2_tev=new TH1D( "JetJetMassNND2_tev", "JetJet Mass after NN index 2 in TeV Charged Higgs", nBins-1, xbins_tev);jetjetmassNND2_tev->Sumw2();
    jetjetmassNND3=new TH1D( "JetJetMassNND3", "JetJet Mass after NN index 3 Wprime SSM", nBins-1, xbins);jetjetmassNND3->Sumw2();
    jetjetmassNND3_tev=new TH1D( "JetJetMassNND3_tev", "JetJet Mass after NN index 3 in TeV Wprime SSM", nBins-1, xbins_tev);jetjetmassNND3_tev->Sumw2();
    jetjetmassNND4=new TH1D( "JetJetMassNND4", "JetJet Mass after NN index 4  rhoT", nBins-1, xbins);jetjetmassNND4->Sumw2();
    jetjetmassNND4_tev=new TH1D( "JetJetMassNND4_tev", "JetJet Mass after NN index 4 in TeV  rhoT", nBins-1, xbins_tev);jetjetmassNND4_tev->Sumw2();
    jetjetmassNND5=new TH1D( "JetJetMassNND5", "JetJet Mass after NN index 5 DM WZ", nBins-1, xbins);jetjetmassNND5->Sumw2();
    jetjetmassNND5_tev=new TH1D( "JetJetMassNND5_tev", "JetJet Mass after NN index 5 in TeV DM WZ", nBins-1, xbins_tev);jetjetmassNND5_tev->Sumw2();
    jetjetmassNND6=new TH1D( "JetJetMassNND6", "JetJet Mass after NN index 6  DM ZZ", nBins-1, xbins);jetjetmassNND6->Sumw2();
    jetjetmassNND6_tev=new TH1D( "JetJetMassNND6_tev", "JetJet Mass after NN index 6 in TeV DM ZZ", nBins-1, xbins_tev);jetjetmassNND6_tev->Sumw2();

    // alternative data
    jetjetmassNNDD=new TH1D( "JetJetMassNNDD", "JetJet Mass for non SM", nBins-1, xbins);jetjetmassNNDD->Sumw2();
    jetjetmassNNDD_tev=new TH1D( "JetJetMassNNDD_tev", "JetJet Mass for non SM in TeV", nBins-1, xbins_tev);jetjetmassNNDD_tev->Sumw2();
    jetjetmassNNDD0=new TH1D( "JetJetMassNNDD0", "JetJet Mass after NN index 0 Dijets", nBins-1, xbins);jetjetmassNNDD0->Sumw2();
    jetjetmassNNDD0_tev=new TH1D( "JetJetMassNNDD0_tev", "JetJet Mass after NN index 0 in TeV Dijets", nBins-1, xbins_tev);jetjetmassNNDD0_tev->Sumw2();
    jetjetmassNNDD1=new TH1D( "JetJetMassNNDD1", "JetJet Mass after NN index 1 t/W+jets/s-top", nBins-1, xbins);jetjetmassNNDD1->Sumw2();
    jetjetmassNNDD1_tev=new TH1D( "JetJetMassNNDD1_tev", "JetJet Mass after NN index 1 in TeV t/W+jets/s-top", nBins-1, xbins_tev);jetjetmassNNDD1_tev->Sumw2();
    jetjetmassNNDD2=new TH1D( "JetJetMassNNDD2", "JetJet Mass after NN index 2 Charged Higgs", nBins-1, xbins);jetjetmassNNDD2->Sumw2();
    jetjetmassNNDD2_tev=new TH1D( "JetJetMassNNDD2_tev", "JetJet Mass after NN index 2 in TeV Charged Higgs", nBins-1, xbins_tev);jetjetmassNNDD2_tev->Sumw2();
    jetjetmassNNDD3=new TH1D( "JetJetMassNNDD3", "JetJet Mass after NN index 3 Wprime SSM", nBins-1, xbins);jetjetmassNNDD3->Sumw2();
    jetjetmassNNDD3_tev=new TH1D( "JetJetMassNNDD3_tev", "JetJet Mass after NN index 3 in TeV Wprime SSM", nBins-1, xbins_tev);jetjetmassNNDD3_tev->Sumw2();
    jetjetmassNNDD4=new TH1D( "JetJetMassNNDD4", "JetJet Mass after NN index 4  rhoT", nBins-1, xbins);jetjetmassNNDD4->Sumw2();
    jetjetmassNNDD4_tev=new TH1D( "JetJetMassNNDD4_tev", "JetJet Mass after NN index 4 in TeV  rhoT", nBins-1, xbins_tev);jetjetmassNNDD4_tev->Sumw2();
    jetjetmassNNDD5=new TH1D( "JetJetMassNNDD5", "JetJet Mass after NN index 5 DM WZ", nBins-1, xbins);jetjetmassNNDD5->Sumw2();
    jetjetmassNNDD5_tev=new TH1D( "JetJetMassNNDD5_tev", "JetJet Mass after NN index 5 in TeV DM WZ", nBins-1, xbins_tev);jetjetmassNNDD5_tev->Sumw2();
    jetjetmassNNDD6=new TH1D( "JetJetMassNNDD6", "JetJet Mass after NN index 6  DM ZZ", nBins-1, xbins);jetjetmassNNDD6->Sumw2();
    jetjetmassNNDD6_tev=new TH1D( "JetJetMassNNDD6_tev", "JetJet Mass after NN index 6 in TeV DM ZZ", nBins-1, xbins_tev);jetjetmassNNDD6_tev->Sumw2();


    // 100% MC based NN
    NN0 = new TH1D( "NN0", "Output from NN at index 0 dijets", 100, 0, 1.1);
    NN1 = new TH1D( "NN1", "Output from NN at index 1 t/W+jets/s-top", 100, 0, 1.1);
    NN2 = new TH1D( "NN2", "Output from NN at index 2 Charged Higgs", 100, 0, 1.1);
    NN3 = new TH1D( "NN3", "Output from NN at index 3 Wprime SSM", 100, 0, 1.1);
    NN4 = new TH1D( "NN4", "Output from NN at index 4 rhoT techo", 100, 0, 1.1);
    NN5 = new TH1D( "NN5", "Output from NN at index 5 DM WZ", 100, 0, 1.1);
    NN6 = new TH1D( "NN6", "Output from NN at index 6 DM ZZ", 100, 0, 1.1);

    // dijet sample replaced by a fraction of 2015
    NND0 = new TH1D( "NND0", "Output from NN at index 0 dijets", 100, 0, 1.1);
    NND1 = new TH1D( "NND1", "Output from NN at index 1 t/W+jets/s-top", 100, 0, 1.1);
    NND2 = new TH1D( "NND2", "Output from NN at index 2 Charged Higgs", 100, 0, 1.1);
    NND3 = new TH1D( "NND3", "Output from NN at index 3 Wprime SSM", 100, 0, 1.1);
    NND4 = new TH1D( "NND4", "Output from NN at index 4 rhoT techo", 100, 0, 1.1);
    NND5 = new TH1D( "NND5", "Output from NN at index 5 DM WZ", 100, 0, 1.1);
    NND6 = new TH1D( "NND6", "Output from NN at index 6 DM ZZ", 100, 0, 1.1);

 // dijet sample replaced by a fraction of data (alternative data) 
    NNDD0 = new TH1D( "NNDD0", "Output from NN at index 0 dijets", 100, 0, 1.1);
    NNDD1 = new TH1D( "NNDD1", "Output from NN at index 1 t/W+jets/s-top", 100, 0, 1.1);
    NNDD2 = new TH1D( "NNDD2", "Output from NN at index 2 Charged Higgs", 100, 0, 1.1);
    NNDD3 = new TH1D( "NNDD3", "Output from NN at index 3 Wprime SSM", 100, 0, 1.1);
    NNDD4 = new TH1D( "NNDD4", "Output from NN at index 4 rhoT techo", 100, 0, 1.1);
    NNDD5 = new TH1D( "NNDD5", "Output from NN at index 5 DM WZ", 100, 0, 1.1);
    NNDD6 = new TH1D( "NNDD6", "Output from NN at index 6 DM ZZ", 100, 0, 1.1);
 
    // RMM outputs
    projRMM = new TH1D( "projRMM", "None zero RMM in 1D", 2000, 0, 2000);
    projRMMfilled = new TH1D( "projRMMfilled", "None zero RMM in 1D filled in inputNN", 2000, 0, 2000);
    m_tree  = new TTree("inputNN","inputNN");
    m_tree->SetAutoFlush(100000);
    m_tree->Branch("proj",   &m_proj);
    m_tree->Branch("proj_index1",   &m_proj_index1);
    m_tree->Branch("proj_index2",   &m_proj_index2);
    m_tree->Branch("run",   &m_run);
    m_tree->Branch("event", &m_event);
    m_tree->Branch("weight",  &m_weight);


    // RMM dimensions
    h_dimensions = new TH1D("dimensions", "(1)maxNumber,(2)maxTypes, (3)mSize",5,0,5); 
    h_dimensions->Fill(1,(float)glob.maxNumber);
    h_dimensions->Fill(2,(float)glob.maxTypes);
    h_dimensions->Fill(3,(float)glob.mSize);
 
}




/*
* Destructor
**/

Histo::~Histo() { }



// write histograms
void Histo::finalize() {

      cout << "Histo::finalize() is called" << endl;
      cout << "Print the numbers of events after the selection cuts (\"debug\" histogram)" << endl;
     // debug->Print();
     for (Int_t j=1; j<debug->GetNbinsX(); j++) {
      cout << " ->Cut Nr=" << j << " accepted events=" << debug->GetBinContent(j+1) << endl;
     }

    cout << "--> Number of events requested:" <<  glob.MaxEvents  << endl;
    cout << "--> Number of events processed:" <<  glob.nev  << endl;
    cout << "--> Number of events  selected:" <<  glob.TotalEvents << endl;

    cout << "\n\n-- Write output file=" << ffile << endl;
    cout << "\n\n";
    RootFile->Write();
    RootFile->Print();
    RootFile->Close();
    return;

}



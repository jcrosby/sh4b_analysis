/*
 *      Loop.cxx
 *
 *      Copyright 2010 Sergei Chekanov <chakanau@hep.anl.gov> ANL
 *
 *      This program is free software; you can redistribute it and/or modify
 *      it under the terms of the GNU General Public License as published by
 *      the Free Software Foundation; either version 2 of the License, or
 *      (at your option) any later version.
 *
 *      This program is distributed in the hope that it will be useful,
 *      but WITHOUT ANY WARRANTY; without even the implied warranty of
 *      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *      GNU General Public License for more details.
 *
 *      You should have received a copy of the GNU General Public License
 *      along with this program; if not, write to the Free Software
 *      Foundation, Inc., 51 Franklin S1kreet, Fifth Floor, Boston,
 *      MA 02110-1301, USA.
 */



#include "Ana.h"
#include "Global.h"
#include "SystemOfUnits.h"
#include "Histo.h"
#include "TSpline.h"
#include<iostream>
#include<fstream>
#include<stdlib.h>
#include<TCanvas.h>

using namespace std;

extern Global glob;
extern Histo  h;

auto c = new TCanvas("c1","c1",600,600);

// project event
float**  projectevent(const float  CMS, const int maxN, const int maxNumberTypes, const vector<LParticle> LR_jets, const vector<LParticle> LR_bbjets);



// https://arxiv.org/pdf/1612.07231.pdf
// cut on electron
// 2016 recomendations
const double electronETA=2.47;

// photon Eta cut 
const double photonETA=2.37;

// https://arxiv.org/pdf/1612.07231.pdf
// 2016 recommendation
const double muonETA=2.5;

// angle between lepton and dijet system 
// not used for anything
const double dTheta=0.5; 

// dR between lepton and jet center (0.4 for antiKT4)
const double dRmax=0.4;

// overlap removal. pT(l)>ptoverlap*pT(jet) when matched with lepton 
// This is motivated by jet energy resolution of 15% near pT(jet)>20 GeV
// Generally, the resolution is 10% near the region of interest pT>60 GeV (lepton or true jet).
// We assume Sigma, i.e. 10%. This is overestimate since resolution is smaller for EM jet 
// https://arxiv.org/abs/1210.6210
// We use ATL-COM-PHYS-2014-451 (https://cds.cern.ch/record/1700874/)
// recommendation for overlap removal:
// remove jets with dR<0.2
// remove electrons with dR<0.4
const double dRoverlap_jets=0.2; 
const double dRoverlap_lepton=0.4;


// jet mass cut for cross checking higgs+
const double JetMassCut=80;

// works for all years 
// not used for final selections 
const double PToverlap=0.3;  // determinned by this study 
const int    maxTracksMU=3;   // max value of tracks for overlap with muons
const int    maxTracksEL=10;   // max value of tracks for overlap with electrons 

// fraction of e/mu momentum carried by neutrino 
const double EMissFrac=1.0;   // determinned by this study? 

//pi2
const double PI2=2*TMath::Pi();
const double PI=TMath::Pi();


// Event loop.
void Ana::Loop()
{
   if (fChain == 0) return;

   Long64_t nentries = fChain->GetEntriesFast();
   cout << " -> number of entries =" << nentries << endl;
   Long64_t nbytes = 0, nb = 0;
   for (Long64_t n=0; n<nentries; n++) {
     
      if (glob.MaxEvents>0) 
         if (glob.nev>=glob.MaxEvents) break;

      Long64_t ientry = LoadTree(n);
      if (ientry < 0) break;
      nb = fChain->GetEntry(n);   nbytes += nb;

       if (glob.nev<=10 &&
       (glob.nev<=100 && (glob.nev%10) == 0) ||
       (glob.nev<=10000 && (glob.nev%1000) == 0)  ||
       (glob.nev>=10000 && (glob.nev%10000) == 0) ) {
          cout << "Events= " << glob.nev << " N(ele)=" << glob.Nelec << " N(mu)="<< glob.Nmuon << endl; }
      
      int runNum=0; 
      int evNum=0;

#if not montecarlo
      runNum=runNumber;
      evNum=eventNumber;
      glob.data_year=2015;
      if (runNumber>=297730) glob.data_year=2016;
      if (runNumber>=325713) glob.data_year=2017;
      if (runNumber>=348895) glob.data_year=2018;
      glob.isMonteCarlo=false;


      // run3
      if (runNumber>=420000) {
             glob.data_year=2022;
             glob.CMS = 13600; // redefine energy 
      };

#endif


       // counter
      glob.nev++;
     // weight in MC (data=1)
     double weight=1.0;
/* 
#if montecarlo
        evNum=EventNumber;
        runNum=RunNumber; 
        weight= AnalysisWeight / glob.SumOfWeights; //  = m_eventInfo->mcEventWeight(); 
        glob.isMonteCarlo=true;

        // nominalWeight is not used 
        //cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PMGTools/PMGxsecDB_mc16.txt"
        // read MC cross section 
        if (glob.firstEvent==true) {
          float CrossSection=glob.GetXsec(runNum, "PMGxsecDB_mc16.txt"); 
           glob.firstEvent=false;
           cout << "= Extracted AMI cross section * filter_eff for MC for run =" << runNum << " is " << CrossSection << " pb"<< endl;
           h.cross_section->Fill(1,CrossSection);
        };
#endif
 */
      // skip bad events
      h.debug->Fill("Good events",1.0);
      h.hweights->Fill(weight);
      //h.hweights_nominal->Fill( nominalWeight );

      vector<LParticle> selected;
      vector<LParticle> alljets;
      vector<LParticle> LR_jets; // Large-R jets
      vector<LParticle> LR_bbjets; // Large-R bb jets


      // Large-R jet Triggers
      bool passHLT_largeRjet=(trigPassed_HLT_j420_a10r_L1J100) || (trigPassed_HLT_j420_a10_lcw_L1J100); 

      //cout << "PASS MET " << METTrigPassed << " " <<  passHLT_MET << " type=" << glob.type << endl; 


     if ( passHLT_largeRjet ) h.triggers->Fill(0);

     // apply to data only. This will reduce some biases for background MC + extra statistics  
     // disable it for signal MC
     if (glob.isMonteCarlo == false) {
   
       if (glob.type == 0) {
             if (passHLT_largeRjet==false) continue;
       }; 

     }// end data 

      h.debug->Fill("Trigger passed",1.0);

      if (CutEvent(ientry) <0) continue;
      h.debug->Fill("Event cut",1.0);

      for(unsigned int  i = 0; i < recojet_antikt10UFO_NOSYS_pt->size(); i++){
        double pt=recojet_antikt10UFO_NOSYS_pt->at(i)/GeV;
        double phi=recojet_antikt10UFO_NOSYS_phi->at(i);
        double eta=recojet_antikt10UFO_NOSYS_eta->at(i);
        //double jete=jet_e->at(i)/GeV;
        double jetm=recojet_antikt10UFO_NOSYS_m->at(i)/GeV; // do I need to scale?
        
        cout << "jet MASS: " << jetm << endl;
  
        h.cutflow_jets->Fill("all jets",1);
        if( pt < glob.PT_CUT ) continue;
        h.cutflow_jets->Fill("pT cut",1);
        if( abs(eta) > glob.ETA_CUT ) continue;
        h.cutflow_jets->Fill("Eta cut",1);
        
        //cout << "HERE1" << endl;

        TLorentzVector ljet;
        ljet.SetPtEtaPhiM(pt,eta,phi,jetm);
        double y_jet=ljet.Rapidity();

        // Get xbb disc.
        float ph = recojet_antikt10UFO_NOSYS_Xbb2020v3_Higgs->at(i);
        float ptop = recojet_antikt10UFO_NOSYS_Xbb2020v3_Top->at(i);
        float pqcd = recojet_antikt10UFO_NOSYS_Xbb2020v3_QCD->at(i);
        double xbb_disc = log10(ph / ((1-0.25)*pqcd +(0.25)*ptop));

        bool isbbJet = false;
        if (xbb_disc > 2.44) isbbJet =true;
        //cout << "HERE2" << endl;
        LParticle p;
        p.SetP(ljet);
        p.SetType( 0  );
        //p.SetStatus(  jet_JvtPass_Tight->at(i) );
        p.SetParent(0); // save EM pt in MeV units 
        p.SetCharge(0);
        p.SetParameter(isbbJet   ); // 60% 
        p.SetParameter(isbbJet   ); // 60%
        p.SetParameter(recojet_antikt10UFO_NOSYS_m->at(i)); 
        //p.SetParameter(jet_IsCJet->at(i)  ); // C-jet

        alljets.push_back(p);

        //if(!jet_passOR->at(i)) continue;
        //cout << "HERE3" << endl;

        h.jet_pt->Fill(pt,weight);
        h.jet_eta->Fill(eta,weight);

        p.SetType(0); // default jets
        // only good jets
        selected.push_back(p);

        // 77% B-jets 
        if (isbbJet) LR_bbjets.push_back(p);
        else LR_jets.push_back(p); // light jets 
                 

        }; // end jets
 
    // sort in pT
    if (alljets.size()>1) std::sort(alljets.begin(), alljets.end(), greater<LParticle>() ) ;
    if (selected.size()>1) std::sort(selected.begin(), selected.end(), greater<LParticle>() ) ;
    if (LR_bbjets.size()>1) std::sort(LR_bbjets.begin(), LR_bbjets.end(), greater<LParticle>() ) ;
    if (LR_jets.size()>1) std::sort(LR_jets.begin(), LR_jets.end(), greater<LParticle>() ) ;
    //cout << "HERE4" << endl;
/* 
#if montecarlo

   // no anomaly be default
   int AnomalyType=0;

   // Run: A_MC16d_SIGNAL_WprimeSSM_anomaly1
   if (AnomalyType == 1) {
   // Anomaly I
   // Make jets > 2 as photons
   for (int i=2; i<Ljets.size(); i++) {
      LParticle ghost=Ljets.at(i);
      TLorentzVector LM=ghost.GetP();
      TLorentzVector lm;
      lm.SetPtEtaPhiM(LM.Perp(),LM.Eta(),LM.Phi(), 0 );
      double y_m=lm.Rapidity();
      LParticle p;
      p.SetP(lm);
      selectedphotons.push_back(p);
      // remove this last jet
    }

     // remove replaced jets
     if (Ljets.size()>2) Ljets.erase(std::next(Ljets.begin(), 2), std::next(Ljets.begin(), Ljets.size()));

     if (selectedphotons.size()>1) std::sort(selectedphotons.begin(), selectedphotons.end(), greater<LParticle>() ) ;
     if (Ljets.size()>1) std::sort(Ljets.begin(), Ljets.end(), greater<LParticle>() ) ;
      cout << "Ljets size=" << Ljets.size() << " Photons=" << selectedphotons.size()  << endl;
   };

    // Run: A_MC16d_SIGNAL_WprimeSSM_anomaly2 
    if (AnomalyType == 2) {
    // Anomaly II
    // Make > 2 jets  as b-jets  
    for (int i=2; i<Ljets.size(); i++) {
      LParticle ghost=Ljets.at(i);
      TLorentzVector LM=ghost.GetP();
      TLorentzVector lm;
      lm.SetPtEtaPhiM(LM.Perp(),LM.Eta(),LM.Phi(), LM.M() );
      double y_m=lm.Rapidity();
      LParticle p;
      p.SetP(lm);
      Bjets.push_back(p);
      }

     // remove L-jets 
     if (Ljets.size()>2) Ljets.erase(std::next(Ljets.begin(), 2), std::next(Ljets.begin(), Ljets.size()));

     cout << "Ljets size=" << Ljets.size() << " B-jets size=" << Bjets.size() << endl;
     if (Bjets.size()>1) std::sort(Bjets.begin(), Bjets.end(), greater<LParticle>() ) ;
     if (Ljets.size()>1) std::sort(Ljets.begin(), Ljets.end(), greater<LParticle>() ) ;
   }


   // low multiplicity events
   if (AnomalyType == 3) {
     if (Ljets.size()>2) Ljets.erase(std::next(Ljets.begin(), 2), std::next(Ljets.begin(), Ljets.size()));
     if (selectedmuons.size()>1) selectedmuons.erase(std::next(selectedmuons.begin(), 2), std::next(selectedmuons.begin(), selectedmuons.size()));
     if (selectedelectrons.size()>1) selectedelectrons.erase(std::next(selectedelectrons.begin(), 2), std::next(selectedelectrons.begin(), selectedelectrons.size()));
     Bjets.erase(std::next(Bjets.begin(), 0), std::next(Bjets.begin(), Bjets.size()));
     selectedphotons.erase(std::next(selectedphotons.begin(), 0), std::next(selectedphotons.begin(), selectedphotons.size()));
     cout << "Ljets size=" << Ljets.size() << " Photons=" << selectedphotons.size()  << endl;
   };



#endif

 */
//************************************************************************************
// main selections
//************************************************************************************

    // Leading Large-R jet > 450 GeV
    // Subleading Large-R jet > 250 GeV 
    if (glob.type == 0) { 
       if(selected.size() < 2) continue;
        LParticle j1=selected.at(0);
        TLorentzVector L1=j1.GetP();
        if (L1.Perp()<450) continue;
        
        LParticle j2=selected.at(0);
        TLorentzVector L2=j2.GetP();
        if (L2.Perp()<250) continue;
    }
     // look at triggers after off-line selection
    if (  passHLT_largeRjet ) h.triggers_selected->Fill(0);

    //cout << "HERE5" << endl;
//*********************************************************************************************
//*********************************************************************************************

    h.n_jet->Fill(selected.size(),weight);

//************************************************************************************
//  dijet block 
//************************************************************************************
      //cout << "HERE6" << endl;
      if (selected.size()>1)  { // at least 2 jets 
      LParticle p1=selected.at(0);
      LParticle p2=selected.at(1);
      TLorentzVector LP1=p1.GetP();
      TLorentzVector LP2=p2.GetP();
      double YStar=abs(LP1.Rapidity()-LP2.Rapidity())/2.0;

      vector<double> par1=p1.GetParameters();
      vector<double> par2=p2.GetParameters();
      // 77%
      double jet1_xbb=par1[1];
      double jet2_xbb=par2[1];

      double pt1=LP1.Perp();
      double pt2=LP2.Perp();
      double eta1=LP1.Eta();
      double eta2=LP2.Eta();
      double phi1=LP1.Phi();
      double phi2=LP2.Phi();
      double mass1=LP1.M();
      double mass2=LP2.M();
      h.debug->Fill("LeadingJetPt",1.0);
      //if (abs(yStar)>0.6) continue;
      h.debug->Fill("y*",1.0);
      //cout << "HERE7" << endl;
      h.LR_leading_jet_pt->Fill(pt1,weight);
      h.LR_leading_jet_m->Fill(mass1,weight);
      h.LR_leading_jet_eta->Fill(eta1,weight);
      h.LR_leading_jet_phi->Fill(phi1,weight);
      // delta phi
      //h.dphi->Fill(deltaPhi,weight);
      h.njet->Fill(recojet_antikt10UFO_NOSYS_pt->size(),weight);
      h.ystar->Fill(YStar,weight);
      // add 2 vectors
      TLorentzVector PP=LP1+LP2;
      //cout << PP.M() << endl;
      double mass_jj=PP.M();
      // inclusive event with leptons 
      h.jetjetmass->Fill(mass_jj,weight);
      h.jetjetmass_tev->Fill(mass_jj/GeV,weight); // same in TeV 
      //cout << "HERE8" << endl;

      if (YStar<0.6) h.jetjetmass_ystar->Fill(mass_jj,weight);

     if (jet1_xbb>0 ||  jet2_xbb>0) {
        h.jetjetmass_b->Fill(mass_jj,weight);
      }

     if (jet1_xbb>0) h.leadingjetpt_b->Fill(pt1,weight);
     if (jet2_xbb>0) h.secondjetpt_b ->Fill(pt2,weight);


      if (jet1_xbb>0 && jet2_xbb>0) {
        h.jetjetmass_bb->Fill(mass_jj,weight);
      }
      //cout << "HERE9" << endl;

       // ---------------------------------------------------

       // fill here some masses for debugging
       // jj
        if ( LR_jets.size()>1){
            LParticle p1= LR_jets.at(0);
            LParticle p2= LR_jets.at(1);
            TLorentzVector PP=p1.GetP()+p2.GetP();
            h.Mjj->Fill(PP.M(), weight);
        }

      // j+b
        if ( LR_bbjets.size()>0 && LR_jets.size()>0){
            LParticle p1= LR_jets.at(0);
            LParticle p2= LR_bbjets.at(0);
            TLorentzVector PP=p1.GetP()+p2.GetP();
            h.Mjb->Fill(PP.M(), weight);
        }

     // b+b
        if ( LR_bbjets.size()>1){
            LParticle p1= LR_bbjets.at(0);
            LParticle p2= LR_bbjets.at(1);
            TLorentzVector PP=p1.GetP()+p2.GetP();
            h.Mbb->Fill(PP.M(), weight);
        }
      }
      //cout << "HERE10" << endl;
// start again single leptons

#if FANN

  h.m_proj_index1.clear();
  h.m_proj_index2.clear();
  h.m_proj.clear();
  h.m_run=runNum;
  h.m_event=evNum; 
  h.m_weight=weight;

  bool fillTrainSample=true;

   //cout << "HERE11" << endl;

  
   // making RMM
   float** projArray =  projectevent(glob.CMS, glob.maxNumber, glob.maxTypes, LR_jets, LR_bbjets);
   //cout << "line1" << endl;
   int count=0;
   int non_empty=0;
   //TMatrixD* printMatrix(glob.mSize,glob.mSize);
   for (int w1 = 0; w1 < glob.mSize; w1++) {
          //cout << "line2" << w1 <<endl;
          for (int h1 = 0; h1 < glob.mSize; h1++) {
                  //cout << "line3" << h1 << endl;
                  float dd=projArray[w1][h1];
                  if (h1<w1)   dd=dd*glob.angNorm; // decrease angles by 0.01 
                  // suppress 0
                  int i1=h1;
                  int i2=glob.mSize-w1-1;
                  h.profrmm->Fill((glob.Names2.at(i1)).c_str(),  (glob.Names1.at(i2)).c_str(),dd);
                  if (dd>0) h.projrmm->Fill((glob.Names2.at(i1)).c_str(),  (glob.Names1.at(i2)).c_str(),dd);
                  //cout << "line4" << h1 << endl;
                  //cout << "HERE12" << endl;

                  // we exclude 5 cells:
                  // (1,1) - E(lead)
                  // (2,1) - M(jj)
                  // (1+maxNumber,1) - Mjj for for light-jet + b-jets 
                  // (1+maxNumber,1+maxNumber) - E(b-jet)  
                  // (2+maxNumber,1+maxNumber) - Mjj for for 2-b jets 
                  // Note: when preparing RMM for training, we do not to exclude such cells 
                  // since the will be excluded by nn_prepay python scripts
                  // this is 1st jet E and M(jj) 
                  float nn_input=dd;
                  //if ( (w1==1 && h1==1) ||  (w1==2 && h1==1) ) nn_input=0;
                  //if ( (w1==(1+glob.maxNumber) && h1==1) ) nn_input=0;
                  //if ( (w1==(1+glob.maxNumber) && h1==(1+glob.maxNumber)) ) nn_input=0;
                  //if ( (w1==(2+glob.maxNumber) && h1==(1+glob.maxNumber)) ) nn_input=0;

                  h.projRMM->Fill(count,dd); // 1D projection of RMM 
                  count++;

                   if (dd>0 && glob.nevfill < glob.max_events4ANN && fillTrainSample==true) {
                   h.m_proj.push_back(dd);
                   h.m_proj_index1.push_back( w1 );
                   h.m_proj_index2.push_back( h1 );
                   h.projRMMfilled->Fill(count,dd);
                   non_empty++;
                  }
                  //printMatrix[w1][h1] = dd;

     }};

     //cout << "HERE13" << endl;
  // add RMM to ROOT file for future training for 2015 data and MC. 
  if (glob.nevfill < glob.max_events4ANN && non_empty>0 && fillTrainSample==true) {
            h.m_tree->Fill();
            glob.nevfill++;
  }

   if (glob.printevent == true && n==glob.printeventNum) {
      pmat = new TH2D("Print Matrix", "Print Matrix",projArray);
      pmat->Draw("colz");
      c->SaveAs("RMM_event.pdf");

   }
   
   //Free the array of pointers
   for (int w1 = 0; w1 < glob.mSize; w1++)  delete[] projArray[w1];
   delete[] projArray; 
#endif

   

   cout << "HERE14" << endl;
   glob.TotalEvents++;
   //if (glob.TotalEvents%10000 ==0 ) cout << "Nr muons="<< glob.Nmuon << " Nr electrons=" << glob.Nelec << endl;

   }//end loop over all events
}//end loop method


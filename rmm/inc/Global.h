/***************************************************************************
 *            global.h
 *
 *  Fri Sep 21 15:58:02 2007
 *  Copyright  2007  chekanov
 *  chekanov@mail.desy.de
 ****************************************************************************/




using namespace std;
#include<iostream>
#include <iostream>              
#include <fstream>
#include <sstream>

// S.Chekanov (ANL)
#include "TFile.h"
#include "TRandom.h"
#include "TStopwatch.h"
#include "TSpline.h"
#include <string>
#include <vector>
#include <map>

//#include "CalibrationDataInterface/CalibrationDataInterfaceROOT.h"
//#include "CalibrationDataInterface/CalibrationDataVariables.h"
//#include "CalibrationDataInterface/Uncertainty.h"

//using Analysis::CalibrationDataVariables;
//using Analysis::CalibrationDataContainer;
//using Analysis::UncertaintyResult;

class Global {

  public:


//    Analysis::CalibrationDataInterfaceROOT calib; 
//    Analysis::CalibrationDataVariables ajet;
//    Analysis::Uncertainty uncertainty = Analysis::Total;

    Global ();
    virtual ~Global ();

    int TotalEvents;
    void getNtuples(string name);
    void getIni();
    void getLumi();
    int nev;			// events processed 
    int nevfill;                // count for RMM filled events 
    vector <string> ntup;	// ntuple list

    int max_events4ANN;

    TFile *RootFile;
    TStopwatch timer;
    char *ffile;
    int MaxEvents;
    int debug;
    int systematics;
    int type; 
    int gen_events;
    int data_year;
    int Nelec;
    int Nmuon;
    bool isMonteCarlo;
    double SumOfWeights;
    bool printevent;
    int printeventNum;

    TRandom*  grandom;

    bool firstEvent;
 
    //set<int> myruns;
    //set<double> mylumi;


    // RMM settings
    std::vector<string> Names1;
    std::vector<string> Names2;
    float  angNorm; // factor to increase weights of angles compare to masses
    int maxNumber; // max number for each object (MET is not counted)
    int maxTypes;  // max numbers of types (met not counted)
    int mSize;
    float CMS; // in GeV;
    // -------------------------------


    double PT_LEPTON_LEAD;
    double MJJ_CUT;
    double PT_CUT;
    double PT_LEPTON;
    double PT_PHOTON; 
    double ETA_CUT;
    TSpline3* line;
    TSpline3* line1;
    int m_run, m_evt;
    int   LumiBlockNumber;
    int   BCIDbeamCrossId;
    double  sumOfWeights;
 
// read file with cross section 
// /cvmfs/atlas.cern.ch/repo/sw/database/GroupData/dev/PMGTools/PMGxsecDB_mc16.txt
double GetXsec(int runnumber, string xsecfile) {
  ifstream xseclist;
  xseclist.open(xsecfile.c_str());
  bool found = false;
  double xsec, kfact, filteff;
  xsec = kfact = filteff = 0;
  string line;
  while (std::getline(xseclist, line) && !found) {
    std::istringstream iss(line);
    int a;
    string desc = "";
    float b, c, d;
    if (!(iss >> a >> desc >> b >> c >> d)) {
      continue;
    } // error
    if (a == runnumber) {
      // std::cout << " x-sec, k-factor and filt eff for the run from SUSYTools
      // file " <<b << " " << c  << " " << d << std::endl;
      xsec = b;
      kfact = c;
      filteff = d;
      found = true;
    }
  }
  xseclist.close();
  return xsec * kfact * filteff;
}
 

    void print_init() {
        cout << "\n\n  --Job input--: " << endl;
        cout << "   Max events=" << MaxEvents <<
                "\n   Debug=" << debug << 
                "\n   Type=" << type <<
                "\n   Systematics=" << systematics << endl; 
    };



     void print_cuts() {
        cout << "\n\n  --Selected cuts--: " << endl;
        cout << "   PT_LEPTON=" << PT_LEPTON <<
                "\n   Jet PT=" << PT_CUT <<
                "\n   Jet ETA=" << ETA_CUT << endl; 
    };
 

};


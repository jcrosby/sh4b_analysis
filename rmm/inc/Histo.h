/***************************************************************************
 *            histo.h
 *
 *  Fri Sep 21 16:08:23 2007
 *  Copyright  2007  chekanov
 *  chekanov@mail.desy.de
 ****************************************************************************/
using namespace std;
#include<iostream>
#include "TROOT.h"
#include "TFile.h"
#include "TH1D.h"
#include "TH2F.h"
#include "TH2D.h"
#include "TTree.h"
#include "TProfile.h"
#include <TProfile2D.h>

class Histo {

  public:


    //Large-R Kinematics
    TH1D* LR_jet_pt;
    TH1D* LR_jet_eta;
    TH1D* LR_jet_phi;
    TH1D* LR_jet_m;

    TH1D* LR_leading_jet_pt;
    TH1D* LR_leading_jet_eta;
    TH1D* LR_leading_jet_phi;
    TH1D* LR_leading_jet_m;
    
    //Tagger
    TH1D* xbb_higgs;
    TH1D* leading_jet_DL1r;

    //Jet Kinematic quantities
    TH1D* jet_pt;
    TH1D* jet_eta;
    TH1D* runnumber2017;
    TH1D* runnumber2016;
    TH1D* monrandom;

    TH1D* leadingjetpt;
    TH1D* leadingjetpt_el;
    TH1D* leadingjetpt_mu;

    TH1D* leadingjetm;
    TH1D* leadingjeteta;
    TH1D* leadingjetphi;
    TH2D* leadingjetetavsphi;
    TH1D* leadingjetmv2c20;
    TH1D* leadingjetm800;
    TH1D* leadingjetm800b2;
    TH1D* leadingjetm800b1;
    TH1D* leadingjetm800b12;
    TH1D* leadingjetm700;
    TH1D* leadingjetm700b2;
    TH1D* leadingjetm700b1;
    TH1D* leadingjetm700b12;
    TH1D* leadingjetm600;
    TH1D* leadingjetm600b2;
    TH1D* leadingjetm600b1;
    TH1D* leadingjetm600b12;

    TH1D* binsM;
    TH1D* binsM_tev; // same in TeV 
    TH1D *h_dimensions;

    TH1D* secondjetpt;
    TH1D* secondjetm;
    TH1D* secondjeteta;
    TH1D* secondjetphi;
    TH2D* secondjetetavsphi;
    TH1D* secondjetmv2c20;

    TH1D* jetjetmass; // jet-jet mass 
    TH1D* jetjetmass_tev; // jet-jet mass 
    TH1D* jetjetmassnb; // jet-jet mass 
    TH1D* jetjetmass_badgood; // bad-good

    TH1D* jetjetmass_noor; // jet-jet mass
    TH1D* jetjetmass_noor_tev; // jet-jet mass

    // run dependence
    // 297730 - 305777 
    TH1D* jetjetmass1; // jet-jet mass 
    TH1D* jetjetmass1_mu; // jet-jet mass 
    TH1D* jetjetmass1_el; // jet-jet mass 
    // 305777 - 311481 
    TH1D* jetjetmass2;
    TH1D* jetjetmass2_mu; // jet-jet mass 
    TH1D* jetjetmass2_el; // jet-jet mass 
    // 325713 - 336719
    TH1D* jetjetmass3; // jet-jet mass 
    TH1D* jetjetmass3_mu; // jet-jet mass 
    TH1D* jetjetmass3_el; // jet-jet mass 
    // 336719 - 340453 
    TH1D* jetjetmass4;
    TH1D* jetjetmass4_mu; // jet-jet mass 
    TH1D* jetjetmass4_el; // jet-jet mass 


    // e and mu in the same event
    TH1D* jetjetmass_muel; // jet-jet mass 
    TH1D* jetjetmass_muel_tev; // jet-jet mass 
     
    // with dynamic MET
    TH1D* jetjetmass_ptmiss; // with ptmiss 
    TH1D* jetjetmass_ptmiss_el; // with ptmiss+ele 
    TH1D* jetjetmass_ptmiss_mu; // with ptmiss+mu 

    TH1D* jetjetmass_ptmiss2; // with ptmiss 
    TH1D* jetjetmass_ptmiss2_el; // with ptmiss+ele 
    TH1D* jetjetmass_ptmiss2_mu; // with ptmiss+mu 


    // fixed MET
    TH1D* jetjetmass_ptmiss100; // with ptmiss 
    TH1D* jetjetmass_ptmiss100_el; // with ptmiss+ele 
    TH1D* jetjetmass_ptmiss100_mu; // with ptmiss+mu 

    TH1D* jetjetmass_ptmiss200; // with ptmiss 
    TH1D* jetjetmass_ptmiss200_el; // with ptmiss+ele 
    TH1D* jetjetmass_ptmiss200_mu; // with ptmiss+mu 

    TH1D* jetjetmass_ptmiss400;
    TH1D* jetjetmass_ptmiss600; 
    TH1D* jetjetmass_ptmiss800;
    TH1D* jetjetmass_ptmiss1000;

    // lepton cuts
    TH1D* jetjetmass_ptl100;
    TH1D* jetjetmass_ptl200;
    TH1D* jetjetmass_ptl400;
    TH1D* jetjetmass_ystar;
    // jet masses for Mjj>3000 GeV
    TH1D* leadingjetmjj3000;
    TH1D* subleadingjetmjj3000;
    TH1D* leadingjetmjj3000_inc;
    TH1D* subleadingjetmjj3000_inc;
    // after 1 TeV cut
    TH1D* leadingjetmjj3000pt1;
    TH1D* leadingjetmjj3000pt1_inc;

    // jet masses for Mjj>3000 GeV plus b-tag
    TH1D* leadingjetmjj3000_bb;
    TH1D* subleadingjetmjj3000_bb;
    TH1D* leadingjetmjj3000_inc_bb;
    TH1D* subleadingjetmjj3000_inc_bb;
    TH1D* leadingjetmjj3000_b;
    TH1D* subleadingjetmjj3000_b;
    TH1D* leadingjetmjj3000_inc_b;
    TH1D* subleadingjetmjj3000_inc_b;

    //Mjj vs jet mass 
    TH2D* mjj3000_leadmass;

    // inclusive dijets without lepton
    TH1D* jetjetmass_inc;
    TH1D* jetjetmass_inc_pt60;
    TH1D* jetjetmass_inc_ystar;
    TH1D* jetjetmass_inc_tev;


    // jet mass > 80 GeV
    TH1D* jetjetmass_jhigh80;
    TH1D* jetjetmass_jlow80;  
    TH1D* jetjetmass_jhigh80_mu; 
    TH1D* jetjetmass_jlow80_mu;
    TH1D* jetjetmass_jhigh80_el;
    TH1D* jetjetmass_jlow80_el;
    
    // either jet (not both)    
    TH1D* jetjetmass_jhigh80e;
    TH1D* jetjetmass_jhigh80e_mu;
    TH1D* jetjetmass_jhigh80e_el;

     // plus 1 btag
     TH1D* jetjetmass_jhigh80e_b;
     TH1D* jetjetmass_jhigh80e_b_inc;  


    // inclusive
    TH1D* jetjetmass_jhigh80_inc; 
    TH1D* jetjetmass_jlow80_inc;
    TH1D* jetjetmass_jhigh80e_inc;
    // with 60 GeV cut
    TH1D* jetjetmass_pt60_jhigh80_inc;
    TH1D* jetjetmass_pt60_jlow80_inc;
    TH1D* jetjetmass_pt60_jhigh80e_inc;

    // CPU cores
    TH1D* cpucores;

    TH2D* ptmiss_ptlepton;
    TH1D* ptmissDIVptlepton;
    TH1D* transverseMass;
    TH1D* transverseMassJJL;

    TH1D* jet_muon_angle; // jet-muon angle 
    TH1D* jet_elec_angle; // jet-elec angle 
    TH1D* jet_muon_dR; // jet-muon angle 
    TH1D* jet_elec_dR; // jet-elec angle 
    TH1D* jet_muon3000_dR; // jet-muon angle for Mjj>3000 
    TH1D* jet_elec3000_dR; // jet-elec angle 
     // after selection
    TH1D* jet_muon_dR_after; // jet-muon angle 
    TH1D* jet_elec_dR_after; // jet-elec angle 

    TH1D* ptmissTrk;
    TH1D* ptmissClus;
    TH1D* ptmissSum;
    TH1D* ptmissTrk_mu;
    TH1D* ptmissClus_mu;
    TH1D* ptmissSum_mu;
    TH1D* ptmissTrk_el;
    TH1D* ptmissClus_el;
    TH1D* ptmissSum_el;


    TH1D* ystar;
    TH1D* dphi;
    TH1D* njet;
    TH1D* et;
    TH1D* ht;
    TH1D* m_ele_pt;
    TH1D* m_mu_pt;
    TH1D* overlap_ele;
    TH1D* overlap_ntrk_ele;
    TH1D* overlap_mu;
    TH1D* overlap_ntrk_mu;
    TH1D* jet_elec_overlap_dR;

 
    TH2D* elec_eta_phi;
    TH2D* muon_eta_phi;

    // after b-tag
    TH1D* leadingjetpt_b;
    TH1D* leadingjetm_b;
    TH1D* leadingjeteta_b;
    TH1D* leadingjetphi_b;
    TH2D* leadingjetetavsphi_b;
    TH1D* leadingjetmv2c20_b;
    TH1D* secondjetpt_b;
    TH1D* secondjetm_b;
    TH1D* secondjeteta_b;
    TH1D* secondjetphi_b;
    TH2D* secondjetetavsphi_b;
    TH1D* secondjetmv2c20_b;
    TH1D* jetjetmass_b;
    TH1D* jetjetmass_b_exc;
    TH1D* jetjetmassnb_b;

    TH1D* ystar_b;
    TH1D* dphi_b;
    TH1D* njet_b;
    TH1D* et_b;
    TH1D* ht_b;
    // after double b-tag
    TH1D* leadingjetpt_bb;
    TH1D* leadingjetm_bb;
    TH1D* leadingjeteta_bb;
    TH1D* leadingjetphi_bb;
    TH2D* leadingjetetavsphi_bb;
    TH1D* leadingjetmv2c20_bb;
    TH1D* secondjetpt_bb;
    TH1D* secondjetm_bb;
    TH1D* secondjeteta_bb;
    TH1D* secondjetphi_bb;
    TH2D* secondjetetavsphi_bb;
    TH1D* secondjetmv2c20_bb;
    TH1D* jetjetmass_bb;
    TH1D* jetjetmassnb_bb;

    TH1D* ystar_bb;
    TH1D* dphi_bb;
    TH1D* njet_bb;
    TH1D* et_bb;
    TH1D* ht_bb;
    // after 70% 
    TH1D* leadingjetpt_b70;
    TH1D* leadingjetm_b70;
    TH1D* leadingjeteta_b70;
    TH1D* leadingjetphi_b70;
    TH2D* leadingjetetavsphi_b70;
    TH1D* leadingjetmv2c20_b70;
    TH1D* secondjetpt_b70;
    TH1D* secondjetm_b70;
    TH1D* secondjeteta_b70;
    TH1D* secondjetphi_b70;
    TH2D* secondjetetavsphi_b70;
    TH1D* secondjetmv2c20_b70;
    TH1D* jetjetmass_b70;
    TH1D* jetjetmassnb_b70;

    TH1D* ystar_b70;
    TH1D* dphi_b70;
    TH1D* njet_b70;
    TH1D* et_b70;
    TH1D* ht_b70;
    // after double b-tag
    TH1D* leadingjetpt_bb70;
    TH1D* leadingjetm_bb70;
    TH1D* leadingjeteta_bb70;
    TH1D* leadingjetphi_bb70;
    TH2D* leadingjetetavsphi_bb70;
    TH1D* leadingjetmv2c20_bb70;
    TH1D* secondjetpt_bb70;
    TH1D* secondjetm_bb70;
    TH1D* secondjeteta_bb70;
    TH1D* secondjetphi_bb70;
    TH2D* secondjetetavsphi_bb70;
    TH1D* secondjetmv2c20_bb70;
    TH1D* jetjetmass_bb70;
    TH1D* jetjetmassnb_bb70;

    TH1D* ystar_bb70;
    TH1D* dphi_bb70;
    TH1D* njet_bb70;
    TH1D* et_bb70;
    TH1D* ht_bb70;

    // after b-tag flat rej
    TH1D* leadingjetpt_flat_b;
    TH1D* leadingjetm_flat_b;
    TH1D* secondjetpt_flat_b;
    TH1D* secondjetm_flat_b;
    TH1D* jetjetmass_flat_b;
    TH1D* jetjetmassnb_flat_b;
    // after double b-tag flat rej
    TH1D* leadingjetpt_flat_bb;
    TH1D* leadingjetm_flat_bb;
    TH1D* secondjetpt_flat_bb;
    TH1D* secondjetm_flat_bb;
    TH1D* jetjetmass_flat_bb;
    TH1D* jetjetmassnb_flat_bb;

    // after 70%  flat rej
    TH1D* leadingjetpt_flat_b70;
    TH1D* leadingjetm_flat_b70;
    TH1D* secondjetpt_flat_b70;
    TH1D* secondjetm_flat_b70;
    TH1D* jetjetmass_flat_b70;
    TH1D* jetjetmassnb_flat_b70;
    // after 70% double b-tag flat rej
    TH1D* leadingjetpt_flat_bb70;
    TH1D* leadingjetm_flat_bb70;
    TH1D* secondjetpt_flat_bb70;
    TH1D* secondjetm_flat_bb70;
    TH1D* jetjetmass_flat_bb70;
    TH1D* jetjetmassnb_flat_bb70;

    TH2D* tilePhiEta;

    TH2D* jetwithwithout;

    // 3 TeV region
    TH1D* muon3000_pt;
    TH1D* muon3000_eta;
    TH1D* muon3000_phi;
    TH1D* elec3000_pt;
    TH1D* elec3000_eta;
    TH1D* elec3000_phi;

    // alternative 2 TeV region
    TH1D* muon2000_pt;
    TH1D* muon2000_eta;
    TH1D* muon2000_phi;
    TH1D* elec2000_pt;
    TH1D* elec2000_eta;
    TH1D* elec2000_phi;

 
    // always medium
    TH1D* jetjetmass_mu;
    TH1D* jetjetmass_mu_tev;

    TH1D* jetlepmass_el;
    TH1D* jetlepmass_mu;

    // cut to any jet
    TH1D* jetlepmass_any_el;
    TH1D* jetlepmass_any_mu;

    TH1D* jetlepmass_lead_el;
    TH1D* jetlepmass_lead_mu;

    TH1D* mass_ee;
    TH1D* mass_mumu;
    TH1D* mass_emu;


    // exclusive Mjj definitions
    TH1D* jetjetmass_el_exc;
    TH1D* jetjetmass_mu_exc;

 
    // M(jjlep) 
    TH1D* jjlmass;
    TH1D* jjlmass_el;
    TH1D* jjlmass_mu;
    TH1D* jjlmass_tev;
    TH1D* jjlmass_el_tev;
    TH1D* jjlmass_mu_tev;

    TH1D* jjlmass_pt;
    TH1D* jjllmass_pt;

    // M(jll) 
    TH1D* jllmass;
    TH1D* jllmass_el;
    TH1D* jllmass_mu;
    TH1D* jllmass_tev;
    TH1D* jllmass_el_tev;
    TH1D* jllmass_mu_tev;
    // e+mu
    TH1D* jllmass_muel;
    TH1D* jllmass_muel_tev;

    // M(jjjlep)     
    TH1D* jjjlmass;
    TH1D* jjjlmass_el;
    TH1D* jjjlmass_mu;
    TH1D* jjjlmass_tev;
    TH1D* jjjlmass_el_tev;
    TH1D* jjjlmass_mu_tev;

    // M(jjll)
    TH1D* jjllmass;
    TH1D* jjllmass_el;
    TH1D* jjllmass_mu;
    TH1D* jjllmass_tev;
    TH1D* jjllmass_el_tev;
    TH1D* jjllmass_mu_tev;


    // tight+medium
    TH1D* jetjetmass_el;
    TH1D* jetjetmass_el_tev;

    TH1D* jetjetmass_el_isolated_Gradient;
    TH1D* jetjetmass_el_nonIsolated_Gradient;

    TH1D* jetjetmass_el_tight;
    TH1D* jetjetmass_el_loose;
    TH1D* jetjetmass_el_medium;
    // after isolation
    TH1D* jetjetmass_el_medium_isolated_Gradient; TH1D* jetjetmass_el_medium_nonIsolated_Gradient;
    TH1D* jetjetmass_el_tight_isolated_Gradient; TH1D*  jetjetmass_el_tight_nonIsolated_Gradient;

    TH1D* jetjetmass_mu_b;
    TH1D* jetjetmass_mu_bb;
    TH1D* jetjetmass_el_b;
    TH1D* jetjetmass_el_bb;

    // 2 triggers
    TH1D* jetjetmass_lowtrigger; 
    TH1D* jetjetmass_hightrigger;
    TH1D* n_muon;
    TH1D* n_electron;
    TH1D* n_jet;
    TH1D* n_trigger;
    TH1D* n_trigger_after_cuts;
    TH2D* n_muon_electron;
    TH2D* n_ptmuon_ptelec;
    TH1D* n_muonCB;

    TH1D* pt_ele_tight;
    TH1D* pt_ele_medium;
    TH1D* pt_ele_loose;

    TH1D* pt_mu_tight;
    TH1D* pt_mu_medium;
    TH1D* pt_mu_loose;

    // pt of leptons in Mjj>3.5 TeV
    TH1D* muon_pt3500;
    TH1D* elec_pt3500;

    TH1D* jetjetmass_rest; 

    TH1D* cutflow;
    TH1D* cutflow_weighted;
    TH1D* cutflow_weighted_pu;
    TH1D* cross_section;

    TH1D* cutflow_electrons;
    TH1D* cutflow_muons;
    TH1D* cutflow_jets;
    TH1D* cutflow_photons;


    // ANN studies
    TProfile2D* profrmm;
    TH2D * projrmm;
    TH1D * NN0;
    TH1D * NN1;
    TH1D * NN2;
    TH1D * NN3;
    TH1D * NN4;
    TH1D * NN5;
    TH1D * NN6;
    TH1D * NN7;

    // data replace SM MC
    TH1D * NND0;
    TH1D * NND1;
    TH1D * NND2;
    TH1D * NND3;
    TH1D * NND4;
    TH1D * NND5;
    TH1D * NND6;
    TH1D * NND7;

    // alternative data
    TH1D * NNDD0;
    TH1D * NNDD1;
    TH1D * NNDD2;
    TH1D * NNDD3;
    TH1D * NNDD4;
    TH1D * NNDD5;
    TH1D * NNDD6;
    TH1D * NNDD7;

    // to fill RMM
    TH1D * projRMM;
    TH1D * projRMMfilled;
    std::vector<Double32_t> m_proj;
    std::vector<UInt_t> m_proj_index1;
    std::vector<UInt_t> m_proj_index2;
    UInt_t m_run;
    Long64_t m_event;
    Double32_t m_weight;

    TTree*  m_tree;

    // 100% MC based
    TH1D * jetjetmassNN;
    TH1D * jetjetmassNN_tev;
    TH1D * jetjetmassNN0;
    TH1D * jetjetmassNN0_tev;
    TH1D * jetjetmassNN1;
    TH1D * jetjetmassNN1_tev;
    TH1D * jetjetmassNN2;
    TH1D * jetjetmassNN2_tev;
    TH1D * jetjetmassNN3;
    TH1D * jetjetmassNN3_tev;
    TH1D * jetjetmassNN4;
    TH1D * jetjetmassNN4_tev;
    TH1D * jetjetmassNN5;
    TH1D * jetjetmassNN5_tev;
    TH1D * jetjetmassNN6;
    TH1D * jetjetmassNN6_tev;

    // NN based on 2015 data for dijets
    TH1D * jetjetmassNND;
    TH1D * jetjetmassNND_tev;
    TH1D * jetjetmassNND0;
    TH1D * jetjetmassNND0_tev;
    TH1D * jetjetmassNND1;
    TH1D * jetjetmassNND1_tev;
    TH1D * jetjetmassNND2;
    TH1D * jetjetmassNND2_tev;
    TH1D * jetjetmassNND3;
    TH1D * jetjetmassNND3_tev;
    TH1D * jetjetmassNND4;
    TH1D * jetjetmassNND4_tev;
    TH1D * jetjetmassNND5;
    TH1D * jetjetmassNND5_tev;
    TH1D * jetjetmassNND6;
    TH1D * jetjetmassNND6_tev;

    // alternative
    TH1D * jetjetmassNNDD;
    TH1D * jetjetmassNNDD_tev;
    TH1D * jetjetmassNNDD0;
    TH1D * jetjetmassNNDD0_tev;
    TH1D * jetjetmassNNDD1;
    TH1D * jetjetmassNNDD1_tev;
    TH1D * jetjetmassNNDD2;
    TH1D * jetjetmassNNDD2_tev;
    TH1D * jetjetmassNNDD3;
    TH1D * jetjetmassNNDD3_tev;
    TH1D * jetjetmassNNDD4;
    TH1D * jetjetmassNNDD4_tev;
    TH1D * jetjetmassNNDD5;
    TH1D * jetjetmassNNDD5_tev;
    TH1D * jetjetmassNNDD6;
    TH1D * jetjetmassNNDD6_tev;


// 9 invariant masses
    TH1D* Mjj;
    TH1D* Mjb;
    TH1D* Mbb;
    TH1D* Mje;
    TH1D* Mjm;
    TH1D* Mjg;
    TH1D* Mbe;
    TH1D* Mbm;
    TH1D* Mbg;

 
// ---- do not modify below -----
    Histo();
    virtual ~Histo();
    void setHistograms();
    void setOutput(string output);
    void finalize();
    char *ffile;
    TH1D *debug;
    TH1D *triggers;
    TH1D *triggers_ex;
    TH1D *triggers_selected;
    TH1D *hweights;
    TH1D *hweights_nominal;
    TFile *RootFile;

};


#!/bin/bash
# S.Chekanov (ANL)

echo "Set ROOT enviroment for Dijet+Lepton program"

# unlimit number of openned files
ulimit -n 2048


HH=`hostname -A` 

echo "HOST=$HH"

export STORAGE="/atlasfs/atlas/local/chekanov/Dijet2017lepton/data_Aug_2017/"
if [[ $HH =~ .*atlas25.* ]]; then
  echo "HEP ANL ROOT setup"
  source /share/sl6/set_asc.sh
  #export STORAGE="/atlasfs/atlas/local/chekanov/Dijet2017lepton/data_Aug_2017/"
  export STORAGE="/atlasfs/atlas/local/chekanov/Dijet2017lepton/data_March_2018/"
fi

if [[ $HH =~ .*atlaslogin.* ]]; then
  echo "HEP ANL ROOT setup"
  source /users/admin/share/sl7/setup.sh 
  export STORAGE="/users/chekanov/data/Dijet2018lepton/data_June_2018/"
fi


if [[ $HH =~ .*lcrc.anl.* ]]; then
  echo "LCRC ANL ROOT setup"
  #source /soft/hep/hep_setup.sh
  #with numpy
  #module load python py-numpy
  source /soft/hep/release21.2.51_setup.sh

  # python -m pip install --user scipy 
  # module load python py-numpy
  #echo "setup py-numpy" 
  # export STORAGE="/lcrc/group/ATLAS/atlasfs/local/chekanov/Dijet2018lepton/data_June_2018/"
  export STORAGE="/lcrc/group/ATLAS/atlasfs/local/chekanov/Dijet2018lepton/data_September_2018/"
fi


if [[ $HH =~ .*lxplus.* ]]; then
  echo "LXPLUS ROOT setup"
  setupATLAS 
  localSetupROOT 6.08.02-x86_64-slc6-gcc49-opt
  export STORAGE="/eos/atlas/atlascerngroupdisk/phys-exotics/jdm/lepdijet/Dijet2017lepton/data_Aug_2017/" 
fi

echo "STORAGE=$STORAGE"


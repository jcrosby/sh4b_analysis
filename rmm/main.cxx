/***************************************************************************
 *            main.cxx
 *
 *  Fri Sep 21 15:05:23 2007
 *  Copyright  2007  chekanov
 *  chekanov@mail.desy.de
 ****************************************************************************/



#include<iostream>
#include<fstream>
#include<stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <TROOT.h>
#include <TTree.h>
#include <TChain.h>
#include <TFile.h>
#include <TBrowser.h>
#include <TStyle.h>
#include "TApplication.h"
#include "TObject.h"
#include "TH1.h"
#include "TMath.h"
#include "TCanvas.h"
#include "TRandom.h"
#include "TThread.h"
#include <TTreePerfStats.h>

// analysis
#include "Ana.h"
#include "Global.h"
#include "Histo.h"


using namespace std;

Global glob;
Histo  h;


int main(int argc, char **argv)
{


      int i=1;
      if(argc < 2) {
	        printf("You must provide at least one argument that defines input list\n");
	        exit(0);
	    }

 // report settings
    for (;i<argc;i++) printf("Argument %d:%s\n",i,argv[i]);
    char * inp = argv[1];
    string name=std::string(inp);
    cout << "Input file=" << name << endl; 

    // get dictinary for vector
    gROOT->ProcessLine("#include <vector>");
    //string io_name=name+std::string("_ioperf.root");
    // initialize
    glob.getIni();
    // read list of ntuples
    glob.getNtuples(name);
    // set output and histograms
    h.setOutput(name);
    h.setHistograms();


    const int Nfiles = glob.ntup.size();
    cout << " -> Nr of files to read:" << Nfiles << endl;
    cout << " -> Nr of events to process:" <<  glob.MaxEvents << endl;


    // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/JetUncertaintiesRel21Summer2018SmallR#Jet_energy_resolution_JER_uncert
    // R4_SR_Scenario*_SimpleJER.config
    // This is recommended for analyses which are sensitive to neither the JES nor the JER, and which do not intend to perform a combination. 
    // default tree (sys=0)
    //string maintree="Nominal"; 

    string stree="AnalysisMiniTree";

// only works for MC, not for data
#if montecarlo
    if (glob.systematics == 1) stree="JET_EffectiveNP_Detector1__1up";
    if (glob.systematics == 2) stree="JET_EffectiveNP_Detector1__1down";
    if (glob.systematics == 3) stree="JET_EffectiveNP_Detector2__1up";
    if (glob.systematics == 4) stree="JET_EffectiveNP_Detector2__1down";
    if (glob.systematics == 5) stree="JET_EffectiveNP_Mixed1__1up";
    if (glob.systematics == 6) stree="JET_EffectiveNP_Mixed1__1down";
    if (glob.systematics == 7) stree="JET_EtaIntercalibration_TotalStat__1up";
    if (glob.systematics == 8) stree="JET_EtaIntercalibration_TotalStat__1down";
    if (glob.systematics == 9) stree="JET_EtaIntercalibration_NonClosure_highE__1up";
    if (glob.systematics == 10) stree="JET_EtaIntercalibration_NonClosure_highE__1down"; 
    if (glob.systematics == 11) stree="JET_EtaIntercalibration_NonClosure_negEta__1up";
    if (glob.systematics == 12) stree="JET_EtaIntercalibration_NonClosure_negEta__1down";
    if (glob.systematics == 13) stree="JET_EtaIntercalibration_NonClosure_posEta__1up";
    if (glob.systematics == 14) stree="JET_EtaIntercalibration_NonClosure_posEta__1down";
    if (glob.systematics == 15) stree="JET_JER_DataVsMC_AFII__1up";
    if (glob.systematics == 16) stree="JET_JER_DataVsMC_AFII__1down";
    if (glob.systematics == 17) stree="JET_RelativeNonClosure_AFII__1up";
    if (glob.systematics == 18) stree="JET_RelativeNonClosure_AFII__1down";
    if (glob.systematics == 19) stree="JET_PunchThrough_AFII__1up";
    if (glob.systematics == 20) stree="JET_PunchThrough_AFII__1down";
    if (glob.systematics == 21) stree="JET_JER_EffectiveNP_1__1up";
    if (glob.systematics == 22) stree="JET_JER_EffectiveNP_1__1down";
    if (glob.systematics == 23) stree="JET_JER_EffectiveNP_2__1up";
    if (glob.systematics == 24) stree="JET_JER_EffectiveNP_2__1down";
    if (glob.systematics == 25) stree="JET_JER_EffectiveNP_3__1up";
    if (glob.systematics == 26) stree="JET_JER_EffectiveNP_3__1down";
    if (glob.systematics == 27) stree="JET_JER_EffectiveNP_4__1up";
    if (glob.systematics == 28) stree="JET_JER_EffectiveNP_4__1down";
    if (glob.systematics == 29) stree="JET_JER_EffectiveNP_5__1up";
    if (glob.systematics == 30) stree="JET_JER_EffectiveNP_5__1down";
    if (glob.systematics == 31) stree="JET_JER_EffectiveNP_6__1up";
    if (glob.systematics == 32) stree="JET_JER_EffectiveNP_6__1down";
    if (glob.systematics == 33) stree="JET_JER_EffectiveNP_7__1up";
    if (glob.systematics == 34) stree="JET_JER_EffectiveNP_7__1down";
    if (glob.systematics == 35) stree="JET_JER_EffectiveNP_8__1up";
    if (glob.systematics == 36) stree="JET_JER_EffectiveNP_8__1down";
    if (glob.systematics == 37) stree="JET_JER_EffectiveNP_9__1up";
    if (glob.systematics == 38) stree="JET_JER_EffectiveNP_9__1down";
    if (glob.systematics == 39) stree="JET_JER_EffectiveNP_10__1up";
    if (glob.systematics == 40) stree="JET_JER_EffectiveNP_10__1down";
    if (glob.systematics == 41) stree="JET_JER_EffectiveNP_11__1up";
    if (glob.systematics == 42) stree="JET_JER_EffectiveNP_11__1down";
    if (glob.systematics == 43) stree="JET_BJES_Response__1up";
    if (glob.systematics == 44) stree="JET_BJES_Response__1down";
    if (glob.systematics == 45) stree="JET_Flavor_Composition__1up";
    if (glob.systematics == 46) stree="JET_Flavor_Composition__1down";
    if (glob.systematics == 47) stree="JET_Pileup_OffsetNPV__1up";
    if (glob.systematics == 48) stree="JET_Pileup_OffsetNPV__1down";
    if (glob.systematics == 49) stree="JET_Pileup_OffsetMu__1up";
    if (glob.systematics == 50) stree="JET_Pileup_OffsetMu__1down";
    if (glob.systematics == 51) stree="JET_SingleParticle_HighPt__1up";
    if (glob.systematics == 52) stree="JET_SingleParticle_HighPt__1down";
    if (glob.systematics == 53) stree="MUON_SAGITTA_RESBIAS__1up";
    if (glob.systematics == 54) stree="MUON_SAGITTA_RESBIAS__1down";
    if (glob.systematics == 55) stree="MUON_SCALE__1up";
    if (glob.systematics == 56) stree="MUON_SCALE__1down";
    if (glob.systematics == 57) stree="MUON_ID__1up";
    if (glob.systematics == 58) stree="MUON_ID__1down";
    if (glob.systematics == 59) stree="EG_SCALE_ALL__1up";
    if (glob.systematics == 60) stree="EG_SCALE_ALL__1down";
    if (glob.systematics == 61) stree="EG_RESOLUTION_ALL__1up";
    if (glob.systematics == 62) stree="EG_RESOLUTION_ALL__1down";

    cout << " -> systematics:" <<  glob.systematics << "  tree:" <<  stree << endl;

#endif

    if (glob.systematics >90) stree="AnalysisMiniTree"; // instead of "outTreeJET_JER_SINGLE_NP__1down";



    //stree=maintree+"/"+stree; 
    cout << "Analysis of tree " << stree << endl;
    for (int i = 0; i < Nfiles; i++) {

       float donefiles= (float)i / (float)Nfiles;
       cout << "\nPercentage done = " << (int)(donefiles*100) <<  " %" << endl;
       cout << "\nfile to read:" << glob.ntup[i] << endl;

       TFile f(glob.ntup[i].c_str());
       if (f.IsZombie()) continue; 
       //if (f.TestBit(TFile::kRecovered)==false) continue;


       TTree *tree = (TTree *)f.Get(stree.c_str());
       //* Extract cutflow histograms **/
       //string key="HNominal";
       //cout << "Extract=" << key << endl;

        glob.SumOfWeights=1.0;
        glob.m_run=i;

       //weighted cutflow 
       //key="HNominal_PUWgt";
       //cout << "Extract=" << key << endl;
       TH1D* hh2  = (TH1D*)f.Get( ("H" + stree + "_PUWgt").c_str() );
       if (hh2 != 0) {
       for (int j=0; j<(hh2->GetXaxis())->GetNbins(); j++) {
                     double y1=hh2->GetBinContent(j);
                     cout << " cut=" << j << " Events=" << y1 << endl;
                     h.cutflow_weighted_pu->SetBinContent(j,y1);
          }
       };

       //sum of weights 
       //weighted cutflow 
       //key="HNominal_Wgt";
       //cout << "Extract=" << key << endl;
       TH1D* hh3  = (TH1D*)f.Get( ("H" + stree + "_Wgt").c_str() );
       if (hh3 != 0) {
       for (int j=0; j<(hh3->GetXaxis())->GetNbins(); j++) {
                     double y1=hh3->GetBinContent(j);
                     cout << " cut=" << j << " Events=" << y1 << endl;
                     if (j==1) glob.SumOfWeights= y1;
                     h.cutflow_weighted->SetBinContent(j,y1);
                     h.cutflow->SetBinContent(j,y1);
          }
       };

        cout << "SumOfWeights = " << glob.SumOfWeights << endl;
        // protect
        if (glob.SumOfWeights==0) {
                 glob.SumOfWeights=1;
        }; 


// run over all event is negative
        if (glob.MaxEvents>0)
            if (glob.nev >= glob.MaxEvents) break; 
// perf
        //TTreePerfStats *ps= new TTreePerfStats("ioperf",tree); 

        Ana ana(tree);

        ana.Notify();

//    ana.GetEntry(12); // Fill t data members with entry number 12
//    ana.Show();       // Show values of entry 12
//    ana.Show(16);     // Read and show values of entry 16


       // fast processing
       if (glob.type != -999) ana.Loop();      // Loop on all entries
     
       // benchmark IO   
       //ps->SaveAs(io_name.c_str());

        delete tree;
        f.Close();


    }


    // write histograms
    h.finalize();

    cout << "Final processing statistics.." << endl;
    cout << "--> Number of events requested:" <<  glob.MaxEvents  << endl;
    cout << "--> Number of events processed:" <<  glob.nev  << endl;
    cout << "--> Number of events  selected:" <<  glob.TotalEvents << endl;
    cout << "Nr muons="<< glob.Nmuon << " Nr electrons=" << glob.Nelec << endl;

    return 0;
}




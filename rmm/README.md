This analysis framework is used to for anomaly detection analysis using the MasterShef ntuples (https://gitlab.cern.ch/dijetpluslepton/anomalydetection/MasterShef). 
The ntuples can be  downloaded as:

Run2+Run3 data:
```
user.chekanov.24.2.9-local.06-09-2023.data20%.period%.DAOD_PHYS.grp%_v01_p5631_commonANL_default.root/
```

MC20 simulations:
```
user.chekanov.24.2.9-local.06-09-2023.mc20e.%.DAOD_PHYS.e7081_s3681_r13145_p5631_commonANL_default.root/
```
Note that MC file names has only run numbers. You need to match these run numbers with the MC list:
https://gitlab.cern.ch/dijetpluslepton/anomalydetection/MasterShef/-/tree/master/source/MasterShef/share/sample_lists/Common/PHYS
 

To compile for data, type "make". To compile for MC, type "make mc".
The output goes to the directory "out" that needs to be made.

To process Run2 data, run "A_RUN_DATA_RUN2_ALL" to process Run3 data, run "A_RUN_DATA_RUN3_ALL". It is assumed that the data directory are given in setup.sh using the variable "STORAGE". It should point to the directories from MasterShef:

```
data2015/
data2016/
data2017/
data2018/
data2022/
```

While running, the temporary output will be in "output". After each year is done, the final output will be in "out/t[event type]/sys0", where [event type]  is "0 - 6" (int). During data processing, the program reads "main.ini" which defines which type of data stream is used.
The output ROOT files have test histograms and RMM as ntuple with non-zero values. 
The program runs in 24 threads and then combines the outputs.

The main analysis code is in src/Loop.cxx". To add a histogram, define it in inc/Histo.ini and src/Histo.cxx. The global variables are define in "inc/Global.inc".


S.Chekanov (ANL).
 

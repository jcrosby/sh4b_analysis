
date=`date +%Y%m%d`
tag='SH4b'
SCRIPT=mc_nosysts_mc20e
GRID_NAME=${RUCIO_ACCOUNT-${USER}}
OUT_DS=user.${GRID_NAME}.${date}_SH4b_${SCRIPTn}_${tag}
INPUT_DATASETS=grid_inputs.txt
ZIP=job.tgz


#SCRIPT used
echo 'script: '${SCRIPT}

function submit-job(){
    
    DS=$1
    
    USER_DSID=$(sed -r 's/[^\.]*\.([0-9]{6,8})\..*/\1/' <<< ${DS})
    #DSID=$(sed -r 's/.*[.]//' <<< ${USER_DSID})
    DSID="0" # Use when running on data
    MASS=$(sed -e 's/^.*.Py8EG*.//' <<< ${DS})
    DATA_RUN=$(sed -e 's/^.*.24022023*.//' <<< ${DS})
    #echo "Mass: ${MASS}"
    echo "Data run: ${DATA_RUN}"
    OUT_DS=user.${GRID_NAME}.${date}_${SCRIPT}.${DATA_RUN}.${DSID}.${tag}
    echo "input file: ${DS}.txt" 
    echo "OUT_DS: ${OUT_DS}"

    if [[ $(wc -c <<< ${OUT_DS}) -ge 120 ]] ; then
        echo "ERROR: dataset name ${OUT_DS} is too long, can't submit!" 1>&2
        return 1
    fi

    echo "DSID: " ${DSID}
  
    prun --exec="${SCRIPT} ${DS}.txt ${DSID}" \
             --useAthenaPackages --cmtConfig=x86_64-centos7-gcc11-opt \
             --outDS ${OUT_DS} --inDS ${DS} \
             --outputs=output_root:output.root \
             --writeInputToTxt=IN:${DS}.txt \
             --noEmail \
               > ${OUT_DS}.log  2>&1

}

export -f submit-job
export GRID_NAME ZIP SCRIPT DRYRUN INPUT_DATASETS date tag DSID

while read line; do
printf ${line} | xargs -P 10 -I {} bash -c "submit-job {}"
done < ${INPUT_DATASETS}



#Move files to submit directory
SUBMIT_DIR=submit

echo "Preparing submit directory"
if [[ -d ${SUBMIT_DIR} ]]; then
    echo "Removing old submit directory"
    rm -rf ${SUBMIT_DIR}
fi

mkdir ${SUBMIT_DIR}
#copy  files
mv user.* submit
mv job.tgz submit

echo $BREAK
echo "Submission successful"

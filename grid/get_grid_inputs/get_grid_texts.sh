#generate certain r-tag grid input


RECON='data18'

#rm grid_inputs.txt

echo "Retrieving Inputs"

cat dids_output.txt | while read line || [[ -n $line ]];
do
  if grep "$RECON" <<< $line; then
     echo $line >> grid_inputs.txt
  fi
done


#generate all grid inputs... used on data
#input_file=dids_output.txt

#generate specific r-tag grid inputs
input_file=grid_inputs.txt


while read p; do
  rucio list-files "$p" >> "$p".txt
done < $input_file


echo "Cleaning datasets"

while read g; do
  prefix="$g""/"
  while read p; do
    sed -i -e 's/\(|\|+\|-\|[\|]\|SCOPE\|:\|NAME\|DID\|TYPE\|DIDType.CONTAINER\| \)//g' "$g".txt
    sed -i '/^[^u]/d' "$g".txt
    sed -i -r 's/(.{59}).*/\1/' "$g".txt
    sed -i '/^$/d' "$g".txt
  done < "$g".txt
  sed -i '$!s/$/,/' "$g".txt
  awk -v prefix="$prefix" '{print prefix $0}' "$g".txt > "temp_$g".txt
  rm "$g".txt
  mv "temp_$g".txt "$g".txt
done < $input_file

rm [].txt

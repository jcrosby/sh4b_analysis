rm dids_output.txt
rm dataset_outputs.txt

source list-dids_rucio.sh > dids_output.txt

sed -i -e 's/\(|\|+\|-\|[\|]\|SCOPE\|:\|NAME\|DID\|TYPE\|DIDType.CONTAINER\| \)//g' dids_output.txt
sed -i -e 's/\(\[\|\]\)//g' dids_output.txt
sed -i -e '/^$/d' dids_output.txt
sed -i -e 's/\(.\{13\}\)//' dids_output.txt

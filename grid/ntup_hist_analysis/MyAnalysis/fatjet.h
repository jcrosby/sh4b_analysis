//////////////////////////////////////////////////////////
// This class has been written by Bill Murray    5th April 2018
// Adapted for ZGamma Calibration October 2018
//////////////////////////////////////////////////////////

#ifndef fatjet_h
#define fatjet_h
#include <TLorentzVector.h>

class fatjet : public TLorentzVector {
 public :
  std::vector<float> vrPt;
  std::vector<float> vrEta;
  std::vector<float> vrPhi;
  std::vector<float> vrMass;
  std::vector<float> vrPt_ufo;
  std::vector<float> vrEta_ufo;
  std::vector<float> vrPhi_ufo;
  std::vector<float> vrMass_ufo;
  //int TrackJetTruthLabel;
  int truthlabel;
  int truthlabel_ufo;
  float truthmass_S;
  float truthpt_S;
  float trutheta_S;
  float truthphi_S;
  float truthmass_H;
  float truthpt_H;
  float trutheta_H;
  float truthphi_H;
  int numtrackjets;
  int n_b_DL1r77;
  int n_b_DL1r85;
  int n_b_DL1r85_77; // A.X.
  std::vector<int> DL1r_77;
  std::vector<int> DL1r_85;
  int numtrackjets_ufo;
  int n_b_DL1r77_ufo;
  int n_b_DL1r85_ufo;
  std::vector<int> DL1r_77_ufo;
  std::vector<int> DL1r_85_ufo;
  std::vector<int> UFOTrackJetTruthLabel;
  std::vector<int> TrackJetTruthLabel;
 
  fatjet();
  ~fatjet();
  bool isSelected();
  bool inHwindow();
  bool inLSB();//Low sideband of higss mass
  bool inHSB(); //High sideband of higgs mass
  bool LSB;
  bool Hwind;
  bool HSB;
  bool isRatio();
  bool isHcand();
  bool isScand();




  ClassDef(fatjet,0);

};

#endif

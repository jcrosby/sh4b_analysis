#ifndef Nominal_h
#define Nominal_h

#include "processtype.h"

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <TF1.h>
#include <TKey.h>
#include <TProfile.h>
#include <TEfficiency.h>
//#include <TEfficiencyHelper.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TSelector.h>
#include <TTreeReader.h>
#include <TTreeReaderValue.h>
#include <TTreeReaderArray.h>
#include <TTreeFormula.h>
#include "fatjet.h"
#include "smallrjet.h"
#ifndef processdata
#include "truthfatjet.h"
#include "truth_b.h"
#endif

#include <vector>
#include <string>
#include <map>



class Nominal : public TSelector {
public :
bool CheckValue(ROOT::Internal::TTreeReaderValueBase& value) {
  if (value.GetSetupStatus() < 0) {
    std::cerr << "Error " << value.GetSetupStatus() << "setting up reader for " << value.GetBranchName() << '\n';
    return false;
  }
  return true;
}


   TTreeReader    fReader;
   TTree          *fChain = 0;

   TTreeReaderValue<ULong64_t> EventNumber = {fReader, "eventNumber"};
   TTreeReaderValue<uint32_t> RunNumber = {fReader, "runNumber"};
   TTreeReaderValue<float> passDRcut = {fReader, "passRelativeDeltaRToVRJetCut"};
   // Only define these if looking at MC
#ifndef processdata 
   TTreeReaderValue<uint32_t> DSID = {fReader, "mcChannelNumber"};
   TTreeReaderArray<float> EventWeight = {fReader, "mcEventWeights"};
   TTreeReaderValue<Float_t> PRWEventWeight = {fReader, "pileupWeight_NOSYS"};
   TTreeReaderValue<Float_t> GeneratorWeight = {fReader, "generatorWeight_NOSYS"};
   TTreeReaderArray<std::vector<int>> TrackJetHadronConeExclTruthLabelID = {fReader, "HadronConeExclTruthLabelID_NOSYS"};
   TTreeReaderArray<int> LargeRJetTruthLabelID = {fReader, "R10TruthLabel_R21Consolidated_NOSYS"};
   TTreeReaderArray<int> SmallRJetTruthLabelID = {fReader, "recojet_antikt4_NOSYS_HadronConeExclTruthLabelID"};
   TTreeReaderArray<float> TruthLargeRJetPt = {fReader, "truthjet_antikt10_pt"};
   TTreeReaderArray<float> TruthLargeRJetEta = {fReader, "truthjet_antikt10_eta"};
   TTreeReaderArray<float> TruthLargeRJetPhi = {fReader, "truthjet_antikt10_phi"};
   TTreeReaderArray<float> TruthLargeRJetMass = {fReader, "truthjet_antikt10_m"};
   TTreeReaderArray<int> LargeRJetGhostBHadronsCount = {fReader, "recojet_antikt10_NOSYS_GhostBHadronsFinalCount"};
   //UFO
   TTreeReaderArray<float> TruthLargeRUFOJetPt = {fReader, "truthUFOjet_antikt10_pt"};
   TTreeReaderArray<float> TruthLargeRUFOJetEta = {fReader, "truthUFOjet_antikt10_eta"};
   TTreeReaderArray<float> TruthLargeRUFOJetPhi = {fReader, "truthUFOjet_antikt10_phi"};
   TTreeReaderArray<float> TruthLargeRUFOJetMass = {fReader, "truthUFOjet_antikt10_m"};
   TTreeReaderArray<int> LargeRUFOJetGhostBHadronsCount = {fReader, "recoUFOjet_antikt10_NOSYS_GhostBHadronsFinalCount"};
   TTreeReaderArray<int> LargeRUFOJetTruthLabelID = {fReader, "UFO_R10TruthLabel_R22v1_NOSYS"};
   TTreeReaderArray<std::vector<int>> TrackJetHadronConeExclTruthLabelID_ufo = {fReader, "UFO_R10_HadronConeExclTruthLabelID_NOSYS"};
   
   TTreeReaderValue<Float_t> TruthPt_H = {fReader, "truth_H1_pt"};
   TTreeReaderValue<Float_t> TruthEta_H = {fReader, "truth_H1_eta"};
   TTreeReaderValue<Float_t> TruthPhi_H = {fReader, "truth_H1_phi"};
   TTreeReaderValue<Float_t> TruthMass_H = {fReader, "truth_H1_m"};
   TTreeReaderValue<Float_t> TruthPt_S = {fReader, "truth_H2_pt"};
   TTreeReaderValue<Float_t> TruthEta_S = {fReader, "truth_H2_eta"};
   TTreeReaderValue<Float_t> TruthPhi_S = {fReader, "truth_H2_phi"};
   TTreeReaderValue<Float_t> TruthMass_S = {fReader, "truth_H2_m"};
   
   TTreeReaderArray<float> TruthBJetPt_H = {fReader, "truth_bb_fromH1_pt"};
   TTreeReaderArray<float> TruthBJetEta_H = {fReader, "truth_bb_fromH1_eta"};
   TTreeReaderArray<float> TruthBJetPhi_H = {fReader, "truth_bb_fromH1_phi"};
   TTreeReaderArray<float> TruthBJetMass_H = {fReader, "truth_bb_fromH1_m"};
   TTreeReaderArray<float> TruthBJetPt_S = {fReader, "truth_bb_fromH2_pt"};
   TTreeReaderArray<float> TruthBJetEta_S = {fReader, "truth_bb_fromH2_eta"};
   TTreeReaderArray<float> TruthBJetPhi_S = {fReader, "truth_bb_fromH2_phi"};
   TTreeReaderArray<float> TruthBJetMass_S = {fReader, "truth_bb_fromH2_m"};
#endif

   // Define these for both MC and data
   TTreeReaderArray<float> JetPt = {fReader, "recojet_antikt4_NOSYS_pt"};
   TTreeReaderArray<float> JetEta = {fReader, "recojet_antikt4_NOSYS_eta"};
   TTreeReaderArray<float> JetPhi = {fReader, "recojet_antikt4_NOSYS_phi"};
   TTreeReaderArray<float> JetMass = {fReader, "recojet_antikt4_NOSYS_m"};
   
   TTreeReaderArray<float> LargeRJetPt = {fReader, "recojet_antikt10_NOSYS_pt"};
   TTreeReaderArray<float> LargeRJetEta = {fReader, "recojet_antikt10_NOSYS_eta"};
   TTreeReaderArray<float> LargeRJetPhi = {fReader, "recojet_antikt10_NOSYS_phi"};
   TTreeReaderArray<float> LargeRJetMass = {fReader, "recojet_antikt10_NOSYS_m"};
   TTreeReaderArray<std::vector<float>> TrackJetPt = {fReader, "recojet_antikt10_NOSYS_leadingVRTrackJetsPt"};
   TTreeReaderArray<std::vector<float>> TrackJetEta = {fReader, "recojet_antikt10_NOSYS_leadingVRTrackJetsEta"};
   TTreeReaderArray<std::vector<float>> TrackJetPhi = {fReader, "recojet_antikt10_NOSYS_leadingVRTrackJetsPhi"};
   TTreeReaderArray<std::vector<float>> TrackJetMass = {fReader, "recojet_antikt10_NOSYS_leadingVRTrackJetsM"};
   TTreeReaderArray<float> TrackJetDR12 = {fReader, "recojet_antikt10_NOSYS_leadingVRTrackJetsDeltaR12"};
   TTreeReaderArray<float> TrackJetDR13 = {fReader, "recojet_antikt10_NOSYS_leadingVRTrackJetsDeltaR13"};
   TTreeReaderArray<float> TrackJetDR32 = {fReader, "recojet_antikt10_NOSYS_leadingVRTrackJetsDeltaR32"};
   TTreeReaderArray<std::vector<char>> TrackJetIsBTagged_DL1r_FixedCutBEff_77 = {fReader, "recojet_antikt10_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_77"}; 
   TTreeReaderArray<std::vector<char>> TrackJetIsBTagged_DL1r_FixedCutBEff_85 = {fReader, "recojet_antikt10_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_85"};
   //UFO
   TTreeReaderArray<float> LargeRUFOJetPt = {fReader, "recoUFOjet_antikt10_NOSYS_pt"};
   TTreeReaderArray<float> LargeRUFOJetEta = {fReader, "recoUFOjet_antikt10_NOSYS_eta"};
   TTreeReaderArray<float> LargeRUFOJetPhi = {fReader, "recoUFOjet_antikt10_NOSYS_phi"};
   TTreeReaderArray<float> LargeRUFOJetMass = {fReader, "recoUFOjet_antikt10_NOSYS_m"};
   TTreeReaderArray<std::vector<float>> UFOTrackJetPt = {fReader, "recoUFOjet_antikt10_NOSYS_leadingVRTrackJetsPt"};
   TTreeReaderArray<std::vector<float>> UFOTrackJetEta = {fReader, "recoUFOjet_antikt10_NOSYS_leadingVRTrackJetsEta"};
   TTreeReaderArray<std::vector<float>> UFOTrackJetPhi = {fReader, "recoUFOjet_antikt10_NOSYS_leadingVRTrackJetsPhi"};
   TTreeReaderArray<std::vector<float>> UFOTrackJetMass = {fReader, "recoUFOjet_antikt10_NOSYS_leadingVRTrackJetsM"};
   
   TTreeReaderArray<float> UFOTrackJetDR12 = {fReader, "recoUFOjet_antikt10_NOSYS_leadingVRTrackJetsDeltaR12"};
   TTreeReaderArray<float> UFOTrackJetDR13 = {fReader, "recoUFOjet_antikt10_NOSYS_leadingVRTrackJetsDeltaR13"};
   TTreeReaderArray<float> UFOTrackJetDR32 = {fReader, "recoUFOjet_antikt10_NOSYS_leadingVRTrackJetsDeltaR32"};
   TTreeReaderArray<std::vector<char>> UFOTrackJetIsBTagged_DL1r_FixedCutBEff_77 = {fReader, "recoUFOjet_antikt10_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_77"}; 
   TTreeReaderArray<std::vector<char>> UFOTrackJetIsBTagged_DL1r_FixedCutBEff_85 = {fReader, "recoUFOjet_antikt10_NOSYS_leadingVRTrackJetsBtag_DL1r_FixedCutBEff_85"};
  
   TTreeReaderArray<char> SmallRJetIsBTagged_DL1d_FixedCutBEff_77 = {fReader, "recojet_antikt4_NOSYS_DL1dv00_FixedCutBEff_77"};
   TTreeReaderArray<char> SmallRJetIsBTagged_DL1d_FixedCutBEff_85 = {fReader, "recojet_antikt4_NOSYS_DL1dv00_FixedCutBEff_85"};
   
   TTreeReaderArray<char> SmallRJetIsBTagged_GN1_FixedCutBEff_70 = {fReader, "recojet_antikt4_NOSYS_GN120220509_FixedCutBEff_70"};
   TTreeReaderArray<char> SmallRJetIsBTagged_GN1_FixedCutBEff_77 = {fReader, "recojet_antikt4_NOSYS_GN120220509_FixedCutBEff_77"};
   TTreeReaderArray<char> SmallRJetIsBTagged_GN1_FixedCutBEff_85 = {fReader, "recojet_antikt4_NOSYS_GN120220509_FixedCutBEff_85"};

  // this code is automatically generated by convert_trig
#ifdef trig13144
  TTreeReaderValue<Bool_t> trigPassed_HLT_2j35_gsc55_bmv2c1060_split_ht300_L1HT190_J15s5pETA21 = {fReader, "trigPassed_HLT_2j35_gsc55_bmv2c1060_split_ht300_L1HT190_J15s5pETA21"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j110_gsc150_boffperf_split_2j35_gsc55_bmv2c1070_split_L1J85_3J30 = {fReader, "trigPassed_HLT_j110_gsc150_boffperf_split_2j35_gsc55_bmv2c1070_split_L1J85_3J30"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j175_gsc225_bmv2c1040_split = {fReader, "trigPassed_HLT_j175_gsc225_bmv2c1040_split"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j420_a10t_lcw_jes_40smcINF_L1J100 = {fReader, "trigPassed_HLT_j420_a10t_lcw_jes_40smcINF_L1J100"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j225_gsc300_bmv2c1070_split = {fReader, "trigPassed_HLT_j225_gsc300_bmv2c1070_split"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j390_a10t_lcw_jes_30smcINF_L1J100 = {fReader, "trigPassed_HLT_j390_a10t_lcw_jes_30smcINF_L1J100"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j150_gsc175_bmv2c1060_split_j45_gsc60_bmv2c1060_split = {fReader, "trigPassed_HLT_j150_gsc175_bmv2c1060_split_j45_gsc60_bmv2c1060_split"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j460_a10t_lcw_jes_L1J100 = {fReader, "trigPassed_HLT_j460_a10t_lcw_jes_L1J100"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_2j35_gsc55_bmv2c1050_split_ht300_L1HT190_J15s5pETA21 = {fReader, "trigPassed_HLT_2j35_gsc55_bmv2c1050_split_ht300_L1HT190_J15s5pETA21"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_2j15_gsc35_bmv2c1040_split_2j15_gsc35_boffperf_split_L14J15p0ETA25 = {fReader, "trigPassed_HLT_2j15_gsc35_bmv2c1040_split_2j15_gsc35_boffperf_split_L14J15p0ETA25"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j225_gsc275_bmv2c1060_split = {fReader, "trigPassed_HLT_j225_gsc275_bmv2c1060_split"};
#endif
#ifdef trig13145
  TTreeReaderValue<Bool_t> trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1SC111 = {fReader, "trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1SC111"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j390_a10t_lcw_jes_30smcINF_L1J100 = {fReader, "trigPassed_HLT_j390_a10t_lcw_jes_30smcINF_L1J100"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j460_a10t_lcw_jes_L1SC111 = {fReader, "trigPassed_HLT_j460_a10t_lcw_jes_L1SC111"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j460_a10r_L1SC111 = {fReader, "trigPassed_HLT_j460_a10r_L1SC111"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j460_a10_lcw_subjes_L1J100 = {fReader, "trigPassed_HLT_j460_a10_lcw_subjes_L1J100"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j150_gsc175_bmv2c1060_split_j45_gsc60_bmv2c1060_split = {fReader, "trigPassed_HLT_j150_gsc175_bmv2c1060_split_j45_gsc60_bmv2c1060_split"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j175_gsc225_bmv2c1040_split = {fReader, "trigPassed_HLT_j175_gsc225_bmv2c1040_split"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j225_gsc275_bhmv2c1060_split = {fReader, "trigPassed_HLT_j225_gsc275_bhmv2c1060_split"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_2j35_bmv2c1060_split_2j35_L14J15p0ETA25 = {fReader, "trigPassed_HLT_2j35_bmv2c1060_split_2j35_L14J15p0ETA25"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j460_a10_lcw_subjes_L1SC111 = {fReader, "trigPassed_HLT_j460_a10_lcw_subjes_L1SC111"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j110_gsc150_boffperf_split_2j45_gsc55_bmv2c1070_split_L1J85_3J30 = {fReader, "trigPassed_HLT_j110_gsc150_boffperf_split_2j45_gsc55_bmv2c1070_split_L1J85_3J30"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j460_a10r_L1J100 = {fReader, "trigPassed_HLT_j460_a10r_L1J100"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j225_gsc300_bmv2c1070_split = {fReader, "trigPassed_HLT_j225_gsc300_bmv2c1070_split"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1J100 = {fReader, "trigPassed_HLT_j420_a10t_lcw_jes_35smcINF_L1J100"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_2j45_gsc55_bmv2c1050_split_ht300_L1HT190_J15s5pETA21 = {fReader, "trigPassed_HLT_2j45_gsc55_bmv2c1050_split_ht300_L1HT190_J15s5pETA21"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j460_a10t_lcw_jes_L1J100 = {fReader, "trigPassed_HLT_j460_a10t_lcw_jes_L1J100"};
#endif
#ifdef trig13167
  TTreeReaderValue<Bool_t> trigPassed_HLT_j100_2j55_bmv2c2060_split = {fReader, "trigPassed_HLT_j100_2j55_bmv2c2060_split"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j420_a10_lcw_L1J100 = {fReader, "trigPassed_HLT_j420_a10_lcw_L1J100"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j175_bmv2c2040_split = {fReader, "trigPassed_HLT_j175_bmv2c2040_split"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j225_bmv2c2060_split = {fReader, "trigPassed_HLT_j225_bmv2c2060_split"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j360_a10_lcw_sub_L1J100 = {fReader, "trigPassed_HLT_j360_a10_lcw_sub_L1J100"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j150_bmv2c2060_split_j50_bmv2c2060_split = {fReader, "trigPassed_HLT_j150_bmv2c2060_split_j50_bmv2c2060_split"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_2j35_btight_2j35_L13J25p0ETA23 = {fReader, "trigPassed_HLT_2j35_btight_2j35_L13J25p0ETA23"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_2j35_bmv2c2060_split_2j35_L14J15p0ETA25 = {fReader, "trigPassed_HLT_2j35_bmv2c2060_split_2j35_L14J15p0ETA25"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j100_2j55_bmedium = {fReader, "trigPassed_HLT_j100_2j55_bmedium"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j420_a10r_L1J100 = {fReader, "trigPassed_HLT_j420_a10r_L1J100"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_j225_bloose = {fReader, "trigPassed_HLT_j225_bloose"};
  TTreeReaderValue<Bool_t> trigPassed_HLT_2j55_bmv2c2060_split_ht300_L14J15 = {fReader, "trigPassed_HLT_2j55_bmv2c2060_split_ht300_L14J15"};
#endif

   float lumi;
   float mc_weighted;
   float dsid_weights;

   bool is2015;
   bool is2016;
   bool is2017;
   bool is2018;

   double rapidity_SH;
   double DR_SH ;
   double DPhi_SH ;
   double DEta_SH ;
   float Ratio_2m_pt ;

   float TotalEventsProcessed;
   float ExactlyOnePhoton_cut;
   float GamPt_cut;
   float Zcand_cut;
   float dphi_cut;
   float ZcandPt_cut;
   float Zwin_cut;
   float Xbb0tagZwin_cut;
   float Xbb50Zwin_cut;
   float Xbb60Zwin_cut;
   float Xbb70Zwin_cut;

//declaration of large R jet vars
   float lj1_mass;
   float lj1_pT;
   float lj1_eta;
   float lj1_phi;
   float lj2_mass;
   float lj2_pT;
   float lj2_eta;
   float lj2_phi;
   
   double DeltaR_truthB_S;
   double DeltaR_truthB_H;

   float lj1_truthS_mass;
   float lj1_truthS_pt;
   float lj1_truthS_phi;
   float lj1_truthS_eta;
   float lj2_truthS_mass;
   float lj2_truthS_pt;
   float lj2_truthS_phi;
   float lj2_truthS_eta;

   float lj1_truthH_mass;
   float lj1_truthH_pt;
   float lj1_truthH_phi;
   float lj1_truthH_eta;
   float lj2_truthH_mass;
   float lj2_truthH_pt;
   float lj2_truthH_phi;
   float lj2_truthH_eta;

   double dR_truthS_reco_lrj;
   double dR_truthH_reco_lrj;
//After kinematic cuts for large-R jets
   TH1D * truthleadfatjet_pT = NULL;
   TH1D * truthleadfatjet_eta = NULL;
   TH1D * truthleadfatjet_phi = NULL;
   TH1D * truthleadfatjet_mass = NULL;
   TH1D * truthsubleadfatjet_pT = NULL;
   TH1D * truthsubleadfatjet_eta = NULL;
   TH1D * truthsubleadfatjet_phi = NULL;
   TH1D * truthsubleadfatjet_mass = NULL;
   TH1D * LRJet1_mass = NULL;
   TH1D * LRJet1_pt = NULL;
   TH1D * LRJet1_eta = NULL;
   TH1D * LRJet1_phi = NULL;
   TH1D * LRJet2_mass = NULL;
   TH1D * LRJet2_pt = NULL;
   TH1D * LRJet2_eta = NULL;
   TH1D * LRJet2_phi = NULL;
   TH1D * Xcand_mass = NULL;
   TH1D * Xcand_pT = NULL;
   TH1D * Xcand_eta = NULL;
   TH1D * Xcand_phi = NULL;
   TH1D * Xcand_rapidity = NULL;
//After b-tagging without S and H assignment
   TH1D * LRJet1_mass_btag = NULL;
   TH1D * LRJet1_pt_btag = NULL;
   TH1D * LRJet1_eta_btag = NULL;
   TH1D * LRJet1_phi_btag = NULL;
   TH1D * LRJet2_mass_btag = NULL;
   TH1D * LRJet2_pt_btag = NULL;
   TH1D * LRJet2_eta_btag = NULL;
   TH1D * LRJet2_phi_btag = NULL;
   TH2D * LRJet1mass_vs_LRJet2mass_btag = NULL;
   TH1D * Xcand_mass_btag = NULL;
   TH1D * Xcand_pT_btag = NULL;
   TH1D * Xcand_eta_btag = NULL;
   TH1D * Xcand_phi_btag = NULL;
   TH1D * ratio_2m_pt_btag = NULL;
//After assigning S and H candidates without b-tagging
   TH1D * Scand_mass = NULL;
   TH1D * Scand_pT = NULL;
   TH1D * Scand_eta = NULL;
   TH1D * Scand_phi = NULL;
   TH1D * Scand_lrjlabel = NULL;
   TH1D * Hcand_mass = NULL;
   TH1D * Hcand_pT = NULL;
   TH1D * Hcand_eta = NULL;
   TH1D * Hcand_phi = NULL;
   TH1D * Xcand_mass_afterSorH = NULL;
   TH1D * Xcand_pT_afterSorH = NULL;
   TH1D * Xcand_eta_afterSorH = NULL;
   TH1D * Xcand_phi_afterSorH = NULL;
   TH1D * dR_SH_afterSorH = NULL;
   TH1D * dEta_SH_afterSorH = NULL;
   TH1D * dPhi_SH_afterSorH = NULL;
   TH1D * dRapidity_SH_afterSorH = NULL;
   TH2D * dR_SHvsmS = NULL;
   TH2D * dR_SHvsmH = NULL;
   TH2D * dEta_SHvsmS = NULL;
   TH2D * dEta_SHvsmH = NULL;
   TH2D * dPhi_SHvsmS = NULL;
   TH2D * dPhi_SHvsmH = NULL;
   TH2D * dRapidity_SHvsmS = NULL;
   TH2D * dRapidity_SHvsmH = NULL;
   TH2D * mSvsmX = NULL;
   TH2D * mHvsmX = NULL;
   TH2D * mSvsmH = NULL;
   TH1D * ratio_2m_pt_afterSorH = NULL;
//After assigning S and H candidates with 2B region
   TH1D * Scand_mass_2b = NULL;
   TH1D * Scand_pT_2b = NULL;
   TH1D * Scand_eta_2b = NULL;
   TH1D * Scand_phi_2b = NULL;
   TH1D * Hcand_mass_2b = NULL;
   TH1D * Hcand_pT_2b = NULL;
   TH1D * Hcand_eta_2b = NULL;
   TH1D * Hcand_phi_2b = NULL;
   TH1D * Xcand_mass_afterSorH_2b = NULL;
   TH1D * Xcand_pT_afterSorH_2b = NULL;
   TH1D * Xcand_eta_afterSorH_2b = NULL;
   TH1D * Xcand_phi_afterSorH_2b = NULL;
   TH1D * dR_SH_afterSorH_2b = NULL;
   TH1D * dEta_SH_afterSorH_2b = NULL;
   TH1D * dPhi_SH_afterSorH_2b = NULL;
   TH1D * dRapidity_SH_afterSorH_2b = NULL;
   TH2D * dR_SHvsmS_2b = NULL;
   TH2D * dR_SHvsmH_2b = NULL;
   TH2D * dEta_SHvsmS_2b = NULL;
   TH2D * dEta_SHvsmH_2b = NULL;
   TH2D * dPhi_SHvsmS_2b = NULL;
   TH2D * dPhi_SHvsmH_2b = NULL;
   TH2D * dRapidity_SHvsmS_2b = NULL;
   TH2D * dRapidity_SHvsmH_2b = NULL;
   TH2D * mSvsmX_2b = NULL;
   TH2D * mHvsmX_2b = NULL;
   TH2D * mSvsmH_2b = NULL;
//After assigning S and H candidates with 2B region
   TH1D * Scand_mass_4b = NULL;
   TH1D * Scand_pT_4b = NULL;
   TH1D * Scand_eta_4b = NULL;
   TH1D * Scand_phi_4b = NULL;
   TH1D * Hcand_mass_4b = NULL;
   TH1D * Hcand_pT_4b = NULL;
   TH1D * Hcand_eta_4b = NULL;
   TH1D * Hcand_phi_4b = NULL;
   TH1D * Xcand_mass_afterSorH_4b = NULL;
   TH1D * Xcand_pT_afterSorH_4b = NULL;
   TH1D * Xcand_eta_afterSorH_4b = NULL;
   TH1D * Xcand_phi_afterSorH_4b = NULL;
   TH1D * dR_SH_afterSorH_4b = NULL;
   TH1D * dEta_SH_afterSorH_4b = NULL;
   TH1D * dPhi_SH_afterSorH_4b = NULL;
   TH1D * dRapidity_SH_afterSorH_4b = NULL;
   TH2D * dR_SHvsmS_4b = NULL;
   TH2D * dR_SHvsmH_4b = NULL;
   TH2D * dEta_SHvsmS_4b = NULL;
   TH2D * dEta_SHvsmH_4b = NULL;
   TH2D * dPhi_SHvsmS_4b = NULL;
   TH2D * dPhi_SHvsmH_4b = NULL;
   TH2D * dRapidity_SHvsmS_4b = NULL;
   TH2D * dRapidity_SHvsmH_4b = NULL;
   TH2D * mSvsmX_4b = NULL;
   TH2D * mHvsmX_4b = NULL;
   TH2D * mSvsmH_4b = NULL;
   TH1D * ratio_2m_pt_afterSorH_4b = NULL;
//After deta cut; assigning S and H candidates with 2B region
   TH1D * Scand_mass_deta_4b_SR1 = NULL;
   TH1D * Scand_pT_deta_4b_SR1 = NULL;
   TH1D * Scand_eta_deta_4b_SR1 = NULL;
   TH1D * Scand_phi_deta_4b_SR1 = NULL;
   TH1D * Hcand_mass_deta_4b_SR1 = NULL;
   TH1D * Hcand_pT_deta_4b_SR1 = NULL;
   TH1D * Hcand_eta_deta_4b_SR1 = NULL;
   TH1D * Hcand_phi_deta_4b_SR1 = NULL;
   TH1D * Xcand_mass_afterSorH_deta_4b_SR1 = NULL;
   TH1D * Xcand_pT_afterSorH_deta_4b_SR1 = NULL;
   TH1D * Xcand_eta_afterSorH_deta_4b_SR1 = NULL;
   TH1D * Xcand_phi_afterSorH_deta_4b_SR1 = NULL;
   TH1D * dR_SH_afterSorH_deta_4b_SR1 = NULL;
   TH1D * dEta_SH_afterSorH_deta_4b_SR1 = NULL;
   TH1D * dPhi_SH_afterSorH_deta_4b_SR1 = NULL;
   TH1D * dRapidity_SH_afterSorH_deta_4b_SR1 = NULL;
   TH2D * mSvsmX_deta_4b_SR1 = NULL;
   TH2D * mHvsmX_deta_4b_SR1 = NULL;
   TH2D * mSvsmH_deta_4b_SR1 = NULL;
//CR, VR of method 1 following cms analysis
   TH1D * Scand_mass_CR_FP = NULL;
   TH1D * Scand_pT_CR_FP = NULL;
   TH1D * Scand_eta_CR_FP = NULL;
   TH1D * Scand_phi_CR_FP = NULL;
   TH1D * Hcand_mass_CR_FP = NULL;
   TH1D * Hcand_pT_CR_FP = NULL;
   TH1D * Hcand_eta_CR_FP = NULL;
   TH1D * Hcand_phi_CR_FP = NULL;
   TH1D * Xcand_mass_afterSorH_CR_FP = NULL;
   TH1D * Xcand_pT_afterSorH_CR_FP = NULL;
   TH1D * Xcand_eta_afterSorH_CR_FP = NULL;
   TH1D * Xcand_phi_afterSorH_CR_FP = NULL;
   TH2D * mSvsmX_CR_FP = NULL;
   TH2D * mHvsmX_CR_FP = NULL;
   TH2D * mSvsmH_CR_FP = NULL;
//CR, VR of method 1 following cms analysis
   TH1D * Scand_mass_CR_PF = NULL;
   TH1D * Scand_pT_CR_PF = NULL;
   TH1D * Scand_eta_CR_PF = NULL;
   TH1D * Scand_phi_CR_PF = NULL;
   TH1D * Hcand_mass_CR_PF = NULL;
   TH1D * Hcand_pT_CR_PF = NULL;
   TH1D * Hcand_eta_CR_PF = NULL;
   TH1D * Hcand_phi_CR_PF = NULL;
   TH1D * Xcand_mass_afterSorH_CR_PF = NULL;
   TH1D * Xcand_pT_afterSorH_CR_PF = NULL;
   TH1D * Xcand_eta_afterSorH_CR_PF = NULL;
   TH1D * Xcand_phi_afterSorH_CR_PF = NULL;
   TH2D * mSvsmX_CR_PF = NULL;
   TH2D * mHvsmX_CR_PF = NULL;
   TH2D * mSvsmH_CR_PF = NULL;
//CR, VR of method 1 following cms analysis
   TH1D * Scand_mass_CR_FF = NULL;
   TH1D * Scand_pT_CR_FF = NULL;
   TH1D * Scand_eta_CR_FF = NULL;
   TH1D * Scand_phi_CR_FF = NULL;
   TH1D * Hcand_mass_CR_FF = NULL;
   TH1D * Hcand_pT_CR_FF = NULL;
   TH1D * Hcand_eta_CR_FF = NULL;
   TH1D * Hcand_phi_CR_FF = NULL;
   TH1D * Xcand_mass_afterSorH_CR_FF = NULL;
   TH1D * Xcand_pT_afterSorH_CR_FF = NULL;
   TH1D * Xcand_eta_afterSorH_CR_FF = NULL;
   TH1D * Xcand_phi_afterSorH_CR_FF = NULL;
   TH2D * mSvsmX_CR_FF = NULL;
   TH2D * mHvsmX_CR_FF = NULL;
   TH2D * mSvsmH_CR_FF = NULL;
//CR, VR of method 1 following cms analysis
   TH1D * Scand_mass_VR_FP = NULL;
   TH1D * Scand_pT_VR_FP = NULL;
   TH1D * Scand_eta_VR_FP = NULL;
   TH1D * Scand_phi_VR_FP = NULL;
   TH1D * Hcand_mass_VR_FP = NULL;
   TH1D * Hcand_pT_VR_FP = NULL;
   TH1D * Hcand_eta_VR_FP = NULL;
   TH1D * Hcand_phi_VR_FP = NULL;
   TH1D * Xcand_mass_afterSorH_VR_FP = NULL;
   TH1D * Xcand_pT_afterSorH_VR_FP = NULL;
   TH1D * Xcand_eta_afterSorH_VR_FP = NULL;
   TH1D * Xcand_phi_afterSorH_VR_FP = NULL;
   TH2D * mSvsmX_VR_FP = NULL;
   TH2D * mHvsmX_VR_FP = NULL;
   TH2D * mSvsmH_VR_FP = NULL;
//CR, VR of method 1 following cms analysis
   TH1D * Scand_mass_VR_PF = NULL;
   TH1D * Scand_pT_VR_PF = NULL;
   TH1D * Scand_eta_VR_PF = NULL;
   TH1D * Scand_phi_VR_PF = NULL;
   TH1D * Hcand_mass_VR_PF = NULL;
   TH1D * Hcand_pT_VR_PF = NULL;
   TH1D * Hcand_eta_VR_PF = NULL;
   TH1D * Hcand_phi_VR_PF = NULL;
   TH1D * Xcand_mass_afterSorH_VR_PF = NULL;
   TH1D * Xcand_pT_afterSorH_VR_PF = NULL;
   TH1D * Xcand_eta_afterSorH_VR_PF = NULL;
   TH1D * Xcand_phi_afterSorH_VR_PF = NULL;
   TH2D * mSvsmX_VR_PF = NULL;
   TH2D * mHvsmX_VR_PF = NULL;
   TH2D * mSvsmH_VR_PF = NULL;
// Second SR
   TH1D * Scand_mass_SR2 = NULL;
   TH1D * Scand_pT_SR2 = NULL;
   TH1D * Scand_eta_SR2 = NULL;
   TH1D * Scand_phi_SR2 = NULL;
   TH1D * Hcand_mass_SR2 = NULL;
   TH1D * Hcand_pT_SR2 = NULL;
   TH1D * Hcand_eta_SR2 = NULL;
   TH1D * Hcand_phi_SR2 = NULL;
   TH1D * Xcand_mass_afterSorH_SR2 = NULL;
   TH1D * Xcand_pT_afterSorH_SR2 = NULL;
   TH1D * Xcand_eta_afterSorH_SR2 = NULL;
   TH1D * Xcand_phi_afterSorH_SR2 = NULL;
   TH2D * mSvsmX_SR2 = NULL;
   TH2D * mHvsmX_SR2 = NULL;
   TH2D * mSvsmH_SR2 = NULL;
// Second SR
   TH1D * Scand_mass_CR0 = NULL;
   TH1D * Scand_pT_CR0 = NULL;
   TH1D * Scand_eta_CR0 = NULL;
   TH1D * Scand_phi_CR0 = NULL;
   TH1D * Hcand_mass_CR0 = NULL;
   TH1D * Hcand_pT_CR0 = NULL;
   TH1D * Hcand_eta_CR0 = NULL;
   TH1D * Hcand_phi_CR0 = NULL;
   TH1D * Xcand_mass_afterSorH_CR0 = NULL;
   TH1D * Xcand_pT_afterSorH_CR0 = NULL;
   TH1D * Xcand_eta_afterSorH_CR0 = NULL;
   TH1D * Xcand_phi_afterSorH_CR0 = NULL;
   TH2D * mSvsmX_CR0 = NULL;
   TH2D * mHvsmX_CR0 = NULL;
   TH2D * mSvsmH_CR0 = NULL;
// Second SR
   TH1D * Scand_mass_CR_HSB1 = NULL;
   TH1D * Scand_pT_CR_HSB1 = NULL;
   TH1D * Scand_eta_CR_HSB1 = NULL;
   TH1D * Scand_phi_CR_HSB1 = NULL;
   TH1D * Hcand_mass_CR_HSB1 = NULL;
   TH1D * Hcand_pT_CR_HSB1 = NULL;
   TH1D * Hcand_eta_CR_HSB1 = NULL;
   TH1D * Hcand_phi_CR_HSB1 = NULL;
   TH1D * Xcand_mass_afterSorH_CR_HSB1 = NULL;
   TH1D * Xcand_pT_afterSorH_CR_HSB1 = NULL;
   TH1D * Xcand_eta_afterSorH_CR_HSB1 = NULL;
   TH1D * Xcand_phi_afterSorH_CR_HSB1 = NULL;
   TH2D * mSvsmX_CR_HSB1 = NULL;
   TH2D * mHvsmX_CR_HSB1 = NULL;
   TH2D * mSvsmH_CR_HSB1 = NULL;
// Second SR
   TH1D * Scand_mass_CR_HSB2 = NULL;
   TH1D * Scand_pT_CR_HSB2 = NULL;
   TH1D * Scand_eta_CR_HSB2 = NULL;
   TH1D * Scand_phi_CR_HSB2 = NULL;
   TH1D * Hcand_mass_CR_HSB2 = NULL;
   TH1D * Hcand_pT_CR_HSB2 = NULL;
   TH1D * Hcand_eta_CR_HSB2 = NULL;
   TH1D * Hcand_phi_CR_HSB2 = NULL;
   TH1D * Xcand_mass_afterSorH_CR_HSB2 = NULL;
   TH1D * Xcand_pT_afterSorH_CR_HSB2 = NULL;
   TH1D * Xcand_eta_afterSorH_CR_HSB2 = NULL;
   TH1D * Xcand_phi_afterSorH_CR_HSB2 = NULL;
   TH2D * mSvsmX_CR_HSB2 = NULL;
   TH2D * mHvsmX_CR_HSB2 = NULL;
   TH2D * mSvsmH_CR_HSB2 = NULL;
// Second SR
   TH1D * Scand_mass_VR_LSB1 = NULL;
   TH1D * Scand_pT_VR_LSB1 = NULL;
   TH1D * Scand_eta_VR_LSB1 = NULL;
   TH1D * Scand_phi_VR_LSB1 = NULL;
   TH1D * Hcand_mass_VR_LSB1 = NULL;
   TH1D * Hcand_pT_VR_LSB1 = NULL;
   TH1D * Hcand_eta_VR_LSB1 = NULL;
   TH1D * Hcand_phi_VR_LSB1 = NULL;
   TH1D * Xcand_mass_afterSorH_VR_LSB1 = NULL;
   TH1D * Xcand_pT_afterSorH_VR_LSB1 = NULL;
   TH1D * Xcand_eta_afterSorH_VR_LSB1 = NULL;
   TH1D * Xcand_phi_afterSorH_VR_LSB1 = NULL;
   TH2D * mSvsmX_VR_LSB1 = NULL;
   TH2D * mHvsmX_VR_LSB1 = NULL;
   TH2D * mSvsmH_VR_LSB1 = NULL;
// Second SR
   TH1D * Scand_mass_VR_LSB2 = NULL;
   TH1D * Scand_pT_VR_LSB2 = NULL;
   TH1D * Scand_eta_VR_LSB2 = NULL;
   TH1D * Scand_phi_VR_LSB2 = NULL;
   TH1D * Hcand_mass_VR_LSB2 = NULL;
   TH1D * Hcand_pT_VR_LSB2 = NULL;
   TH1D * Hcand_eta_VR_LSB2 = NULL;
   TH1D * Hcand_phi_VR_LSB2 = NULL;
   TH1D * Xcand_mass_afterSorH_VR_LSB2 = NULL;
   TH1D * Xcand_pT_afterSorH_VR_LSB2 = NULL;
   TH1D * Xcand_eta_afterSorH_VR_LSB2 = NULL;
   TH1D * Xcand_phi_afterSorH_VR_LSB2 = NULL;
   TH2D * mSvsmX_VR_LSB2 = NULL;
   TH2D * mHvsmX_VR_LSB2 = NULL;
   TH2D * mSvsmH_VR_LSB2 = NULL;
//For trigger study
   TH1D * Jet_mass = NULL;
   TH1D * Jet_pt = NULL;
   TH1D * Jet_eta = NULL;
   TH1D * Jet_phi = NULL;
   TH1D * leadjetMass_beforeTrig_pT250_M100 = NULL;
   TH1D * leadjetPt_beforeTrig_pT250_M100 = NULL;
   TH1D * leadjetMass_beforeTrig_pT500_M40 = NULL;
   TH1D * leadjetPt_beforeTrig_pT500_M40 = NULL;

   TH1D * leadjetPt_afterTrig_pT250_M100 = NULL;
   TH1D * leadjetMass_afterTrig_pT250_M100 = NULL;

   TH1D * leadjetPt_afterTrig_pT500_M40 = NULL;
   TH1D * leadjetMass_afterTrig_pT500_M40 = NULL;
   
   TH1D * leadjetPt_afterTrig390_pT250_M100 = NULL;
   TH1D * leadjetMass_afterTrig390_pT250_M100 = NULL;

   TH1D * leadjetPt_afterTrig390_pT500_M40 = NULL;
   TH1D * leadjetMass_afterTrig390_pT500_M40 = NULL;

   TH1D * subleadjetMass_beforeTrig = NULL;
   TH1D * subleadjetPt_beforeTrig = NULL;
   TH1D * mJJ_beforeTrig_pT450_M50 = NULL;
   TH1D * mJJ_afterTrig420_L1J100_pT450_M50 = NULL;
   TH1D * mJJ_afterTrig390_L1J100_pT450_M50 = NULL;
   TH1D * mJJ_afterTrig420_L1SC111_pT450_M50 = NULL;
   TH1D * mJJ_afterTrig460_a10r_L1SC111_pT450_M50 = NULL;
   TH1D * mJJ_afterTrig460_a10r_L1J100_pT450_M50 = NULL;
   TH1D * mJJ_afterTrig460_a10_subjes_L1SC111_pT450_M50 = NULL;
   TH1D * mJJ_afterTrig460_a10_subjes_L1J100_pT450_M50 = NULL;
   TH1D * mJJ_afterTrig460_a10t_L1SC111_pT450_M50 = NULL;
   TH1D * mJJ_afterTrig460_a10t_L1J100_pT450_M50 = NULL;
   //After truth matching
   TH1D * LRJet_mass_tmatch_S = NULL;
   TH1D * LRJet_pt_tmatch_S = NULL;
   TH1D * LRJet_eta_tmatch_S = NULL;
   TH1D * LRJet_phi_tmatch_S = NULL;
   
   TH1D * Xcand_mass_tmatch = NULL;
   TH1D * Xcand_pT_tmatch = NULL;
   TH1D * Xcand_eta_tmatch = NULL;
   TH1D * Xcand_phi_tmatch = NULL;
   TH1D * Xcand_rapidity_tmatch = NULL;
   TH1D * LRJet_mass_tmatch_H = NULL;
   TH1D * LRJet_pt_tmatch_H = NULL;
   TH1D * LRJet_eta_tmatch_H = NULL;
   TH1D * LRJet_phi_tmatch_H = NULL;
   
   TH1D * LRJet_mass_tmatch_S_lrjlabel = NULL;
   TH1D * LRJet_pt_tmatch_S_lrjlabel = NULL;
   TH1D * LRJet_eta_tmatch_S_lrjlabel = NULL;
   TH1D * LRJet_phi_tmatch_S_lrjlabel = NULL;
   TH1D * LRJet_mass_tmatch_H_lrjlabel = NULL;
   TH1D * LRJet_pt_tmatch_H_lrjlabel = NULL;
   TH1D * LRJet_eta_tmatch_H_lrjlabel = NULL;
   TH1D * LRJet_phi_tmatch_H_lrjlabel = NULL;
   TH1D * LRJet_mass_tmatch_btag_S = NULL;
   TH1D * LRJet_pt_tmatch_btag_S = NULL;
   TH1D * LRJet_eta_tmatch_btag_S = NULL;
   TH1D * LRJet_phi_tmatch_btag_S = NULL;
   
   TH1D * Xcand_mass_tmatch_btag = NULL;
   TH1D * Xcand_pT_tmatch_btag = NULL;
   TH1D * Xcand_eta_tmatch_btag = NULL;
   TH1D * Xcand_phi_tmatch_btag = NULL;
   TH1D * Xcand_rapidity_tmatch_btag = NULL;
   TH1D * LRJet_mass_tmatch_btag_H = NULL;
   TH1D * LRJet_pt_tmatch_btag_H = NULL;
   TH1D * LRJet_eta_tmatch_btag_H = NULL;
   TH1D * LRJet_phi_tmatch_btag_H = NULL;

   TH2D * mSvsmX_tmatch = NULL;
   TH2D * mHvsmX_tmatch = NULL;
   TH2D * mSvsmH_tmatch = NULL;
   TH2D * mSvsmX_tmatch_btag = NULL;
   TH2D * mHvsmX_tmatch_btag = NULL;
   TH2D * mSvsmH_tmatch_btag = NULL;
   TH1D * Truth_b_S_deltaR = NULL;
   TH1D * Truth_b_H_deltaR = NULL;

   TLorentzVector leading_Higgs;   
   TH1D * Num_largeRj = NULL;
   TH1D * Num_largeRj_ufo = NULL;
   TH1D * Num_smallRj = NULL;
   TH1D * Num_largeRj_2btag_77 = NULL;
   TH1D * Num_largeRj_2btag_85 = NULL;
   TH1D * Num_largeRj_2btag_85_77 = NULL;
   TH1D * Num_largeRj_ufo_2btag_77 = NULL;
   TH1D * Num_largeRj_ufo_2btag_85 = NULL;
   TH1D * Num_smallRj_b_77 = NULL;
   TH1D * Num_smallRj_b_85 = NULL;
   TH1D * Num_smallRj_b_GN1_70 = NULL;
   TH1D * Num_smallRj_b_GN1_77 = NULL;
   TH1D * Num_smallRj_b_GN1_85 = NULL;
    
   TH1D * Num_VRjets = NULL;
   TH1D * Num_VRjets_DL1r_77 = NULL;
   TH1D * Num_VRjets_DL1r_85 = NULL;
   TH1D * Num_VRjets_ufo = NULL;
   TH1D * Num_VRjets_DL1r_77_ufo = NULL;
   TH1D * Num_VRjets_DL1r_85_ufo = NULL;
   TH1D * Num_bjets_Higgs = NULL;
   TH1D * Num_bjets_S = NULL;
   TH1D * Xcand_mass_b77 = NULL;
   
   TH1D * deltaR_truthS_reco_lrj = NULL;
   TH1D * deltaR_truthH_reco_lrj = NULL;
   TH1D * deltaR_truthS_reco_lrj_btag = NULL;
   TH1D * deltaR_truthH_reco_lrj_btag = NULL;
 

   TH1D * Cutflow = NULL;
   TH1D * TotalEvents = NULL;
   TH1D * Trig_420 = NULL;
   TH1D * AtLeast2lrj = NULL;
   TH1D * AtLeast2Bs_lrj = NULL;

   TEfficiency * trig = 0;
   std::vector<TH1*> histlist;

   Nominal(TTree * /*tree*/ =0) { }
   virtual ~Nominal() { }
   virtual void    Begin(TTree *tree);
   virtual void    Init(TTree *tree);
   virtual Bool_t  Notify();
   virtual Bool_t  Process(Long64_t entry,int index);
   virtual void    Terminate(TString mc_filename,int i);

   //Bool_t Trigger(TTreeReaderValue<uint32_t> RunNumber);

   TH1D * makeHist(TString name,int nbins,double x1,double x2);
   TH1F * makeHist_F(TString name,int nbins,double x1,double x2);
   TH2D * make2DHist(TString name,int nbins,double x1,double x2,int nbinsy,double y1,double y2);

   ClassDef(Nominal,0);

};

#endif

#ifdef Nominal_cxx
void Nominal::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the reader is initialized.
   fReader.SetTree(tree);
}

Bool_t Nominal::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be for a new TTree in a TChain.

   return kTRUE;
}


#endif

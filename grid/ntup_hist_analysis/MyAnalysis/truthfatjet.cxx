#define truthfatjet_cxx

#include "truthfatjet.h"
#include <iostream>

truthfatjet::truthfatjet(){
}

truthfatjet::~truthfatjet(){
}
bool truthfatjet::isSelected() {
  if (Pt() < 250.) return false;
  if (Eta() > 2. || Eta() < -2.) return false;
  if (M() < 50.) return false;
  //if (2*M()/Pt() > 1.) return false;
 
  return true;
}

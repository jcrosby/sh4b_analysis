#ifndef smallrjet_h
#define smallrjet_h

#include <TLorentzVector.h>
#include "fatjet.h"

class fatjet;

class smallrjet : public TLorentzVector {
 public :
  fatjet * fatjetPointer;
 //std::vector<smallrjet*> smallRJetPointers;
 int truthlabel;
 int DL1d_77;
 int DL1d_85;
 int GN1_70;
 int GN1_77;
 int GN1_85;
  smallrjet();
  ~smallrjet();
  void AddFatJetPointer(fatjet * next);
  
  ClassDef(smallrjet,0);
};

#endif // #ifdef smallrjet_h

#ifndef truthfatjet_h
#define truthfatjet_h

#include <TLorentzVector.h>

class truthfatjet : public TLorentzVector {
 public:
  truthfatjet();
  ~truthfatjet();
  
  bool isSelected();

  ClassDef(truthfatjet,0);
};

#endif // #ifdef truthfatjet_h

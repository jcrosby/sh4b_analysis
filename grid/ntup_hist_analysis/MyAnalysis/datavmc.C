#include <TGraph.h>
#include <TGraphErrors.h>
#include <TLatex.h>
#include <TCanvas.h>
#include <TLegend.h>
#include <TFile.h>
#include <TString.h>
#include <TProfile.h>
#include <TFitResult.h>
#include <TH1D.h>
#include <TF1.h>
#include <TF2.h>
#include <TLine.h>
#include <vector>
#include <iostream>

#include "AtlasStyle.C"
#include "AtlasLabels.C"

const int nfile = 6;

TString fileNames[nfile] = {"gamma_jets.root","ttgamma.root","Wqq.root","ZqqJets.root","Zqq.root","data.root"};

void drawplot(TString name,TH1D** hists);
template <class THnD> int readplots(TFile ** filePointers,THnD ** histPointers,TString name);

void datavmc() {

  TH1D* Zcand_Zwin_mass[nfile];
  TH1D* Zcand_Zwin_pt[nfile];
  TH1D* Zcand_Zwin_eta[nfile];
  TH1D* Zcand_Zwin_phi[nfile];

  TH1D * Zcand_Zwin_sr_Xbb_50_mass[nfile];
  TH1D * Zcand_Zwin_sr_Xbb_50_pt[nfile];
  TH1D * Zcand_Zwin_sr_Xbb_50_eta[nfile];
  TH1D * Zcand_Zwin_sr_Xbb_50_phi[nfile];

  TH1D * Zcand_Zwin_sr_Xbb_60_mass[nfile];
  TH1D * Zcand_Zwin_sr_Xbb_60_pt[nfile];
  TH1D * Zcand_Zwin_sr_Xbb_60_eta[nfile];
  TH1D * Zcand_Zwin_sr_Xbb_60_phi[nfile];

  TH1D * Zcand_Zwin_sr_Xbb_70_mass[nfile];
  TH1D * Zcand_Zwin_sr_Xbb_70_pt[nfile];
  TH1D * Zcand_Zwin_sr_Xbb_70_eta[nfile];
  TH1D * Zcand_Zwin_sr_Xbb_70_phi[nfile];

  //SetAtlasStyle();

  TString dir = "TemplatesLatest/";
  TFile * filePointers[nfile];
  
  for (int i=0;i<nfile;++i) {
    filePointers[i] = TFile::Open(dir+fileNames[i]);
    if (!filePointers[i]) {
      std::cout<<"File "<<dir+fileNames[i]<<" could not be opened"<<std::endl;
    }
  }

  std::cout<<"All files have been opened. Beginning to read plots."<<std::endl;

  readplots(filePointers,Zcand_Zwin_mass,"Zcand_Zwin_mass");
  readplots(filePointers,Zcand_Zwin_pt,"Zcand_Zwin_pt");
  readplots(filePointers,Zcand_Zwin_eta,"Zcand_Zwin_eta");
  readplots(filePointers,Zcand_Zwin_phi,"Zcand_Zwin_phi");

  readplots(filePointers,Zcand_Zwin_sr_Xbb_50_mass,"Zcand_Zwin_sr_Xbb_50_mass");
  readplots(filePointers,Zcand_Zwin_sr_Xbb_50_pt,"Zcand_Zwin_sr_Xbb_50_pt");
  readplots(filePointers,Zcand_Zwin_sr_Xbb_50_eta,"Zcand_Zwin_sr_Xbb_50_eta");
  readplots(filePointers,Zcand_Zwin_sr_Xbb_50_phi,"Zcand_Zwin_sr_Xbb_50_phi");

  readplots(filePointers,Zcand_Zwin_sr_Xbb_60_mass,"Zcand_Zwin_sr_Xbb_60_mass");
  readplots(filePointers,Zcand_Zwin_sr_Xbb_60_pt,"Zcand_Zwin_sr_Xbb_60_pt");
  readplots(filePointers,Zcand_Zwin_sr_Xbb_60_eta,"Zcand_Zwin_sr_Xbb_60_eta");
  readplots(filePointers,Zcand_Zwin_sr_Xbb_60_phi,"Zcand_Zwin_sr_Xbb_60_phi");

  readplots(filePointers,Zcand_Zwin_sr_Xbb_70_mass,"Zcand_Zwin_sr_Xbb_70_mass");
  readplots(filePointers,Zcand_Zwin_sr_Xbb_70_pt,"Zcand_Zwin_sr_Xbb_70_pt");
  readplots(filePointers,Zcand_Zwin_sr_Xbb_70_eta,"Zcand_Zwin_sr_Xbb_70_eta");
  readplots(filePointers,Zcand_Zwin_sr_Xbb_70_phi,"Zcand_Zwin_sr_Xbb_70_phi");

  std::cout<<"All plots have been read. Beginning to draw plots."<<std::endl;

  drawplot("Zcand_Zwin_mass",Zcand_Zwin_mass);
  drawplot("Zcand_Zwin_pt",Zcand_Zwin_pt);
  drawplot("Zcand_Zwin_eta",Zcand_Zwin_eta);
  drawplot("Zcand_Zwin_phi",Zcand_Zwin_phi);

  drawplot("Zcand_Zwin_sr_Xbb_50_mass",Zcand_Zwin_sr_Xbb_50_mass);
  drawplot("Zcand_Zwin_sr_Xbb_50_pt",Zcand_Zwin_sr_Xbb_50_pt);
  drawplot("Zcand_Zwin_sr_Xbb_50_eta",Zcand_Zwin_sr_Xbb_50_eta);
  drawplot("Zcand_Zwin_sr_Xbb_50_phi",Zcand_Zwin_sr_Xbb_50_phi);

  drawplot("Zcand_Zwin_sr_Xbb_60_mass",Zcand_Zwin_sr_Xbb_60_mass);
  drawplot("Zcand_Zwin_sr_Xbb_60_pt",Zcand_Zwin_sr_Xbb_60_pt);
  drawplot("Zcand_Zwin_sr_Xbb_60_eta",Zcand_Zwin_sr_Xbb_60_eta);
  drawplot("Zcand_Zwin_sr_Xbb_60_phi",Zcand_Zwin_sr_Xbb_60_phi);

  drawplot("Zcand_Zwin_sr_Xbb_70_mass",Zcand_Zwin_sr_Xbb_70_mass);
  drawplot("Zcand_Zwin_sr_Xbb_70_pt",Zcand_Zwin_sr_Xbb_70_pt);
  drawplot("Zcand_Zwin_sr_Xbb_70_eta",Zcand_Zwin_sr_Xbb_70_eta);
  drawplot("Zcand_Zwin_sr_Xbb_70_phi",Zcand_Zwin_sr_Xbb_70_phi);

  std::cout<<"All plots have been drawn."<<std::endl;

}

void drawplot(TString name,TH1D** hists) {

  TCanvas * can = new TCanvas("can "+name,"can "+name,800,600);
  can->SetRightMargin(0.1);
  can->SetLeftMargin(0.15);

  can->cd(); 
  TPad * pad1 = new TPad("pad1","pad1",0,0.35,1,1);
  pad1->SetLeftMargin(0.15);
  pad1->SetBottomMargin(0.01);
  //pad1->SetTopMargin(1.5); 
  pad1->SetRightMargin(0.08);
  pad1->Draw();   
  pad1->SetTicks();   
  
  can->cd();
  
  TPad * pad2 = new TPad("pad2","pad2",0,0.0,1,0.35);  
  pad2->SetLeftMargin(0.15); 
  pad2->SetRightMargin(0.08);  
  pad2->SetTopMargin(0.1); 
  pad2->SetBottomMargin(0.35);     
  pad2->Draw();
  pad2->SetTicks(); 

  TLegend * leg = new TLegend(0.73,0.5,0.9,0.88);
  leg->SetTextFont(42);
  leg->SetTextSize(0.04);
  leg->SetBorderSize(0);
  leg->SetMargin(0.25);

  double_t x = 0.2;
  double_t y = 0.8;
  TLatex * l = new TLatex(x,y,"ATLAS");
  l->SetNDC();
  l->SetTextFont(72);
  l->SetTextSize(0.05);
  l->SetTextColor(1);
  
  TLatex * p = new TLatex(x+0.08,y,"Work in Progress");
  p->SetNDC();
  p->SetTextFont(42);
  p->SetTextSize(0.05);
  p->SetTextColor(1);
  
  TLatex * r = new TLatex(x,0.73,"#sqrt{s} = 13 TeV, 139 fb^{-1}");
  r->SetNDC();
  r->SetTextFont(42);
  r->SetTextColor(1);
  r->SetTextSize(0.045);

  TLatex * n = new TLatex(x,0.65,"200 < p_{T}(J) < 450 GeV, p_{T}(#gamma) > 175 GeV");
  n->SetNDC();
  n->SetTextFont(42);
  n->SetTextColor(1);
  n->SetTextSize(0.045);

  TLatex * Xbb50 = new TLatex(x+0.3,0.8,"Xbb 50%");
  Xbb50->SetNDC();
  Xbb50->SetTextFont(72);
  Xbb50->SetTextColor(kOrange+2);
  Xbb50->SetTextSize(0.045);
  TLatex * Xbb60 = new TLatex(x+0.3,0.8,"Xbb 60%");
  Xbb60->SetNDC();
  Xbb60->SetTextFont(72);
  Xbb60->SetTextColor(kOrange+2);
  Xbb60->SetTextSize(0.045);
  TLatex * Xbb70 = new TLatex(x+0.3,0.8,"Xbb 70%");
  Xbb70->SetNDC();
  Xbb70->SetTextFont(72);
  Xbb70->SetTextColor(kOrange+2);
  Xbb70->SetTextSize(0.045);

  TLatex * v = new TLatex(x+0.3,0.8,"Pre-tagging");
  v->SetNDC();
  v->SetTextFont(42);
  v->SetTextColor(1);
  v->SetTextSize(0.045);

  for (int i=0;i<nfile;++i) {
    if (hists[i] == 0) {
      std::cout<<"Histogram "<<i<<" is empty."<<std::endl;
      continue;
    }
    else {
      if ( name.Contains("mass") ) hists[i]->Rebin(5);
      else if ( name.Contains("pt") ) hists[i]->Rebin(32);
      else if ( name.Contains("eta") ) hists[i]->Rebin(5);
      else if ( name.Contains("phi") ) hists[i]->Rebin(5);
    }
  }

  THStack * data_v_mc_stacked = new THStack();
  int cols[5] = {kBlue,kGreen,kOrange+2,kMagenta,kRed};
  TString types[5] = {"#gamma+jets","tt#gamma","Wqq#gamma","Zqq+Jets","Zqq#gamma"};
  int col=0;
  double y1 = 0;
  double y2 = -1E30;

  double nBinx=0; 
 
  for (int i=0;i<nfile;++i) { 
    if (hists[i]->GetMinimum() < y1) y1 = hists[i]->GetMinimum();   
    if (hists[i]->GetMaximum() > y2) y2 = hists[i]->GetMaximum()+(hists[i]->GetMaximum())*1.2;

    nBinx = hists[i]->GetNbinsX();

    Double_t overflowerror = hists[i]->GetBinError(nBinx+1);
    if(nBinx>0){ //Adding get bin content--overflow
      hists[i]->SetBinContent(nBinx+1, hists[i]->GetBinContent(nBinx)+hists[i]->GetBinContent(nBinx+1));

    // Adding bin error--overflow
      hists[i]->SetBinError(nBinx, sqrt(pow(hists[i]->GetBinError(nBinx),2) + pow(overflowerror,2)));
      hists[i]->SetBinError(nBinx+1, 0);

    }
  }

  for (int i=0;i<5;++i) {    
    if (hists[i] != 0) {
      col=cols[i];
      hists[i]->SetFillColor(col);
      hists[i]->SetLineColor(col);
      
      leg->AddEntry(hists[i],types[i],"f");
      data_v_mc_stacked->Add(hists[i]);
    }
    else continue;
  }

  TH1F* all_MC = (TH1F*)hists[0]->Clone("all_MC"); //is sum_histo in leptonic case and sum_histo is statistical error bars on the stacked histograms
  all_MC->Add(hists[1]);
  all_MC->Add(hists[2]);
  all_MC->Add(hists[3]);
  all_MC->Add(hists[4]);
  //for (int i=1;i<5; i++) { 
   // background_all->Add(hists[i]);
  //}
 
  pad1->cd();   
  //Drawing the stacked MC and Data in the upper pad
  data_v_mc_stacked->SetMinimum(y1);   
  data_v_mc_stacked->SetMaximum(y2);   
  data_v_mc_stacked->Draw("hist");

// Drawing statistical uncertainties on the stacked MC in upper pad of the canvas
  all_MC->SetFillColor(kBlack);
  all_MC->SetLineColor(kWhite);
  all_MC->SetMarkerColor(kBlack);
  all_MC->SetFillStyle(3004);
  all_MC->SetLineWidth(2);
  all_MC->SetMarkerSize(0.);
  leg->AddEntry(all_MC, "Stat. unc.","f");
  all_MC->Draw("sameE2p");

// Preparing to draw statistical uncertainties on th elower pad in the canvas where ratio plot is

  TH1F* stat_unc_pad2_MC = (TH1F*)all_MC->Clone("stat_unc_pad2_MC");  

  for(int bin=0; bin <= stat_unc_pad2_MC->GetNbinsX(); ++bin){
    Double_t oberr = 0;
    Double_t obcontent = 0;
    Double_t berr = stat_unc_pad2_MC->GetBinError(bin);
    oberr = stat_unc_pad2_MC->GetBinError(bin+1);
    Double_t bcontent = stat_unc_pad2_MC->GetBinContent(bin);
    obcontent = stat_unc_pad2_MC->GetBinContent(bin+1);
    //std::cout<< "bin error: "<<oberr<< " bin content: "<<obcontent<<std::endl;
    if(oberr > 0 && obcontent > 0){
      stat_unc_pad2_MC->SetBinError(bin+1, oberr/obcontent);
      stat_unc_pad2_MC->SetBinContent(bin+1, 1.0);
    }
  }

//Data is the last inpit file so hists[i] i=5 is the data.root because it starts counting from zero.!!!
  hists[5]->SetLabelSize(0.102, "X");
  hists[5]->SetLabelSize(0.102, "Y");
  hists[5]->SetMarkerStyle(20);   
  hists[5]->SetMarkerSize(0.8);   
  hists[5]->SetMarkerColor(kBlack);  
  hists[5]->SetLineColor(kBlack); 
  hists[5]->SetFillColor(kWhite);
  leg->AddEntry(hists[5],"Data","lep");   
  hists[5]->Draw("same ep");
// title of the X and Y axis
  if ( name.Contains("pt") ) {
    //hists[5]->GetXaxis()->SetRangeUser(200000.,450000.);
    hists[5]->GetXaxis()->SetTitle("Large-R Jet P_{T} [MeV]");
    data_v_mc_stacked->GetYaxis()->SetTitle("Events / 10 GeV");
    data_v_mc_stacked->GetYaxis()->SetTitleSize(0.055);     
    data_v_mc_stacked->GetYaxis()->SetLabelSize(0.055);
  }
  else if ( name.Contains("mass") ) {
    //hists[5]->GetXaxis()->SetRangeUser(50000.,150000.);
    hists[5]->GetXaxis()->SetTitle("Large-R Jet Mass [MeV]");
    data_v_mc_stacked->GetYaxis()->SetTitle("Events / 5 GeV");
    data_v_mc_stacked->GetYaxis()->SetTitleSize(0.055);     
    data_v_mc_stacked->GetYaxis()->SetLabelSize(0.055);
  }
  else if ( name.Contains("eta") ) {
    //hists[5]->GetXaxis()->SetRangeUser(-3.0,3.0);
    hists[5]->GetXaxis()->SetTitle("Large-R Jet #eta");
    data_v_mc_stacked->GetYaxis()->SetTitle("Events / 0.5");
    data_v_mc_stacked->GetYaxis()->SetTitleSize(0.055);     
    data_v_mc_stacked->GetYaxis()->SetLabelSize(0.055);
  }
  else if ( name.Contains("phi") ) {
    //hists[5]->GetXaxis()->SetRangeUser(-4.0,4.0);
    hists[5]->GetXaxis()->SetTitle("Large-R Jet #phi");
    data_v_mc_stacked->GetYaxis()->SetTitle("Events / 0.5");
    data_v_mc_stacked->GetYaxis()->SetTitleSize(0.055);     
    data_v_mc_stacked->GetYaxis()->SetLabelSize(0.055);
  }

  can->cd();

  TH1D *h_ratio = (TH1D*)hists[5]->Clone(); //Cloning the data in order to divide by the sum of MC
  h_ratio->Divide(all_MC); 

  h_ratio->SetMinimum();
  h_ratio->SetTitle("");   
  h_ratio->SetMarkerStyle(20);   
  h_ratio->SetMarkerSize(1);   
  h_ratio->SetYTitle("Data/MC");

// Now entering lower pad in the canvas to draw the ratio plot  
  pad2->SetGridy();   
  pad2->SetGridx();   
  pad2->cd();   
  h_ratio->GetYaxis()->SetRangeUser(0.5,1.5);   
  h_ratio->SetLabelSize(0.102,"X");     
  h_ratio->SetLabelSize(0.102,"Y");     
  h_ratio->SetTitleSize(0.1,"X");   
  h_ratio->SetTitleSize(0.1,"Y");   
  h_ratio->SetTitleOffset(0.48,"Y");   
  h_ratio->SetTitleOffset(1.2,"X");   
  h_ratio->GetYaxis()->SetNdivisions(507,"xyz");  
  h_ratio->Draw("E0P");
  h_ratio->SetTickLength(0.01,"X"); //0.05   
  h_ratio->SetLabelOffset(0.01,"X"); //0.02

// Drawing stat. unc. in the ratio plot
  stat_unc_pad2_MC->SetFillColor(kBlack);
  stat_unc_pad2_MC->SetLineColor(kWhite);
  stat_unc_pad2_MC->SetMarkerColor(kBlack);
  stat_unc_pad2_MC->SetFillStyle(3004);
  stat_unc_pad2_MC->SetLineWidth(2);
  stat_unc_pad2_MC->SetMarkerSize(0.);
  stat_unc_pad2_MC->Draw("sameE2p");


  pad1->cd();

  leg->Draw();
 
  l->Draw();
  p->Draw();
  r->Draw();
  n->Draw();
  
  if (name.Contains("Zcand_mass")) v->Draw();
  if (name.Contains("sr_Xbb_50")) Xbb50->Draw();
  if (name.Contains("sr_Xbb_60")) Xbb60->Draw();
  if (name.Contains("sr_Xbb_70")) Xbb70->Draw();
  
  can->SaveAs("plots_datavmc/Nominal_"+name+"_data_v_mc.pdf");
  
  return;
}

template <class THnD> int readplots(TFile ** filePointers,THnD ** histPointers,TString name) {
  int trouble = 0;
  gROOT->SetBatch();   
  gStyle->SetOptStat(0);
  std::cout<<"readplots is called for name="<<name<<std::endl;
  for (int i=0;i<nfile;++i) {
    histPointers[i] = 0;
    if (!filePointers[i]) {
      trouble++;
      continue;
    }
    filePointers[i]->cd();

    histPointers[i] = (THnD*)gROOT->FindObject(name);
    if (histPointers[i] == 0 ) {
      trouble++;
    } else {
      if ( name.Contains("mass") ) histPointers[i]->GetXaxis()->SetTitle("m_{J} [MeV]");
      else if ( name.Contains("pt") ) histPointers[i]->GetXaxis()->SetTitle("p_{T} [MeV]");
      else if ( name.Contains("eta") ) histPointers[i]->GetXaxis()->SetTitle("#eta");
      else if ( name.Contains("phi") ) histPointers[i]->GetXaxis()->SetTitle("#phi");
    }
  }

  if (trouble > 0) std::cout<<"readplots failed to find "<<trouble<<" from "<<nfile<<" for "<<name<<std::endl;
  
  return trouble;
}

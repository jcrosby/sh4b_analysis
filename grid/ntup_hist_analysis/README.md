
## Setup and Layout
This code requires root to be set up (6.14.04 is the current recommendation) and python2.7.

```#
git clone <url>
git checkout -b <branchname>
source setup_root.sh
```


There are two folders:


1. "MyAnalysis" containing the source files and headers used,
2. "Run" containing the files related to running the code.

## Running the code

The Makefile inside the run directory creates the executable and the dictionary required, depending on whether you want to run over data or MC. In order to run this code, the paths to the files in the makefile may need to be updated, including the path to root and python versions for linking.

In order to compile the code, enter run directory.
```
cd run
make all
```

If you run over **MC simulations** including systematics chains, with or without (pdf&scale) systematics:
```
make [mc/mc_nosysts]
./mc ${opdir} ${infile} ${outfile} ${path/to/dir/containing/MC/file}
```
where opdir = directory name to store ouput files; infile = name of the file to run over; outfile = name for the output file.
If you run with **data**:
```
make data
./data ${opdir} ${infile} ${outfile} ${path/to/dir/containing/MC/file}
```

The difference between the commands is whether another file, "processtype.h", contains the definition "processdata" or not. Within the Nominal.cxx and Nominal.h files, certain variables are only defined when running over the MC samples.

After modify the code,

```
make clean
make [mc/data]
```
### LinkDef
There is a LinkDef.h file inside the MyAnalysis folder. This needs to contain all of the classes created for this particular code so if any new ones are created, they need to be added here.

### Testing
The testRun.sh is a simple bash script to run the code for a single MC file, locally. The name of the "infile" and the path to the directory in the command may need to be updated. This file needs to be an executable and then to run this file in the "Run" directory, the command is
```
./testRun.sh
```
### Running for multiple files
The domc.sh file is a scipt for batch submission for the MC samples with systematics. It is included as an example of how to run over multiple MC files. The dodata.sh are the same for the data, respectively. After running the "make" command as above, simply run this script in the "Run" directory, changing the paths to the relevant files as required:

```
./domc.sh
```
Of course, afterwards, use the addup_*.sh script to merge output files.

to run the macros:
```
root -l path/to/macro
```




#bin/sh

date=`date +%Y%m%d`
outdir=Nominal"_"${date}_SH4b_mc20a_bkg
indir=/lustre/fs22/group/atlas/dbattulga/ntup_SH_Oct20/bkg/
#indir=/lustre/fs22/group/atlas/dbattulga/ntup_SH_Oct20/MC/
if [[ -e $outdir ]] ; then
   echo outdir $outdir already exists
   #echo I quit
   #exit
fi

#for thisfile in `ls -d /lustre/fs22/group/atlas/dbattulga/ntup_SH_Oct20/bkg/user.dabattul.*r13144*_TREE` ; do
for thisfile in `ls -d /lustre/fs22/group/atlas/dbattulga/ntup_SH_Oct20/bkg/user.dabattul.*r13167*_TREE` ; do
#for thisfile in `ls -d /lustre/fs22/group/atlas/dbattulga/ntup_SH_Oct20/bkg/user.dabattul.*r13145*_TREE` ; do

    # let counter=counter+1 
    # if [ $counter -eq 3 ]; then
    # 	exit
    # fi

    infile=`echo $thisfile | awk -F/ '{print $9}'`
    outfile="nominal-"`echo $thisfile | awk -F. '{print $5"."$6".root"}'`

    echo ${infile}
    echo ${outfile}
    filename=${outfile}"_"${date}"_mc.sub"
    echo filename=$filename
    rm -fr $filename
touch $filename
cat <<EOF >$filename

universe = vanilla
priority=100
executable=./mc_nosysts_R22_mc20a
arguments=$PWD/${outdir} ${infile} ${outfile} ${indir}
getenv=True
log=$PWD/${outfile}.log
error=$PWD/${outfile}.err
output=$PWD/${outfile}.out
should_transfer_files   = YES
transfer_input_files = /lustre/fs22/group/atlas/dbattulga/ntup_SH_Oct20/bkg/${infile}, $PWD/read_json.py, $PWD/mc_metadata_HH4b.json, $PWD/libMyAnalysis_rdict.pcm  
when_to_transfer_output = ON_EXIT
request_cpus = 1
request_memory = 2G
notification = Always
queue 1



EOF
      chmod +x $filename
      condor_submit $PWD/$filename
          (( c++ ))
done

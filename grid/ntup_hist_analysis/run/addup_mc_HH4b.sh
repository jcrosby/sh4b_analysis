date=`date +%Y%m%d`
dir=Nominal_all_bkgs_SH4B_20230118 #all  #_QCD4b_DL1r_Xbb #HH4b_DL1r_Xbb
outdir=Templates"_bkg_SH4b_"${date}
#outdir_year=Templates"_NewTuples_Mear_"${date}

if [[ -e $outdir ]] ; then
    echo dir $outdir already exists
    echo I quit
    exit
fi

mkdir ${outdir}
#mkdir ${outdir_year}

echo $

# QCD
#hadd ${outdir}/qcd_4bjets.root ${dir}/nominal_Nominal-Py8EG_A14NNPDF23LO*
hadd ${outdir}/qcd.root ${dir}/nominal-Pythia8EvtGen_A14NNPDF23LO_jetjet*
# ttbar
hadd ${outdir}/ttbar.root ${dir}/nominal-PhPy8EG_A14_ttbar*

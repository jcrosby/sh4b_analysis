# sh4b_analysis



## Getting started

This is the growing framework for the SH4b analysis in the boosted regime

## Ntuple Production

Currently there are two active frameworks used for ntuple production that are also using r22. Both can be used for this analysis.

- [hh4bAnalysis](https://gitlab.cern.ch/easyjet/hh4b-analysis)
- [sh4bAnalysis](https://gitlab.cern.ch/sh4b/sh4banalysis)

## Grid Sumbission

In order to run ntuples on the grid, first go into the grid/ntup_hista_analysis folder and run (assuming you're running on lxplus):

```
source setup_grid.sh
```

Bulk of ntup_hist_analysis is contributed from Daariimaa battulga

### Make Executable

Depending on if you're running on Monte Carlo (MC) or data and which data taking year, the two lines that need editing are L4 `EXENOSYS` or L5 `EXEDAT` and L21 `all: ${pointertoEXE}`. Each data taking year have a separate list of triggers. Depending on which data taking year your executable is, we'll have to change which triggers the comilation runs over. In order to do so:

```
vim ../MyAnalysis/Nominal.cxx 
```

On L6 we see `#define trig13167`. Here we change depending on which data taking year we're running over. 

* mc20* are reprocessed mc16*
* mc20a, mc20d, mc20e correspond to the different profiles of number of interactions for data15-16, data17 and data18.
* This is encoded in some reoccuring r-tags

| mc campaign | r-tag  |  #define  |   Year    |
| ----------- | ------ | --------- | --------- |
| mc20a       | r13167 | trig13167 | data15-16 |
| mc20d       | r13144 | trig13144 | data17    |
| mc20e       | r13145 | trig13145 | data18    |


Once the definition is set in `Nominal.cxx` and the executable name corresponds in the `Makefile` you can now run:

```
make all
```

### Get Grid Inputs

```
cd get_grid_inputs
```

Here we generate our grid input text files and move them to the folder `/run.grid`. First edit the shell scirpt.
```
vim list-dids_rucio.sh
```
Here we see
```
rucio list-dids user.dabattul.ntup_bkg*_24022023*_TREE
```
Change the user to whatever ntuples you want to run over.
Next we can run the bash script:
```
source get_clean_did_list.sh
```
This generates `dids_output.txt` that is cleaned up from all the rucio printouts.
Next we generate our text files we'll use for grid inputs that contain all the root files within these containers and is formatted for proper submission.

First edit:
```
vim get_grid_texts.txt
```

Assuming the `dids_output.txt` contains several r-tags, we extract the specific r-tag to whatever mc campaign you're running over. We edit L4 `RECON=rxxxxx` to whichever r-tag is necessary. If you want to generate the whole `dids_output.txt` regardless of r-tag, uncomment `#input_file=dids_output.txt`.

Next we generate text files and move them to the `/run.grid` folder
```
source get_grid_texts.sh
source mv_files.sh
```

### Grid Submission
```
cd ../run.grid
source setup_grid.sh
```
Make sure to copy over your exectuable to this folder. Now edit:
```
vim submit_grid.sh
```
Here edit the input executable on L4 `SCRIPT=mc_nosyst_mc20a`. Now depending on what you want for your output container names, you can edit L19-L22. Comment out `prun` to check output names. Once you're satisfied, run:
```
source submit_grid.sh
```
This outputs job ID to a log file for each container and moves all necessary files to a folder called `/submit`

